<?php
class UsuarioDAL extends Exceptions implements Template {

	private $resp;
	private $db;

	public function __construct() {
		$this->db = new DB();
	}

	public function getResp() {
		return $this->resp;
	}

        
        public function buscar($extra="") {
		try {
			$sql = "SELECT * FROM usuario ".$extra;
			$this->db->query($sql);
			if($this->db->quantidadeRegistros() > 0) {
				while($obj = $this->db->fetchObj()) {
					$arr[] = new UsuarioTO($obj->id, $obj->nome, $obj->login, $obj->senha, $obj->email, $obj->nivel, $obj->status, $obj->cadastro);
				}
				$this->resp = $arr;
				return true;
			} else {
				$arr[] = new UsuarioTO('0', '0', '0', '0', '0', '0', '0', '0');
				$this->resp = $arr;
				return false;
			}
		} catch (Exception $e) {
			$arr[] = new UsuarioTO($e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage());
			$this->resp = $arr;
			return false;
		}
	}

	public function inserir($usuario) {
		try {
			$dados = "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getId())."',";
			$dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getNome())."',";
			$dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getLogin())."',";
                        $dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getSenha())."',";
                        $dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getEmail())."',";
                        $dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getNivel())."',";
                        $dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getStatus())."',";
			$dados .= "'".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getCadastro())."'";
			
			$sql = "INSERT INTO usuario VALUES (".$dados.")";
			if($this->db->query($sql)) {
                                $this->resp = "Dados inseridos.";
				return true;
			} else {
				$this->resp = "Dados n�o inseridos.<br>".$this->db->getErro();
				return false;
			}
		} catch (Exception $e) {
			parent::exceptionUsuario("UsuarioDAL","inserirUsuario");
			$this->resp = $e->getMessage();
			return false;
		}
	}

	public function atualizar($usuario) {
			try {
				
				$sql = "UPDATE usuario SET
						nome='". mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getNome())."',
						login='".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getLogin())."',
                                                senha='".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getSenha())."',
                                                email='".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getEmail())."',
                                                nivel='". mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getNivel())."',
                                                status='".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getStatus())."',
						cadastro='".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getCadastro())."'
				
				WHERE id='".mysqli_real_escape_string($this->db->LigarConexao(),$usuario->getId())."'";
				if($this->db->query($sql)) {
					$this->resp = "Dados atualizados.";
					return true;
				} else {
					$this->resp = "Dados n�o atualizados.<br>".$this->db->getErro();
					return false;
				}
			} catch (Exception $e) {
				parent::exceptionUsuario("UsuarioDAL","atualizarUsuario");
				$this->resp = $e->getMessage();
				return false;
			}
		}

	public function excluir($cod_usuario) {
		try {
			$sql = "DELETE FROM usuario WHERE id='".mysqli_real_escape_string($this->db->LigarConexao(),$cod_usuario)."'";
			if($this->db->query($sql)) {
				$this->resp = "Dados deletados.";
				return true;
			} else {
				$this->resp = "Dados n�o deletados.<br>".$this->db->getErro();
				return false;
			}
		} catch (Exception $e) {
			parent::exceptionUsuario("UsuarioDAL","deletaUsuario");
			$this->resp = $e->getMessage();
			return false;
		}
	}

	public function contar($extra="") {
		try {
			$sql = "SELECT * FROM usuario ".$extra;
			$this->db->query($sql);
			if($this->db->quantidadeRegistros() > 0) {
				$this->resp = $this->db->quantidadeRegistros();
				return true;
			} else {
				$this->resp = "0";
				$this->resp = "Nenhum registro encontrado.";
				return false;
			}
		} catch (Exception $e) {
			parent::exceptionUsuario("UsuarioDAL","contarUsuario");
			$this->resp = $e->getMessage();
			return false;
		}
	}
        
         public function status($id, $nivel) {
		try {
			if ($nivel == "1")
			$sql = "UPDATE usuario SET nivel='2' WHERE id=$id";
			
			 else
			 $sql = "UPDATE usuario SET nivel='1' WHERE id=$id";
			 
			if($this->db->query($sql)) {
				$this->resp = "Dados atualizados.";
				return true;
			} else {
				$this->resp = "Dados n�o atualizados.<br>".$this->db->getErro();
				return false;
			}
		} catch (Exception $e) {
			parent::exceptionUsuario("UsuarioDAL","atualizarUsuario");
			$this->resp = $e->getMessage();
			return false;
		}
	}
        
        //desativar usu�rio ou n�o
        public function statusUsuario($id, $nivel) {
		try {
			if ($nivel == "0")
			$sql = "UPDATE usuario SET status='1' WHERE id=$id";
			
			 else
			 $sql = "UPDATE usuario SET status='0' WHERE id=$id";
			 
			if($this->db->query($sql)) {
				$this->resp = "Dados atualizados.";
				return true;
			} else {
				$this->resp = "Dados n�o atualizados.<br>".$this->db->getErro();
				return false;
			}
		} catch (Exception $e) {
			parent::exceptionUsuario("UsuarioDAL","atualizarUsuario");
			$this->resp = $e->getMessage();
			return false;
		}
	}


}
?>