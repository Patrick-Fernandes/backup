<?
include("../include/header.php");

if ($_SESSION["login"]->getNivel() != 1) {
echo "<script>window.location='home';</script>";
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
                    <?= montaCab("Usuario", "Incluir"," ")?>
    
        <div class="row">
            <div class="col-lg-6">

                <form method="post" action="../Usuario_incluir">

                    <div class="form-group">
                        <label> Nome:</label>
                        <input type="text" name="txtNome" class="form-control tam-max" />
                    </div>


                    <div class="form-group">
                        <label> E-mail:</label>
                        <input type="text"  name="txtEmail" class="form-control tam-max"/>
                    </div>

                    <div class="form-group">
                        <label> Login:</label>
                        <input type="text" name="txtLogin" class="form-control"/>
                    </div>

                    <div class="form-group">
                        <label> Senha:</label>
                        <input type="password"  name="txtSenha" class="form-control tam-max-senha"/>
                    </div>

                    <div class="form-group">
                        <label> Digite sua senha novamente:</label>
                        <input type="password" size=40 name="txtConfereSenha" class="form-control tam-max-senha"/>
                    </div><br>

                    <label> N�vel:</label><br>
                    <select name="txtNivel" id="txtNivel" class="form-control tam-max-option" name="option">
                        <option value="" disabled selected>selecione</option>
                        <option value="2">Usu�rio</option>
                        <option value="1">Administrador</option>
                    </select><br><br>

                    <label> Status:</label><br>
                    <select name="txtSts" id="txtSts" class="form-control tam-max-option" name="option">
                        <option value="" disabled selected>selecione</option>
                        <option value="1">Ativo</option>
                        <option value="0">Inativo</option>
                    </select>

                    <br><br>
                    <button type="submit" class="btn btn-default">Enviar</button>
                    <button type="reset" class="btn btn-default">Resetar</button>
                    <a href="Usuarios" class="btn btn-default">Voltar</a>

                </form>
                <br>
            </div>
        </div>
    </div>
</div>



<?php
include("../include/footer.php");
?>
