<?php

/**
 * 
 * retira acentos das funções
 * 
 * @param type $string é a string para arrumar
 * @return type a estring limpa
 */


function sanitizeString($string) {
    // matriz de entrada
    $what = array('ä', 'ã', 'à', 'á', 'â', 'ê', 'ë', 'è', 'é', 'ï', 'ì', 'í', 'ö', 'õ', 'ò', 'ó', 'ô', 'ü', 'ù', 'ú', 'û', 'À', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ', 'ç', 'Ç', ' ', '-', '(', ')', ',', ';', ':', '|', '!', '"', '#', '$', '%', '&', '/', '=', '?', '~', '^', '>', '<', 'ª', 'º');

    // matriz de saída
    $by = array('a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'A', 'A', 'E', 'I', 'O', 'U', 'n', 'n', 'c', 'C', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_');

    // devolver a string
    return str_replace($what, $by, $string);
}


/**
 * 
 * @param  $nomeCabecalhopassa o nome do cabeçalho
 * @param $detalhe  type passa o a legenda 
 * @param $paginaAdicionar passa o link, sendo esse possivel ser nulo 
 * @return um cabeçalho
 */
function montaCab($nomeCabecalho, $detalhe, $paginaAdicionar) {
    if ($paginaAdicionar == " ") {
        return "<div class='row'>
            <div class='col-lg-12'>
                <h3 class='page-header' style='margin-top: 15px'>
                    $nomeCabecalho
                </h3>
                <ol class='breadcrumb'>
                    <li class='active'
                        <i class='fa fa-file-text'></i> $detalhe
                    </li>
                </ol>
            </div>
        </div>";
    } else {
        return "<div class='row'>
            <div class='col-lg-12'>
                <h3 class='page-header' style='margin-top: 15px'>
                    $nomeCabecalho
                </h3>
                <ol class='breadcrumb'>
                    <li class='active'
                        <i class='fa fa-file-text'></i> $detalhe
                    </li>
                </ol>
                <button class='btn btn-default' onclick=\"window.location.href = '$paginaAdicionar'\">$nomeCabecalho</button><br><br>
            </div>
        </div>";
    }
    
    
    
    
}