<?php include ('../include/header.php') ?>

<div id="page-wrapper">
    <div class="container-fluid">
                    <?= montaCab("Documentos", "<i class='fa fa-upload'></i> Upload de arquivos"," ")?>


        <div class="row">
            <div class="col-lg-6">

                <form role="form" action="../controller/DOC_incluir" name="documento" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label>Nome</label>
                        <input class="form-control" name="nome" placeholder="Nome completo">
                    </div>


                    <div class="form-group">
                        <label>Anexe seu arquivo</label>
                        <input type="file" name="arquivo">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-default">Enviar</button>
                    <button type="reset" class="btn btn-default">Resetar</button>
                    <a href="Documento" class="btn btn-default">Voltar</a>
                </form>
                <br>
            </div>
        </div>
    </div>
</div>

<?php include ('../include/footer.php') ?>