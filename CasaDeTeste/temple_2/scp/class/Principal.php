<?php
function __autoload($classe) {
	$dir = str_replace("\\","/",dirname(__FILE__));
	if(file_exists($dir."/".$classe.".php")) {
		include($dir."/".$classe.".php");
	} else {
		if(file_exists($dir."/classesTO/".$classe.".php")) {
			include($dir."/classesTO/".$classe.".php");
		} else {
			if(file_exists($dir."/classesDAL/".$classe.".php")) {
				include($dir."/classesDAL/".$classe.".php");
			} else {
				exit("Arquivo n�o encontrado!");
			}
		}
	}
}


?>