<?
/**
 *  frog::SMS
 *  http://fr0gSMS.codigolivre.org.br
 * 
 *  Autor: Luiz Geovani Vier (vier at brturbo.com - http://vier.ta-na.net)
 *  Data: Maio/2004
 *  Licen�a: GNU GPL - http://www.gnu.org/copyleft/gpl.html
 */
 
include("sms.class.php");

$smsSender = new SMSSender();
$msgRetorno = "";

$sucesso = $smsSender->ping($operadora, $msgRetorno);
?>
<script language="JavaScript">
	parent.removerControleTimeout();
	parent.tratarRetornoPing(<?= ($sucesso ? 'true' : 'false') ?>, '<?= $msgRetorno ?>');
	parent.setarFoco();
</script>