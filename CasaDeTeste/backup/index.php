<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "livraria";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// arquivo onde ficará os comandos para o restore
$arq = fopen("bak_livraria.sql", "w");

// SET FOREIGN_KEY_CHECKS = 0; serve para desabilitar a validação dos relacionamentos
// no momento da restauração. Exemplo: criar a tabela itens_venda antes da venda
fwrite($arq, "SET FOREIGN_KEY_CHECKS=0;\r\n");

// obtém as tabelas do banco
$tables = mysqli_query($conn, "show tables");

echo "<h2> Backup dos dados do sistema </h2>";

while ($linha_table = mysqli_fetch_array($tables)) {
    // obtém o nome da tabela
    $table = $linha_table[0];
    
    echo "<p> Copiando Tabela $table... </p>";
    
    $comando = mysqli_query($conn, "show create table $table");
    
    $linha = mysqli_fetch_array($comando);
    
    // grava o comando create table
    fwrite($arq, "\r\n" . $linha[1] . ";\r\n\r\n");
    
    // obtém os dados já inseridos nesta tabela
    $dados = mysqli_query($conn, "select * from $table");
    
    // percorre os registros obtidos
    while ($linha = mysqli_fetch_array($dados)) {
        // obtém os campos da tabela
        $campos = mysqli_query($conn, "show fields from $table");
        
        // variáveis para acumular nome das colunas e conteúdos
        $colunas = "";
        $conteudos = "";
        
        // percorre todos os campos da tabela
        while ($linha_campos = mysqli_fetch_array($campos)) {
            $campo = $linha_campos["Field"];
            
            $colunas = $colunas . $campo . ", ";
            $conteudos = $conteudos . "'" . $linha["$campo"] . "', ";            
        }
        
        // para tirar a última ", "
        $colunas = substr($colunas, 0, -2);
        $conteudos = substr($conteudos, 0, -2);
        
        // comando insert
        $insere = "insert into $table($colunas) values($conteudos)";
        
        // grava o insert no arquivo
        fwrite($arq, $insere . ";\r\n");
    }
}
fclose($arq);

// fecha a conexão
mysqli_close($conn);

echo "<h3> Ok! Cópia Concluída </h3>";