
<div class="col-sm-6  col-lg-6 col-xs-12">
    <h3>Talentos</h3>        
    <table class="table">
        <thead>
            <tr>
                <th><b>Habilidade</b></th>
                <th><b>Descricao</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Diligente </td>
                <td>
                    Benefício: você pode gastar uma ação de movimento para se
                    concentrar na tarefa à frente. Se fi zer isso, recebe +2 nos testes de
                    perícia realizados até a próxima rodada.

                </td>
            </tr>
            <tr>
                <td>Vitalidade</td>
                <td>da mais 1 de PV por nivel<?php echo $vitalidade = 1 * $nivelTotal; ?></td>
            </tr>
            <tr>
                <td>Reflexos Rapidos</td>
                <td>+2 reflexos</td>
            </tr>
            <tr>
                <td>Vontade de ferro</td>
                <td>+2 vontade</td>
            </tr>
            <tr>
                <td>armaduras leves</td>
            </tr>  
            <tr>
                <td>armas (leves e marciais)</td>
            </tr>  
            <tr>
                <td>escudos</td>
            </tr>
            <tr>
                <td>Magia em combate</td>
            </tr>
        </tbody>
    </table>
    <HR>
    <h3>habilidade de raça</h3>        
    <table class="table">
        <thead>
            <tr>
                <th><b>Habilidade</b></th>
                <th><b>Descricao</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>amanho pequeno </td>
                <td>+1 em atk, + 4 em furtividade </td>
            </tr>     

            <tr>
                <td>ver no escurno </td>
                <td>18m camuflagem total</td>
            </tr> 
            <tr>
                <td>potencia draconica</td>
                <td> +1 em magia elemental  </td>
            </tr> 

            <tr>
                <td> sensivel a luz </td>
                <td>luz solar ou magia do dia -1 em atk </td>
            </tr> 
        </tbody>
    </table>
    <HR>
    <h3>habilidade de Classe</h3>        
    <table class="table">
        <thead>
            <tr>
                <th><b>Habilidade</b></th>
                <th><b>Descricao</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>ENCONTRAR ARMADILHAS</td>
                <td>você pode usar a perícia Percepção
                    para encontrar armadilhas. Veja o Capítulo 4: Perícias para
                    mais detalhes.<br>
                    +10
                </td>
            </tr>
            <tr>
                <td>ATK FURTIVO</td>
                <td>Quando atinge um alvo desprevenido
                    ou fl anqueado com um ataque corpo-a-corpo (ou à distância
                    a até 9m), você causa 1d6 pontos de dano adicional. 

                </td>
            </tr>
            <tr>
                <td>Linhagem Elemental</td>
                <td>
                    <?php
                    if ($feit >= 1) {
                        echo 'ar, eletricidade e sônico.
                        Você pode aprender magias divinas de seus descritores como se fossem
                        magias arcanas. Se a mesma magia possuir uma versão arcana
                        e uma divina, você pode aprender a magia no nível mais baixo
                        possível. Além disso, todas as magias de seus descritores custam
                        1 PM a menos para serem lançadas (mínimo 1 PM).';
                    }
                    if ($feit >= 5) {
                        echo ' a dificuldade contra as magias dos descritores ligados ao seu elemento aumenta em CD +2.<br>';
                    }
                    if ($feit >= 10) {
                        echo 'resistência a energia 15 contra os tipos de energia relacionados ao seu elemento.<br>';
                    }
                    if ($feit >= 15) {
                        echo 'Ar: deslocamento de voo 18m<br>';
                    }
                    if ($feit >= 20) {
                        echo 'você transcende sua forma física e se torna uma
                        criatura de puro poder elemental. Você se torna imune a atordoamento,
                        doenças, paralisia, sono, veneno e aos descritores
                        ligados ao seu elemento. uma vez por dia, pode
                        assumir uma forma elemental por 1 minuto como uma ação
                        padrão. Nessa forma, você recebe redução de dano 20, dois
                        ataques de pancada com bônus de +4, que causam dano equivalente
                        a uma clava duas categorias de tamanho maior que a
                        sua (2d6 para uma criatura Média) e a dificuldade contra as
                        magias dos descritores ligados ao seu elemento aumenta em
                        CD +4. Você pode atacar com as duas pancadas, mas se fi zer
                        isso sofre uma penalidade de –4 nas jogadas de ataque. <br>';
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>                Canalizar Energia Negativa  </td>
                <td> uando canaliza energia negativa, pode curar mortos-vivos ou causar dano a criaturas vivas, também a sua escolha.
                    A quantidade de danos é igual a 1d6, mais 1d6 a cada dois níveis de clérigo seguintes.<br>
                    Criaturas que sofrem dano tem direito a um teste de Vontade 
                    (CD 10 + metade do seu nível + modificador de Carisma) para reduzir esse dano à metade.<br>
                    CD: <?PHP
                    $cdCEN = 10 + (floor($nivelTotal / 2)) + $car;
                    $nvCEN = 1 + $car;
                    ECHO $cdCEN;
                    ?>
                    <BR>
                    Numero de Vezes: <?= $nvCEN ?>
                </td>
            </tr>

            <tr>
                <td>Evasão </td>
                <td> quando sofre um ataque que permite
                    um teste de Refl exos para reduzir o dano à metade, você não
                    sofre nenhum dano se for bem-sucedido. Você ainda sofre dano
                    normal se falhar no teste de Reflexos.
                </td>
            </tr>

            <tr>
                <td>Mágica do Dragão ==PODER CONSEDIDO==</td>
                <td>Pré-requisito: devoto dos Dragões ou Magia (Wynna); qualquer talento metamágico.<BR>
                    Benefício: uma vez por dia, você pode aplicar um talento metamágico a uma magia sem precisar pagar pelo custo extra em PM.
                </td>
            </tr>
            <tr>
                <td>Estender Magia  ==META MAGICO ==</td>
                <td>Custo: +1 PM.
                    <BR>
                    Benefício: a duração de uma magia é duplicada (por exemplo, um imobilizar pessoa estendido dura 2 minutos, em vez de 1 minuto).
                    <BR>
                    Magias com duração instantânea, permanente ou concentração não podem ser afetadas por este talento.</td>
            </tr>
            <tr>
                <td>Magia Duradoura   ==META MAGICO ==</td>
                <td>Custo: +6 PM
                    <BR>
                    Pré-requisito: Estender Magia
                    <BR>
                    Benefício: uma magia duradoura tem duração de um dia.
                    <BR>
                    Especial: magias com duração instantânea, permanente ou concentração não podem ser afetadas por este talento.</td>
            </tr>
        </tbody>
    </table>
</div>