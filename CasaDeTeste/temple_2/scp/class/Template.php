<?php
interface Template {

	public function getResp();
	public function buscar($extra="");
	public function inserir($objto);
	public function atualizar($objto);
	public function excluir($chaveprimaria);
	public function contar($extra="");

}
?>