<?php
if (!is_numeric($_GET["cod"])) {
    ?>
    <script language="javascript">
        history.back(-1);
    </script>
    <?php
}
$cod = $_GET["cod"];

$cab = "Editar documento";
include("../include/header.php");

//Busca o grupo
$dDAL = new DocumentoDAL();
$retorno = $dDAL->buscar("where id=$cod");
if ($retorno) {
    $resp = $dDAL->getResp();
    $documento = $resp[0];
} else {
    ?>
    <script language="javascript">
        alert("Registro n�o encontrado");
        history.back(-1);
    </script>
    <?php
}


if ($documento->getStatus() == "Inativo") {
    $opt = "<option value='Ativo'>Ativo</option>
            <option value='Inativo' selected='selected'>Inativo</option>";
} else {
    $opt = "<option value='Ativo' selected='selected'>Ativo</option>
            <option value='Inativo'>Inativo</option>";
}
?>


<div id="page-wrapper">
    <div class="container-fluid">

        <?= montaCab("Documentos", "<i class='fa fa-upload'></i>" . $cab, " ") ?>


        <div class="row">
            <div class="col-lg-6">

                <form action="../controller/DOC_alteracao" name="documento" method="post" enctype="multipart/form-data">


                    <input type="hidden" size=40 name="txtLogin" id="txtlogin" maxlength="20" value="<? echo $_SESSION["login"]->getId(); ?>"/>

                    <input type="hidden" name="cod" id="cod" value="<?= $documento->getId() ?>"/>


                    <div class="bt">
                        <div class="bt-cadastrar" onclick="documento.submit()"></div>
                        <div class="bt-cancelar" onclick="javascript:history.go(-1);"></div>

                        <div class="form-group">
                            <label>Nome</label>
                            <input class="form-control" name="txtcurso" placeholder="Nome completo" value="<?= $documento->getNome() ?>">
                        </div>

                        <div class="form-group">

                            <label class="label_cad">Status:</label>
                            <select name="txtStatus" id="txtSts" class="w3-input w3-border w3-round tam-max-option"><?= $opt ?></select><br />

                        </div>


                        <div class="form-group">
                            <label>Link do Arquivo Antigo <br>
                                <a href='files/"<?= $documento->getId() ?> "' title='Status'><?= $documento->getNome() ?></a>
                            </label><br>
                            <label>Arquivo</label>
                            <input type="file" name="arquivo">
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default">Enviar</button>
                        <button type="reset" class="btn btn-default">Resetar</button>
                        <br />
                    </div>
                </form>
                <br>
            </div>
        </div>
    </div>
</div>
<? include('../include/footer.php'); ?>
