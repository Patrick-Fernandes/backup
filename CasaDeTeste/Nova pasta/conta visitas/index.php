<?php
/****************************************************************
*@Author: Eduardo Stuart - eduardo@eduardostuart.com
*@Web: http://www.eduardostuart.com
*@Description: Simples contador de visitas que utiliza db-mysql
*@E-mail: eduardo@eduardostuart.com
****************************************************************/
include("contador.php");
$contador = new contador;
$contador->pegaData();
$contador->pegaIp();
$contador->visitaHoje();
?>
<html>

<head>
  <title>Contador de visitas</title>
</head>

<body>
<? $contador->mostraVisitantes(); ?><br><? $contador->mostraVisitantesHoje(); ?>
</body>

</html>