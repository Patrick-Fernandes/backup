<?php
$cab = "Menu Principal";
include("../class/Principal.php");

$nome = $_POST["txtNome"];
$login = $_POST["txtLogin"];
$senhaI = $_POST["txtSenha"];
$confereSenha = $_POST["txtConfereSenha"];
$sts = $_POST["txtSts"];
if (strlen($senhaI) < 8) {
    echo "<script>
            alert('A senha deve conter no minimo 8 caracteres');
            history.go(-1);
        </script>";
    exit;
}
if (ctype_digit($senhaI)) {
    echo "<script>
            alert('A senha deve conter letras(recomendado Maiusculas e Minusculas), alem de numeros');
            history.go(-1);
     </script>";
    exit;
}

if (ctype_lower($senhaI)) {
    echo "<script>
            alert('A senha deve conter no minimo um numero(obrigat�rio)e uma letra maiuscula(opcional recomendado)');
            history.go(-1);
     </script>";
    exit;
}
if (ctype_upper($senhaI)) {
    echo "<script>
            alert('A senha deve conter no minimo um numero(obrigat�rio)e uma letra minuscula(opcional recomendado)');
            history.go(-1);
     </script>";
    exit;
}
if (ctype_alpha($senhaI)) {
    echo "<script>
            alert('A senha deve conter no minimo um caracter num�rico(obrigat�rio)');
            history.go(-1);
     </script>";
    exit;
}
if ($senhaI != $confereSenha) {
    echo "<script>
            alert('A senha n�o confere. Verifique as senhas digitadas!');
            history.go(-1);
            </script>";
    exit;
}


$senha = md5($senhaI);
$email = $_POST["txtEmail"];
$nivel = $_POST["txtNivel"];

date_default_timezone_set("America/Sao_Paulo");
$hoje = date("Y-m-d G:i:s");

$cadastro = $hoje;


if (empty($nome)or
        empty($login)or
        empty($senhaI)) {
    echo "<script>
            alert('Preencha todos os campos!');
            history.go(-1);
            </script>";
    exit;
}

if (empty($email)) {
    $email = "N�o informado";
}

$usuario = new UsuarioTO();

$usuario->setNome($nome);
$usuario->setLogin($login);
$usuario->setSenha($senha);
$usuario->setEmail($email);
$usuario->setNivel($nivel);
$usuario->setStatus($sts);
$usuario->setCadastro($cadastro);

$usuarioDAL = new UsuarioDAL();
$retorno = $usuarioDAL->inserir($usuario);
if ($retorno == true) {
    $msg = "<script>
            alert('Cadastro realizado com sucesso!');
            history.go(-1);
            </script>";
} else {
    $msg = "<script>
            alert('N�o foi poss�vel realizar o Cadastro');
            history.go(-1);
            </script>";
}
?>

<div id="divConteudo">
    <legend> Cadastro de Usuarios </legend>
    <p><?= $msg ?></p>
    <a href="../view/Cadastro_de_Usuario">Clique aqui</a> para cadastrar outro Usuario.
    <a href="../view/Usuarios">Voltar</a>.
</div>

<?php include("../include/footer.php"); ?>

</body>
</html>