# fr0g::SMS 1.2 database
#


#
# Table structure for table 'sms_agenda'
#

DROP TABLE IF EXISTS sms_agenda;
CREATE TABLE sms_agenda (
  id varchar(50) NOT NULL default '',
  idUsuario varchar(50) NOT NULL default '',
  operadora varchar(30) NOT NULL default '',
  ddd int(2) unsigned NOT NULL default '0',
  telefone int(8) unsigned NOT NULL default '0',
  de varchar(50) default NULL,
  mensagemPadrao varchar(255) default NULL,
  qtdEnviadas int(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (id,idUsuario),
  UNIQUE KEY uk (id,idUsuario),
  KEY idUsuario (idUsuario)
) TYPE=MyISAM;



#
# Table structure for table 'sms_log'
#

DROP TABLE IF EXISTS sms_log;
CREATE TABLE sms_log (
  id int(10) unsigned NOT NULL auto_increment,
  idUsuario varchar(50) default NULL,
  horaEnvio datetime NOT NULL default '0000-00-00 00:00:00',
  operadora varchar(20) NOT NULL default '',
  ip varchar(30) default NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY id (id),
  KEY id_2 (id)
) TYPE=MyISAM;



#
# Table structure for table 'sms_usuarios'
#

DROP TABLE IF EXISTS sms_usuarios;
CREATE TABLE sms_usuarios (
  id varchar(50) NOT NULL default '',
  senha varchar(255) NOT NULL default '',
  email varchar(255) default NULL,
  tituloPagina varchar(50) default NULL,
  contatoDefault varchar(50) default NULL,
  newsletter int(1) unsigned NOT NULL default '0',
  qtdEnviadas int(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (id),
  UNIQUE KEY id (id),
  KEY id_2 (id)
) TYPE=MyISAM;

