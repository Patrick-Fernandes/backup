<? $nomeform = "cadastro";?>

<script>
function validate(<? echo $nomeform?>) {

if (<? echo $nomeform?>.nome.value == "")
{
   alert("Digite o Nome!");
   <? echo $nomeform?>.nome.focus();
   return (false);
}

if (<? echo $nomeform?>.cidade.value == "")
{
   alert("Digite o Nome de sua Cidade!");
   <? echo $nomeform?>.cidade.focus();
   return (false);
}

if (<? echo $nomeform?>.estado.value == "")
{
   alert("Selecione o Estado");
   <? echo $nomeform?>.estado.focus();
   return (false);
}

if (<? echo $nomeform?>.fone1.value == "")
{
   alert("Digite seu Telefone!");
   <? echo $nomeform?>.fone1.focus();
   return (false);
}

if (<? echo $nomeform?>.email.value == "")
{
   alert("Digite seu E-mail!");
   <? echo $nomeform?>.email.focus();
   return (false);
}

if (<? echo $nomeform?>.user_senha.value == "")
{
   alert("Digite a Senha!");
   <? echo $nomeform?>.user_senha.focus();
   return (false);
}

return (true);
}
</script>

<form action="?pg=../estrutura/usuarios/cadastrar_db.php" method="post" name="<? echo $nomeform?>" onSubmit="return validate(this);">
<h3>Cadastrar Administrador</h3>
<table width="440" border="0" align="center" cellpadding="2" cellspacing="0"> 
<tr><td colspan="2" align="right">*Campos Obrigatórios</td>
</tr>
        <tr>
          <td width="140" align="right" valign="middle">*Nome:</td>
          <td width="320" valign="middle">
            <strong>
            <input name='nome' type='text' id="nome" size=45>
          </strong></td>
        </tr>
		
		
		                <tr>
                  <td><div align="right">CPF:</div></td>
                  <td><INPUT name="cpf" size="15" maxlength="11">
                  <i>(ex: 00000000000)</i></td>
                </tr>
                <tr>
                  <td><div align="right">Data Nasc.: </div></td>
                  <td><INPUT name="dia" size="2" maxlength="2">
      /
        <INPUT name="mes" size="2" maxlength="2">
      /
      <INPUT name="ano" size="5" maxlength="4"></td>
                </tr>
                <tr>
                  <td><div align="right">Sexo:</div></td>
                  <td><select class="input" name="sexo">
                      <option selected>Selecione</option>
					  <option>=========</option>
                      <option value="Masculino">Masculino</option>
                      <option value="Feminino">Feminino</option>
                  </select></td>
                </tr>
                <tr>
                  <td><div align="right">Endere&ccedil;o:</div></td>
                  <td><INPUT name="endereco" size="35" maxlength="35"></td>
                </tr>
                <tr>
                  <td><div align="right">Complemento:</div></td>
                  <td><INPUT name="complemento" size="35" maxlength="35"></td>
                </tr>
                <tr>
                  <td><div align="right">N&uacute;mero:</div></td>
                  <td><INPUT name="numero" size="12" maxlength="12"></td>
                </tr>
                <tr>
                  <td><div align="right">Bairro:</div></td>
                  <td><INPUT name="bairro" size="35" maxlength="35"></td>
                </tr>
                <tr>
                  <td><div align="right">CEP:</div></td>
                  <td><INPUT name="cep" size="11" maxlength="9">
                    <i>(ex: 00000-000)</i></td>
                </tr>
                <tr>
                  <td><div align="right">Cidade:</div></td>
                  <td><INPUT name="cidade" size="35" maxlength="35"></td>
                </tr>
                <tr>
                  <td><div align="right">Estado:</div></td>
                  <td><select class="input" name="estado">
		  <option selected>UF</option>
		  <option>==</option>
          <option value="AC">AC</option>
          <option value="AL">AL</option>
          <option value="AP">AP</option>
          <option value="AM">AM</option>
          <option value="BA">BA</option>
          <option value="CE">CE</option>
          <option value="DF">DF</option>
          <option value="ES">ES</option>
          <option value="GO">GO</option>
          <option value="MA">MA</option>
          <option value="MT">MT</option>
          <option value="MS">MS</option>
          <option value="MG">MG</option>
          <option value="PA">PA</option>
          <option value="PB">PB</option>
          <option value="PR">PR</option>
          <option value="PE">PE</option>
          <option value="PI">PI</option>
          <option value="RJ">RJ</option>
          <option value="RN">RN</option>
          <option value="RS">RS</option>
          <option value="RO">RO</option>
          <option value="RR">RR</option>
          <option value="SC">SC</option>
          <option value="SP">SP</option>
          <option value="SE">SE</option>
          <option value="TO">TO</option>
                  </select></td>
                </tr>
                <tr>
                  <td><div align="right">Fone:</div></td>
                  <td>(
                      <INPUT name="ddd1" size="2" maxlength="2">
      )
      <INPUT name="fone1" size="11" maxlength="9"></td>
                </tr>
                <tr>
                  <td><div align="right">Celular:</div></td>
                  <td>(
                      <INPUT name="ddd2" size="2" maxlength="2">
      )
      <INPUT name="fone2" size="11" maxlength="9"></td>
                </tr>
								               <tr>
                  <td><div align="right">MSN:</div></td>
                  <td><INPUT name="msn" size="35" maxlength="35"></td>
                </tr>
				 <tr>
                  <td><div align="right">E-mail:</div></td>
                  <td>
                  <INPUT name="email" size="35" maxlength="35">  
				  </td>
                </tr>
	
        <tr> 
          <td width="140" align="right" valign="middle">*Login:</td>
          <td width="320" valign="middle">
			<input name='user_login' type='text' size=45>
</td>
        </tr>
    <tr> 
      <td width="140" align="right" valign="middle">*Senha:</td>
          <td width="320" valign="middle">
            <strong>
            <input name='user_senha' type='password' size=45>
      </strong></td>
    </tr>
</table>
<table align="center">
        <tr> 
          <td width="436" colspan="2"> <p align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
              <input type='submit' value='Cadastrar'>
              </font></p></td>
        </tr>
  </table>
</form>
