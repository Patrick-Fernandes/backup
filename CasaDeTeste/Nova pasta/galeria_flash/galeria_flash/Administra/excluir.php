<?php require_once('../Connections/conSimple.php'); ?>
<?php require_once('../ScriptLibrary/incAddOnDelete.php'); ?>
<?php
// Delete Before Record Addon 1.0.3
if ((isset($HTTP_GET_VARS['IDFoto'])) && ($HTTP_GET_VARS['IDFoto'] != "")) {
	mysql_select_db($database_conSimple, $conSimple);
	$dbr_result = mysql_query("SELECT Foto FROM galeriaflash WHERE IDFoto=".$HTTP_GET_VARS['IDFoto'], $conSimple) or die(mysql_error());
	$dbr = new deleteFileBeforeRecord();
	$dbr->sqldata = mysql_fetch_array($dbr_result);
	$dbr->path = "../images";
	$dbr->pathThumb = "../thumbs";
	$dbr->naming = "prefix";
	$dbr->suffix = "";
	$dbr->checkVersion("1.0.3");
	$dbr->deleteFile();
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

if ((isset($HTTP_GET_VARS['IDFoto'])) && ($HTTP_GET_VARS['IDFoto'] != "")) {
  $deleteSQL = sprintf("DELETE FROM galeriaflash WHERE IDFoto=%s",
                       GetSQLValueString($HTTP_GET_VARS['IDFoto'], "int"));

  mysql_select_db($database_conSimple, $conSimple);
  $Result1 = mysql_query($deleteSQL, $conSimple) or die(mysql_error());

  $deleteGoTo = "index.php";
  if (isset($HTTP_SERVER_VARS['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $HTTP_SERVER_VARS['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_rsFoto = "1";
if (isset($HTTP_GET_VARS['IDFoto'])) {
  $colname_rsFoto = (get_magic_quotes_gpc()) ? $HTTP_GET_VARS['IDFoto'] : addslashes($HTTP_GET_VARS['IDFoto']);
}
mysql_select_db($database_conSimple, $conSimple);
$query_rsFoto = sprintf("SELECT * FROM galeriaflash WHERE IDFoto = %s", $colname_rsFoto);
$rsFoto = mysql_query($query_rsFoto, $conSimple) or die(mysql_error());
$row_rsFoto = mysql_fetch_assoc($rsFoto);
$totalRows_rsFoto = mysql_num_rows($rsFoto);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form method="post" enctype="multipart/form-data" name="Foto" id="Foto" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,BMP,PNG',true,500,'','','','','','');showProgressWindow('fileCopyProgress.htm',300,100);return document.MM_returnValue">
  Foto: 
  <input name="Foto" type="file" id="Foto" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,BMP,PNG',true,500,'','','','','','')" />
  <input type="submit" name="button" id="button" value="Enviar" />
  <input name="IDFoto" type="hidden" id="IDFoto" value="<?php echo $row_rsFoto['IDFoto']; ?>" />
</form>
</body>
</html>
<?php
mysql_free_result($rsFoto);
?>
