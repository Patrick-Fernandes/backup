
<div class="col-sm-6  col-lg-6 col-xs-12">
    <H3>CA</H3>
    <table class="table">
        <thead>
            <tr>
                <th><b>TOTAL</b></th>
                <th><b>10</b></th>
                <th><b>1/2LV</b></th>
                <th><b>MOD DE<BR>HABILI(DES)</b></th>
                <!--<th><b>bonus<BR>escudo</b></th>-->
                <!--<th><b>bonus<BR>armadura</b></th>-->
                <th><b>mod de tamanho</b></th>
                <th><b>escama </b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><H4><b><?= 10 + (floor($nivelTotal / 2)) + $des + $totalEscudo + $totalaRMADURA + 1 + $escamas ?>
                        </b>
                    </H4>
                </td>
                <td>10</td>
                <td><?= floor($nivelTotal / 2) ?></td>
                <td><?= $des; ?></td>
                <!--<td><?= $totalEscudo; ?></td>-->
                <!--<td><?= $totalaRMADURA; ?></td>-->
                <td>1</td> 
                <td><?= $escamas; ?></td>

            </tr>

        </tbody>
    </table>
</div>
<div class="col-sm-6  col-lg-6 col-xs-12">
    <H3>PONTOS DE VIDA</H3>
    <script type="text/javascript">
        function VIDA() {
            var_quant = 0;
            var valor = document.getElementById("VIDA").value;

            // var quantidade = document.getElementById("quantidade").value;
            var vida =<?= 12 + ($con * $nivelTotal) + (($ladino - 1) * 3) + ($feit * 2) + ($clerico * 4) + ($vitalidade); ?>;
            var var_quant = vida - valor;
            document.getElementById("VIDA_qnt").value = var_quant;
        }
        function MANA() {
            var_quant = 0;
            var valo = document.getElementById("MANA").value;
            // var quantidade = document.getElementById("quantidade").value;
            var mana =<?= 3 + ($car) + (($feit - 1) * 3); ?>;
            var var_quant = mana - valo;
            document.getElementById("MANA_qnt").value = var_quant;
        }
    </script>
    <table class="table">
        <thead>
            <tr>
                <th><b>TOTAL</b></th>
                <th><b>DANO</b></th>
                <th><b>ATUAL</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= 12 + ($con * $nivelTotal) + (($ladino - 1) * 3) + ($feit * 2) + ($vitalidade) + ($clerico * 4); ?></td>
                <td><input type="text" class="input form-control" onblur="VIDA()" id="VIDA"></td>
                <td><input type="text" class="input form-control" id="VIDA_qnt" readonly></td>

            </tr>
        </tbody>
    </table>
</div>
<div class="col-sm-6  col-lg-6 col-xs-12">
    <H3>PONTOS DE MANA </H3>       
    <table class="table">
        <thead>
            <tr>
                <th><b>Tipo</b></th>
                <th><b>TOTAL</b></th>
                <th><b>DANO</b></th>
                <th><b>ATUAL</b></th>
            </tr>
        </thead>
        <tbody>
        <td>FEITICEIRO</td>
        <td><?= 3 + ($car) + (($feit - 1) * 3); ?></td>
        <td><input type="text" class="input form-control" onblur="MANA()" id="MANA"></td>
        <td><input type="text" class="input form-control" id="MANA_qnt" readonly></td>
        </tbody>
        <td>FEITICEIRO</td>
        <td><?= 1 + ($sab) + (($clerico - 1) * 3); ?></td>
        <td><input type="text" class="input form-control" onblur="MANA()" id="MANA"></td>
        <td><input type="text" class="input form-control" id="MANA_qnt" readonly></td>
        </tbody>
    </table>
</div>
<div class="col-sm-6  col-lg-6 col-xs-12">
    <H3>PE </H3>
    <table class="table">
        <thead>
            <tr>
                <th><b>TOTAL </b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td> </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-sm-6  col-lg-6 col-xs-12">
    <H3>Resistencia </H3>
    <table class="table">
        <thead>
            <tr>
                <th><b>HABILIDADE </b></th>
                <th><b>TOTAL</b></th>
                <th><b>1/2LV</b></th>
                <th><b>MOD HABIL</b></th>
                <th><b>VONTADE DE FERRO</b></th>
                <th><b>Reflexos Rapidos</b></th>
                <th><b> </b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>FORTITUDE</td>
                <td><b><?= $TOTAL = (floor($nivelTotal / 2) + $con) ?></b></td>
                <td><?= floor($nivelTotal / 2) ?></td>
                <td><?= $con ?>CON</td>
                <td>0</td>
                <td>0</td>
                <td></td>
            </tr>
            <tr>
                <td>REFLEXOS</td>
                <td><?= floor($nivelTotal / 2) + $des + 2 ?></td>
                <td><?= floor($nivelTotal / 2) ?></td>
                <td><?= $des ?>DES</td>
                <td>0</td>
                <td>2</td>
                <td></td>
            </tr>
            <tr>
                <td>VONTADE</td>
                <td><?= floor($nivelTotal / 2) + $sab + 2 ?></td>
                <td><?= floor($nivelTotal / 2) ?></td>
                <td><?= floor($sab) ?>SAB</td>
                <td>2</td>
                <td>0</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</DIV>