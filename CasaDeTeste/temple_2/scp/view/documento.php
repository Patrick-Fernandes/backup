<?php include ('../include/header.php') ?>

<?php
date_default_timezone_set("America/Sao_Paulo");
$data = date("Y-m-d");
$time = date("Hms_Ymd");

$dir = 'files';
$documentoDAL = new DocumentoDAL();
$retorno = $documentoDAL->buscar();
$conteudotabela = "";
if ($retorno == true) {

    $documento = $documentoDAL->getResp();
    for ($i = 0; $i < count($documento); $i++) {
        $linha = $documento[$i];

        $operacoes = "<a href='Alteracao_de_Documento_N" . $linha->getId() . "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> "
                . "&nbsp;&nbsp;&nbsp;<a href='Exclusso_de_Documento_N" . $linha->getId() . "'
     	onclick='return confirmaExclusao(\"" . $linha->getNome() . "\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";

        if ($linha->getStatus() == "Inativo") {
            $cor = "style='color: red;'";
        }else{
              $cor = "";
        }
        $conteudotabela .= "<tr>
                            <td>" . $linha->getNome() . "</td>
                            <td><a href='../" . $dir . "/" . $linha->getLink() . "'>" . $linha->getLink() . "</a></td>
                            <td><a href='Status_de_Documento_N" . $linha->getId() . "'.$cor>" . $linha->getStatus() . "</a></td>
                                
                            <td width='20px'>&nbsp;" . $operacoes . "</td>
                          </tr>
                         ";
    }
} else {
    $conteudotabela .= "<tr><td colspan=\"8\">Nenhum registro encontrado</td></tr>";
}
?>


<div id="page-wrapper">
    <div class="container-fluid">

        <?= montaCab("Documentos", "<i class='fafa-file-text'>Lista de documentos</i>", "Cadastro_de_Documento") ?>


        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered">
                    <th>Curso</th>
                    <th>Link</th>
                    <th>Status</th>
                    <th>Op��es</th>
                    <?= $conteudotabela ?>
                </table>
            </div>
        </div>

    </div>
</div>


<?php include ('../include/footer.php') ?>
<script>
    function confirmaExclusao(titulo) {
        if (confirm("Confirma Exclus�o de " + titulo + "?"))
            return true;
        else
            return false;
    }
</script>