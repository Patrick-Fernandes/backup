<? $nomeform = "cadastro";?>

<script>
function validate(<? echo $nomeform?>) {

if (<? echo $nomeform?>.nome.value == "")
{
   alert("Digite seu Nome!");
   <? echo $nomeform?>.nome.focus();
   return (false);
}

if (<? echo $nomeform?>.cpf.value == "")
{
   alert("Digite seu CPF!");
   <? echo $nomeform?>.cpf.focus();
   return (false);
}

if (<? echo $nomeform?>.dia.value == "")
{
   alert("Digite o dia de seu Nascimento!");
   <? echo $nomeform?>.dia.focus();
   return (false);
}

if (<? echo $nomeform?>.mes.value == "")
{
   alert("Digite o M�s de seu Nascimento!");
   <? echo $nomeform?>.mes.focus();
   return (false);
}

if (<? echo $nomeform?>.ano.value == "")
{
   alert("Digite o Ano de seu Nascimento!");
   <? echo $nomeform?>.ano.focus();
   return (false);
}

if (<? echo $nomeform?>.sexo.value == "")
{
   alert("Selecione seu Sexo!");
   <? echo $nomeform?>.sexo.focus();
   return (false);
}

if (<? echo $nomeform?>.endereco.value == "")
{
   alert("Digite seu Endere�o!");
   <? echo $nomeform?>.endereco.focus();
   return (false);
}

if (<? echo $nomeform?>.bairro.value == "")
{
   alert("Digite seu Bairro!");
   <? echo $nomeform?>.bairro.focus();
   return (false);
}

if (<? echo $nomeform?>.cep.value == "")
{
   alert("Selecione o Cep de sua Cidade!");
   <? echo $nomeform?>.cep.focus();
   return (false);
}

if (<? echo $nomeform?>.cidade.value == "")
{
   alert("Digite o Nome de sua Cidade!");
   <? echo $nomeform?>.cidade.focus();
   return (false);
}

if (<? echo $nomeform?>.estado.value == "")
{
   alert("Selecione o Estado");
   <? echo $nomeform?>.estado.focus();
   return (false);
}

if (<? echo $nomeform?>.fone1.value == "")
{
   alert("Digite seu Telefone!");
   <? echo $nomeform?>.fone1.focus();
   return (false);
}

if (<? echo $nomeform?>.fone2.value == "")
{
   alert("Digite seu Celular!");
   <? echo $nomeform?>.fone2.focus();
   return (false);
}

if (<? echo $nomeform?>.email.value == "")
{
   alert("Digite seu E-mail!");
   <? echo $nomeform?>.email.focus();
   return (false);
}

if (<? echo $nomeform?>.senha1.value == "")
{
   alert("Digite sua Senha!");
   <? echo $nomeform?>.senha1.focus();
   return (false);
}

if (<? echo $nomeform?>.senha2.value == "")
{
   alert("Confirme sua Senha!");
   <? echo $nomeform?>.senha2.focus();
   return (false);
}

return (true);
}
</script>



<table width="417" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" align="center" valign="top">
<form action="?pg=cadastro" method="post" name="<? echo $nomeform?>" onSubmit="return validate(this);">
<input name="acao" type="hidden" value="send">
<input type="hidden" name="id_franquia" value="<? echo $cidade?>">
<input type="hidden" name="userlevel" value="3">
      <br>
  <table width="720"  border="0" cellspacing="0" cellpadding="3">
                <tr align="left">
                  <td colspan="2"><font color="#0066FF">*Dados
                  necess�rios para o cadastro.</font></td>
          </tr>
                <tr align="left">
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">*Nome Completo:</font></div></td>
                  <td><INPUT name="nome" class="input" id="nome" style="width:400; height:18; border:1px solid #cfcfcf" maxLength="400"></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">*CPF:</font></div></td>
                  <td>                  <i>
                    <INPUT name="cpf" class="input" id="cpf" style="width:150; height:18; border:1px solid #cfcfcf" maxLength="11">
                    <font color="#666666">                  (ex: 00000000000)</font></i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">*Data Nasc.: </font></div></td>
                  <td>      <i>
                    <INPUT name="dia" class="input" id="dia" style="width:25; height:18; border:1px solid #cfcfcf" maxLength="2">
                  </i>/
                  <i>
                  <INPUT name="mes" class="input" id="mes" style="width:25; height:18; border:1px solid #cfcfcf" maxLength="2">
                  </i>
        /
        <i>
        <INPUT name="ano" class="input" id="ano" style="width:50; height:18; border:1px solid #cfcfcf" maxLength="4">
        </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Sexo:</font></div></td>
                  <td><SELECT name="sexo" class="input" id="sexo" style="width:100; height:18; border:1px solid #cfcfcf">
                  <option>selecione
                  <option value="=========">=========
                  <option value="Masculino">Masculino
                  <option value="Feminino">Feminino                  
                                    </SELECT></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">*Endere&ccedil;o:</font></div></td>
                  <td><i>
                    <INPUT name="endereco" class="input" id="endereco" style="width:300; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Complemento:</font></div></td>
                  <td><i>
                    <INPUT name="complemento" class="input" id="complemento" style="width:300; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">*N&uacute;mero:</font></div></td>
                  <td><i>
                    <INPUT name="numero" class="input" id="numero" style="width:180; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">*Bairro:</font></div></td>
                  <td><i>
                    <INPUT name="bairro" class="input" id="bairro" style="width:180; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">CEP:</font></div></td>
                  <td><i>
                    <INPUT name="cep" class="input" id="cep" style="width:85; height:18; border:1px solid #cfcfcf" maxLength="9">
                  </i> 
                    <i><font color="#666666">(ex: 00000-000)</font></i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Cidade:</font></div></td>
                  <td><i>
                    <INPUT name="cidade1" class="input" id="cidade1" style="width:180; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Estado:</font></div></td>
                  <td><SELECT class="input" style="width:45; height:18; border:1px solid #cfcfcf" name="estado">
                    <option selected="selected" value="UF">UF</option>
                    <option value="Acre">AC</option>
                    <option value="Alagoas">AL</option>
                    <option value="Amazonas">AM</option>
                    <option value="Amap�">AP</option>
                    <option value="Bahia">BA</option>
                    <option value="Cear�">CE</option>
                    <option value="Distrito Federal">DF</option>
                    <option value="Espirito Santo">ES</option>
                    <option value="Goi�s">GO</option>
                    <option value="Mato Grosso do Sul">MS</option>
                    <option value="Mato Grosso">MT</option>
                    <option value="Maranh�o">MA</option>
                    <option value="Minas Gerais">MG</option>
                    <option value="Par�">PA</option>
                    <option value="Paran�">PR</option>
                    <option value="Para�ba">PB</option>
                    <option value="Pernambuco">PE</option>
                    <option value="Piau�">PI</option>
                    <option value="Rio de Janeiro">RJ</option>
                    <option value="Rio Grande do Norte">RN</option>
                    <option value="Rio Grande do Sul">RS</option>
                    <option value="Roraima">RR</option>
                    <option value="Rond�nia">RO</option>
                    <option value="Santa Catarina">SC</option>
                    <option value="S�o Paulo">SP
                    <option value="Sergipe">SE</option>
                    <option value="Tocantins">TO</option>
                  </select></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Fone:</font></div></td>
                  <td><font color="#0066FF">(
                      <i>
                      <INPUT name="ddd1" class="input" id="ddd1" style="width:25; height:18; border:1px solid #cfcfcf" maxLength="300">
                      </i>      )
                      <i>
                      <input name="fone1" class="input" id="fone1" style="width:85; height:18; border:1px solid #cfcfcf" maxlength="9">
                  </i>                  </font></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Celular:</font></div></td>
                  <td><font color="#0066FF">(
                      <i>
                      <INPUT name="ddd2" class="input" id="ddd2" style="width:25; height:18; border:1px solid #cfcfcf" maxLength="300">
                      </i>      )
                      <i>
                      <input name="fone2" class="input" id="fone2" style="width:85; height:18; border:1px solid #cfcfcf" maxlength="9">
                  </i>                  </font></td>
                </tr>
				               <tr>
                  <td><div align="right"><font color="#0066FF">MSN:</font></div></td>
                  <td><i>
                    <INPUT name="msn" class="input" id="msn" style="width:180; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
			    <tr>
                  <td><div align="right"><font color="#0066FF">E-mail(login):</font></div></td>
                  <td><i>
                    <INPUT name="email" class="input" id="email" style="width:180; height:18; border:1px solid #cfcfcf" maxLength="300">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Senha:</font></div></td>
                  <td><i>
                    <INPUT name="senha1" class="input" id="senha1" style="width:85; height:18; border:1px solid #cfcfcf" maxLength="9">
                  </i></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#0066FF">Confirmar Senha: </font></div></td>
                  <td><i>
                    <INPUT name="senha2" class="input" id="senha2" style="width:85; height:18; border:1px solid #cfcfcf" maxLength="9">
                  </i></td>
                </tr>
                      <tr>
                        <td height="26" colspan="2" width="417"><div align="center"><br><br><b><font color="#0066FF">ATEN��O! Leia os Termos de Uso - Ao se cadastrar estar� automaticamente
                                concordando com os termos descritos.</font></b><font color="#0066FF"><br>
                        </font><br>
                        </div></td>
                      </tr>
                      <tr>
                        <td height="26" colspan="2" width="406"><div align="center">
                          <TEXTAREA name="textarea" cols=40 rows=4 wrap="VIRTUAL" class="input" id="textarea" style="width:600; height:150; border:1px solid #cfcfcf">TERMOS DE USO


ESCREVA AQUI O SEU TEXTO.


Mais informa��es:
(00) 0000-0000
contato@seudominio.com.br


</TEXTAREA>
</span></div></td>
                      </tr>
                <tr>
                <br>
                <br>
                  <td height="26" colspan="2" width="406"><div align="center"><input name="submit" type="image" value="Enviar" src="images/layout/bt_en.gif" width="53" height="16"></div></td>
                </tr>
    </table>
	</form>	</td>
  </tr>
</table>
