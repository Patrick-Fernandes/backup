
<?php
include("../include/header.php");

if (!is_numeric($_GET["cod"])) {
    ?>
    <script language="javascript">
        history.back(-1);
    </script>
    <?php
    }

    $cod = $_GET["cod"];

    if ($_SESSION["login"]->getId() != $cod) {
    ?>
    <script language="javascript">
        alert('Acesso negadis!');
        history.back(-1);
    </script>
    <?php
    }

    //Busca o grupo
    $uDAL = new UsuarioDAL();
    $retorno = $uDAL->buscar("where id=$cod");
    if($retorno)
    {
    $resp = $uDAL->getResp();
    $usuario = $resp[0]; 
    }
    else
    {
    ?>
    <script language="javascript">
        alert("Registro n�o encontrado");
        history.back(-1);
    </script>
    <?php
    }
    $nivel = $usuario->getNivel();
    if($nivel == 1 || $nivel <=1){
    $nome = "Administador";
    }  
    else{
    $nome  = "Usu�rio";}

    ?>

    <header class="w3-container" style="padding-top:22px">
        <h5><b> In�cio</b></h5>
    </header>
    <div class="w3-container w3-white w3-padding-32">
        <div class="w3-row">
            <form method="post" action="../Usuario_Alteracao_2">
                <p class="linhasForm"><label for="txtNome"> Nome:</label>
                    <input type="text" size=40 name="txtNome" id="txtNome" maxlength="20" value="<?= $usuario->getNome() ?>"/></p>

                <p class="linhasForm"><label for="txtLogin"> Login:</label>
                    <input type="text" size=40 name="txtLogin" id="txtLogin" maxlength="20" value="<?= $usuario->getLogin() ?>"/></p>

                <p class="linhasForm"><label for="txtEmail"> E-mail:</label>
                    <input type="text" size=40 name="txtEmail" id="txtEmail" value="<?= $usuario->getEmail() ?>"/></p>

                <p class="linhasForm"><label for="txtSenha"> Cadastre nova senha</label>
                    <input type="password" size=40 name="txtSenha" id="txtSenha" maxlength="40"/><em>(ou digite a senha antiga)</em></p>

                <p class="linhasForm"><label for="txtConfereSenha"> Repita sua senha</label>
                    <input type="password" size=40 name="txtConfereSenha" id="txtConfereSenha" maxlength="40"/></p>

                <input type="hidden" name="cod" id="cod" value="<?= $usuario->getId() ?>"/>
                <input type="submit" value="Alterar" /><input type="reset" value="Limpar" />
            </form>
        </div>
    </div>
    <?php include("../include/footer.php"); ?>
