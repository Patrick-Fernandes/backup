<?
/**
 *  frog::SMS
 *  http://fr0gSMS.codigolivre.org.br
 * 
 *  Autor: Luiz Geovani Vier (vier at brturbo.com - http://vier.ta-na.net)
 *  Data: Maio/2004
 *  Licen�a: GNU GPL - http://www.gnu.org/copyleft/gpl.html
 */

// timeout da execu��o
$execTimeout = 60;

/** 
 * Classe respons�vel pelo envio das mensagems, 
 * obten��o das imagens de confirma��o 
 * e verifica��o de disponibilidade dos servidores.
 * 
 * Baseada na classe SMS criada por Davi Shibayama (dmitsuo@yahoo.com) 
 * e obtida em www.phpbrasil.com
 */
class SMSSender {

  // mensagens de retorno padronizadas
  var $arrMSGRetorno;
  
  // Dados da requisi��o HTTP
  var $respostaHTTP;
  var $cabecalhosHTTP;
  var $corpoHTTP;
  var $proxyList;
  
  /**
   * Construtor
   */
  function SMSSender() {
    
	$this->arrMSGRetorno['manutencao'] = "Erro: O sistema da operadora offline";
	$this->arrMSGRetorno['nroinvalido'] = "Erro: O n�mero de destino n�o foi aceito pela operadora";
	$this->arrMSGRetorno['naoassinante'] = "Erro: SMS n�o habilitado para este tel.";
	$this->arrMSGRetorno['desconhecido'] = "O sistema da operadora retornou erro";
	$this->arrMSGRetorno['repetido'] = "Erro: Mensagem j� enviada anteriormente";
	$this->arrMSGRetorno['cookies'] = "Erro: Cookies desabilitados";
	$this->arrMSGRetorno['check'] = "Erro: C�digo n�o corresponde � imagem";
	$this->arrMSGRetorno['sobrecarga'] = "Servidor da operadora sobrecarregado";
	$this->arrMSGRetorno['erroPing'] = "Sistema da operadora indispon�vel";
	$this->arrMSGRetorno['sucesso'] = "Mensagem enviada com sucesso!";
	
	$this->$proxyList = array(
		'66.119.34.38:8000', '66.119.33.166:80', '170.224.224.133:80', 
		'200.252.140.64:80', '216.148.246.133:80', '193.95.112.71:8080', '66.119.33.166:8000', 
		'66.250.69.3:81', '170.224.224.101:8000', '61.121.224.11:80', '66.250.69.1:81', '170.224.224.101:80', 
		'192.115.106.236:8080', '61.30.47.22:8080', '62.42.228.6:80', '66.119.34.38:80', '192.115.106.236:3128');
	
	//$this->$proxyList = array('localhost:8888');
	// timeout da requisi��o
	global $execTimeout;
	@set_time_limit ($execTimeout);
  } 
  
  /**
   * Fun��o respons�vel pelo envio da mensagem
   */
  function enviar($operadora, $ddd, $fone, $nome, $dddOrigem="", $telOrigem="", $msg, $check="", &$resposta) {
    if (!eregi('^[0-9]{2}$', $ddd)) {
      $resposta = "N�mero DDD inv�lido";
      return false;
    }
    if (!eregi('^[0-9]{8}$', $fone)) {
      $resposta = "N�mero de telefone inv�lido";
      return false;
    }
    if (trim($msg) == '') {
      $resposta = "Mensagem vazia";
      return false;
    }
    if ($operadora == "claro") {
      return $this->_enviarClaro($ddd, $fone, $nome, $dddOrigem, $telOrigem, $msg, $check, $resposta);
    } elseif ($operadora == "vivo-rs") {
      return $this->_enviarVivoRS($ddd, $fone, $nome, $msg, $check, $resposta);
    } elseif ($operadora == "tim") {
      return $this->_enviarTIM($ddd, $fone, $nome, $msg, $check, $resposta);
    } elseif ($operadora == "oi") {
      return $this->_enviarOi($ddd, $fone, $nome, $msg, $check, $resposta);
    } else {
      $resposta = "Erro: Operadora [$operadora] desconhecida";
      return false;
    }
  }
  
  /**
   * Pega do site a imagem de checagem
   * e guarda na sess�o os cookies para o envio da mensagem
   */
  function obterImagemChecagem ($operadora) {
  	global $checkCookie;	
	($operadora) || die ("parametro obrigatorio ausente: operadora");	
  	
  	if ($operadora == "vivo-rs") {
		$url = "http://www.vivo-rs.com.br/consoles/04b_img.asp";
		$data = $this->_http($url);
		$vcookie = $this->_primeiroCookie($this->cabecalhosHTTP);
		
		session_start();
		session_register("checkCookie");
		$checkCookie = "Cookie: {$vcookie[1]}={$vcookie[2]}";
		session_write_close();
		
		header ("Content-type: image/jpeg");
		echo $this->corpoHTTP;
	} else if ($operadora == "tim") {
		$url = "http://servicos.timbrasil.com.br/weblogic/torpedoWeb/smsmt.jsp";
		$this->_httpTP($url);
		
		// inicio pega cookie
		$headers = str_replace("\n", "", str_replace("\r", "", trim($this->cabecalhosHTTP)));
		//echo $headers;
				
		eregi('Set-Cookie: ([^=]+)=(.+)Set-Cookie: ([^=]+)=([^;]+)', $headers, $vcookie);
		/*
		echo count($vcookie);
		for ($i = 0; $i < count($vcookie); $i++) {
			echo "<BR>".$vcookie[$i];
		}
		die();
		*/
		
		session_start();
		session_register("checkCookie");
		$checkCookie = "Cookie: {$vcookie[1]}={$vcookie[2]}\r\nCookie: {$vcookie[3]}={$vcookie[4]}";
		session_write_close();
		//echo $checkCookie;
		// fim pega cookie
		
		// pega SRC da imagem
		$conteudo =  str_replace("\n", "", str_replace("\r", "", trim($this->corpoHTTP)));
		//echo $conteudo;
		
		eregi("<body><img src=\"([^\"]*)", $conteudo, $imgSRC);
		$urlImagem = "http://servicos.timbrasil.com.br/weblogic/torpedoWeb/".$imgSRC[1];
		//echo $urlImagem;
		// fim pega SRC da imagem
		
		$this->_httpTP($urlImagem, "GET", "Referer: $url\r\n$checkCookie\r\n");
		
		header ("Content-type: image/gif");
		echo $this->corpoHTTP;
	} else if ($operadora == "claro") {
		
		$siteURL = "http://www2.claro.com.br/torpedoweb";
		$urlDestino = $siteURL."/clarotw_pwd.asp";
		$postData = array("ddd_para" => "51",
				  "telefone_para" => "92222222",
				  "msg" => "teste",
				  "nome_de" => "tester",
				  "ddd_de" => "",
				  "telefone_de" => "",
				  "caract" => "0"
				 );
		$this->_http($urlDestino, "POST", "Referer: http://www.claro.com.br/portal/site/siteTA/?epi_menuItemID=61dc49d73ce89f4b020fdda1908051a0&epi_menuID=05341fa85af675a8020fdda1908051a0&epi_baseMenuID=05341fa85af675a8020fdda1908051a0&idlocal=52\r\n");
		$conteudo = str_replace("\r\n", "", trim($this->corpoHTTP));
		//echo "<BR>".$this->cabecalhosHTTP."<hr>";
		
		session_start();
		session_register("checkCookie");
		$vcookie = $this->_primeiroCookie($this->cabecalhosHTTP);
		$checkCookie = "Cookie: {$vcookie[1]}={$vcookie[2]}";
		session_write_close();
		//echo "<BR>".$checkCookie."<hr>";
		$er = 'Para enviar seu Torpedo Web, repita o n�mero abaixo no quadro ao lado e clique em enviar: <br><br></td>					  </tr>					  <tr> 				<td align="right">(.*)&nbsp;&nbsp;</td>				<td width="50%"';
		$grab = eregi($er, $conteudo, $figurinhas);
		//echo $grab."------->".$figurinhas[1];
		
		$srcFigurinhas = str_replace("images", $siteURL."/images", trim($figurinhas[1]));
		
		eregi("src=\"([^\"]*)", $srcFigurinhas, $imgSRC);
		
		$this->_http($imgSRC[1], "GET", "Referer: $urlDestino\r\n$checkCookie\r\n");
		//header ("Content-type: image/gif");
		//echo $this->corpoHTTP;
		echo "<html><head><title>check</title></head><body leftmargin=0 topmargin=0>";
		echo $srcFigurinhas."</body></html>";
	} else if ($operadora == "oi") {
		$url = "http://www.oi.com.br/portal2/site/oipessoal/imgauth/imagemsms";
		$data = $this->_http($url);
		$vcookie = $this->_primeiroCookie($this->cabecalhosHTTP);
		
		session_start();
		session_register("checkCookie");
		$checkCookie = "Cookie: {$vcookie[1]}={$vcookie[2]}";
		session_write_close();
		
		header ("Content-type: image/jpeg");
		echo $this->corpoHTTP;
	}
  }
  
  /**
   * Faz uma requisi��o para verificar se o servi�o est� dispon�vel
   */
  function ping ($operadora, &$resposta) {
  	($operadora) || die ("parametro obrigatorio ausente: operadora");
		
	$inicio = time();
	if ($operadora == "claro") {
	
		$urlDestino = "http://www2.claro.com.br/torpedoweb/clarotw_envia.asp";
		$urlDestino .= "?ddd_para=51";
		$urlDestino .= "&telefone_para=92222222";
		$urlDestino .= "&msg=teste";
		$urlDestino .= "&nome_de=tester";
		$urlDestino .= "&ddd_de=";
		$urlDestino .= "&telefone_de=";
		$urlDestino .= "&caract=0";
		$this->_http($urlDestino, "GET", "Referer: http://www2.claro.com.br/torpedoweb/clarotw_pwd.asp\r\n");
		
		$conteudo = str_replace("\r\n", "", strip_tags(trim($this->corpoHTTP)));
		echo "<BR>".$this->cabecalhosHTTP;
		echo "<HR>".$conteudo;
		
		// Verifica se estah mandando para a telinha chata de confirmacao :)
		if (!eregi("Location: clarotw_pwd.asp", $this->cabecalhosHTTP)) {	
                        echo "<BR>ERRO";
			// erro
			if (eregi("server too busy", strtolower($conteudo))) { 
				// servidor sobrecarregado
				$resposta = $this->arrMSGRetorno['sobrecarga'];
				return false;
			} else {
			 	$resposta = $this->arrMSGRetorno['erroPing'];	
				return false;
			}
		}
		
	} elseif ($operadora == "vivo-rs") {
		
		$postData = array("ddd" => "51",
				  "NumeroMovel" => "5100000000",
				  "remetente" => "teste",
				  "msg" => "testando 123",
				  "emoticons" => "",
				  "caracteres" => "0",
				  "temp" => "",
				  "check" => "",
				  "SuportaCookies" => "true"
				 );
	
    		$this->_http("http://www.vivo-rs.com.br/aviso/aviso_envia.asp", "POST", "Referer: http://www.vivo-rs.com.br/consoles/04b.asp\r\n", $postData);
		
		$conteudo = str_replace("\r\n", "", strip_tags(trim($this->cabecalhosHTTP)));
		echo $conteudo;
		
		// Verifica se redirecionou para a pagina de erro (verifica header Location)
		if (!eregi("status=2", $conteudo)) { // erro esperado - imagem de checagem
			$resposta = $this->arrMSGRetorno['erroPing'];	
			return false;
		}
	} elseif ($operadora == "tim") {
		
		$postData = array("prefix" => $ddd,
				  "destNumber" => $fone,
				  "name" => $nome,
				  "message" => $msg,
				  "myNumber" => "",
				  "counter" => "123",
				  "chars" => $check,
				  "site" => "consume",
				  "service" => "smsmt"
				 );
		
    		$this->_http("http://servicos.timbrasil.com.br/weblogic/torpedoWeb/smsmtresult.jsp", "POST", "Referer: http://servicos.timbrasil.com.br/weblogic/torpedoWeb/smsmt.jsp\r\n", $postData);
		
		$conteudo = str_replace("\r\n", "", strip_tags(trim($this->corpoHTTP)));
		echo $conteudo;
		
		// Verifica o retorno
		if (!eregi("Torpedo n&atilde;o pode ser enviado.", $conteudo)) { // erro esperado (nao enviou os cookies nem a checagem
			$resposta = $this->arrMSGRetorno['erroPing'];	
			return false;
		}
	} else if ($operadora == "oi") {
	
		$urlIndex = "http://www.oi.com.br/portal2/site/home/index2.jsp?corbase=rox";
	
		$urlDestino = "http://www.oi.com.br/portal2/site/oipessoal/send_sms.jsp";
		$urlDestino .= "?ddd=$ddd";
		$urlDestino .= "&numero=$fone";
		$urlDestino .= "&texto=$msg";
		$urlDestino .= "&ass=$nome";
		$urlDestino .= "&imgcod=$check";
		$urlDestino .= "&urlfrom=../home/index2.jsp?corbase=rox";
		$this->_http($urlDestino, "GET", "Referer: $urlIndex\r\n$cookies\r\n");
		
		$conteudo = str_replace("\r\n", "", strip_tags(trim($this->cabecalhosHTTP)));
		echo "<HR>".$conteudo;
		
		// Verifica se estah mandando de volta pra pg inicial
		if (!eregi("index2.jsp", $this->cabecalhosHTTP)) {	
                        echo "<BR>ERRO";
			// erro
			if (eregi("server too busy", strtolower($conteudo))) { 
				// servidor sobrecarregado
				$resposta = $this->arrMSGRetorno['sobrecarga'];
				return false;
			} else {
			 	$resposta = $this->arrMSGRetorno['erroPing'];	
				return false;
			}
		}
	} else {
		$resposta = "Operadora [$operadora] desconhecida";	
		return false;
	}
	$segs = (time() - $inicio);
	$resposta = "Dispon�vel";// (".$segs."s)";
	return true;
  }

  /**
   * Fuck-mode version
   * Isso aqui t� uma zona mas vai ficar assim mesmo por enquanto
   */
  function _enviarClaro($ddd, $fone, $nome, $dddOrigem, $telOrigem, $msg, $check, &$mensagem) {

$req = "POST /torpedoweb/clarotw_pwd.asp HTTP/1.0
User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.51  [en]
Host: www2.claro.com.br
Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1
Accept-Charset: iso-8859-1, utf-8, utf-16, *;q=0.1
Accept-Encoding: deflate, gzip, x-gzip, identity, *;q=0
Referer: http://www.claro.com.br/portal/site/siteTA/?epi_menuItemID=61dc49d73ce89f4b020fdda1908051a0&epi_menuID=05341fa85af675a8020fdda1908051a0&epi_baseMenuID=05341fa85af675a8020fdda1908051a0&idlocal=52
Content-Type: application/x-www-form-urlencoded
Content-Length: 90
";
	$this->_http_simple_req("http://www2.claro.com.br", "POST", $req, "ddd_para=$ddd&telefone_para=$fone&nome_de=".UrlEncode($nome)."&ddd_de=$dddOrigem&telefone_de=$telOrigem&msg=".UrlEncode($msg)."&caract=125");
	$vcookie = $this->_primeiroCookie($this->cabecalhosHTTP);
	$checkCookie = "Cookie: {$vcookie[1]}={$vcookie[2]}";

	$checkClaro = "";
	$conteudo = str_replace("\r\n", "", trim($this->corpoHTTP));

	$er = 'Para enviar seu Torpedo Web, repita o n�mero abaixo no quadro ao lado e clique em enviar: <br><br></td>					  </tr>					  <tr> 				<td align="right">(.*)&nbsp;&nbsp;</td>				<td width="50%"';
	$grab = eregi($er, $conteudo, $figurinhas);

	$srcFigurinhas = trim($figurinhas[1]);
	
	for($i=0; $i < 4; $i++){
		
		$srcFigurinhas = substr($srcFigurinhas, strpos($srcFigurinhas,"img") + 4);
		
		eregi("src=\"([^\"]*)", $srcFigurinhas, $imgSRC);
		$img = $imgSRC[1];
		
$req = "GET /torpedoweb/$img HTTP/1.0
User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.51  [en]
Host: www2.claro.com.br
Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1
Accept-Charset: iso-8859-1, utf-8, utf-16, *;q=0.1
Accept-Encoding: deflate, gzip, x-gzip, identity, *;q=0
Referer: http://www2.claro.com.br/torpedoweb/clarotw_pwd.asp
$checkCookie
Cookie2: $Version=1
";
		$this->_http_simple_req("http://www2.claro.com.br", "GET", $req);
		$checkClaro .= $this->_decodeClaroImg($this->corpoHTTP);
	}

	//echo "[$checkClaro]";
	
$req = "GET /torpedoweb/clarotw_envia.asp?tx_senha=$checkClaro&ddd_para=$ddd&telefone_para=$fone&nome_de=".UrlEncode($nome)."&ddd_de=$dddOrigem&telefone_de=$telOrigem&msg=".UrlEncode($msg)."&x=29&y=11 HTTP/1.0
User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.51  [en]
Host: www2.claro.com.br
Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1
Accept-Charset: iso-8859-1, utf-8, utf-16, *;q=0.1
Accept-Encoding: deflate, gzip, x-gzip, identity, *;q=0
Referer: http://www2.claro.com.br/torpedoweb/clarotw_pwd.asp
$checkCookie
Cookie2: $Version=1
";
	$this->_http_simple_req("http://www2.claro.com.br", "GET", $req);
	//echo $this->cabecalhosHTTP;
	//echo "<HR>";
	//echo $this->corpoHTTP;
	//echo "</font>";
	$resposta = $this->corpoHTTP;
	
    // Verifica se existe a string de confirma��o de envio da mensagem no texto da p�gina de resposta
    if (eregi("1000", $resposta) || eregi("1001", $resposta) || eregi("8000", $resposta)) {
	  $mensagem = $this->arrMSGRetorno['manutencao'];
      return false;
    } elseif (eregi("8001", $resposta)) {
	  $mensagem = $this->arrMSGRetorno['naoassinante'];
      return false;
    } elseif (eregi("7000", $resposta)) {
	  $mensagem = $this->arrMSGRetorno['check'];
      return false;
    } elseif (eregi("server too busy", strtolower($resposta))) {
	  $mensagem = $this->arrMSGRetorno['sobrecarga'];
      return false;
    }elseif (eregi("parent.Retorno\(0\)", $resposta)) {
	  $mensagem = $this->arrMSGRetorno['sucesso'];
      return true;
    } else {
      $mensagem = $this->arrMSGRetorno['desconhecido'];
      return false;
    }
  }

  function _enviarVivoRS($ddd, $fone, $nome, $msg, $check, &$mensagem) {
  	
  	global $checkCookie;
	session_start();
	if (!session_is_registered("checkCookie")) {
		$mensagem = $this->arrMSGRetorno['cookies'];
		return false;
	}
	$cookies = $checkCookie;
	session_unregister("checkCookie");
	session_write_close();
	
	$postData = array("ddd" => $ddd,
				  "NumeroMovel" => $ddd.$fone,
				  "remetente" => $nome,
				  "msg" => $msg,
				  "emoticons" => "",
				  "caracteres" => "0",
				  "temp" => "",
				  "check" => $check,
				  "SuportaCookies" => "true"
				 );
    $this->_httpTP("http://www.vivo-rs.com.br/aviso/aviso_envia.asp", "POST", "Referer: http://www.vivo-rs.com.br/consoles/04b.asp\r\n$cookies\r\n", $postData);
	
    $resposta = str_replace("\r\n", "", strip_tags(trim($this->cabecalhosHTTP)));
    // Verifica se redirecionou para a pagina de sucesso (header Location)
    if (eregi("status=1", $resposta)) {
      $mensagem = $this->arrMSGRetorno['sucesso'];
      return true;
    } elseif (eregi("status=2", $resposta)) {
      $mensagem = $this->arrMSGRetorno['check'];
      return false;
    } else {
      $mensagem = $this->arrMSGRetorno['desconhecido'];
      return false;
    }
  }
  
  function _enviarTIM($ddd, $fone, $nome, $msg, $check, &$mensagem) {
  	
  	global $checkCookie;
	session_start();
	if (!session_is_registered("checkCookie")) {
		$mensagem = $this->arrMSGRetorno['cookies'];
		return false;
	}
	$cookies = $checkCookie;
	session_unregister("checkCookie");
	session_write_close();
	
	$postData = array("prefix" => $ddd,
				  "destNumber" => $fone,
				  "name" => $nome,
				  "message" => $msg,
				  "myNumber" => "",
				  "counter" => "123",
				  "chars" => $check,
				  "site" => "consume",
				  "service" => "smsmt"
				 );
	
    $this->_httpTP("http://servicos.timbrasil.com.br/weblogic/torpedoWeb/smsmtresult.jsp", "POST", "Referer: http://servicos.timbrasil.com.br/weblogic/torpedoWeb/smsmt.jsp\r\n$cookies\r\n", $postData);
	
	//echo $this->cabecalhosHTTP;
	//echo "<BR>FIM HEAD<BR><br>";
    $resposta = str_replace("\r\n", "", strip_tags(trim($this->corpoHTTP)));
	//echo $resposta;
	//echo "<BR>FIM BODY<BR><br>";
    // Verifica o retorno

    if (eregi("torpedo enviado com sucesso", $resposta)) {
      $mensagem = $this->arrMSGRetorno['sucesso'];
      return true;
    } elseif (eregi("O n&uacute;mero de destino n&atilde;o &eacute; v&aacute;lido", $resposta)) {
      $mensagem = $this->arrMSGRetorno['nroinvalido'];
      return false;
    } elseif (eregi("O n&uacute;mero de seguran&ccedil;a digitado n&atilde;o &eacute; v&aacute;lido", $resposta)) {
      $mensagem = $this->arrMSGRetorno['check'];
      return false;
    } else {
      $mensagem = $this->arrMSGRetorno['desconhecido'];
      return false;
    }
  }

  function _enviarOi($ddd, $fone, $nome, $msg, $check, &$mensagem) {
  	
  	global $checkCookie;
	session_start();
	if (!session_is_registered("checkCookie")) {
		$mensagem = $this->arrMSGRetorno['cookies'];
		return false;
	}
	$cookies = $checkCookie;
	session_unregister("checkCookie");
	session_write_close();
	
	$urlIndex = "http://www.oi.com.br/portal2/site/home/index2.jsp?corbase=rox";
	
	$urlDestino = "http://www.oi.com.br/portal2/site/oipessoal/send_sms.jsp";
	$urlDestino .= "?ddd=$ddd";
	$urlDestino .= "&numero=$fone";
	$urlDestino .= "&texto=$msg";
	$urlDestino .= "&ass=$nome";
	$urlDestino .= "&imgcod=$check";
	$urlDestino .= "&urlfrom=../home/index2.jsp?corbase=rox";
	$this->_http($urlDestino, "GET", "Referer: $urlIndex\r\n$cookies\r\n");
	
	
	$this->_http($urlIndex, "GET", "Referer: $urlDestino\r\n$cookies\r\n");
	
    $resposta = $this->corpoHTTP;
    //echo $resposta;
    // Verifica se redirecionou para a pagina de sucesso (header Location)
    if (eregi("Mensagem enviada com sucesso", $resposta)) {
      $mensagem = $this->arrMSGRetorno['sucesso'];
      return true;
    } elseif (eregi("O c�digo informado n�o corresponde � imagem", $resposta)) {
      $mensagem = $this->arrMSGRetorno['check'];
      return false;
    } elseif (eregi("Voc� s� pode enviar mensagens pelo Portal para n�meros Oi", $resposta)) {
      $mensagem = $this->arrMSGRetorno['nroinvalido'];
      return false;
    } else {
      $mensagem = $this->arrMSGRetorno['desconhecido'];
      return false;
    }
  }

  /**
   * Retorna o primeiro cookie dos headers http
   */
  function _primeiroCookie($head) {
    $head = str_replace("\r\n", "", $head);
    eregi('Set-Cookie: ([^=]+)=([^;]+)', $head, $vcookie);
    return $vcookie;
  }
  
  /**
   *   Implementa uma requisi��o HTTP via socket  
   *   http://www.spencernetwork.org/memo/tips-3.php
   */
  function _http($url, $method="GET", $headers="", $post=array("")) {
    
	$URL = parse_url($url);
    if (isset($URL['query'])) {
      $URL['query'] = "?".$URL['query'];
    } else {
      $URL['query'] = "";
    }
    
	if (!isset($URL['port'])) $URL['port'] = 80;
    
	$request  = $method." ".$URL['path'].$URL['query']." HTTP/1.0\r\n";
    $request .= "Host: ".$URL['host']."\r\n";
    $request .= "User-Agent: PHP/".phpversion()."\r\n";
    if (isset($URL['user']) && isset($URL['pass'])) {
      $request .= "Authorization: Basic " . base64_encode($URL['user'] . ":" . $URL['pass']) . "\r\n";
    }
    $request .= $headers;
    
	if (strtoupper($method) == "POST") {
      while (list($name, $value) = each($post)) {
        $POST[] = $name . "=" . urlencode($value);
      }
      $postdata = implode("&", $POST);
      $request .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $request .= "Content-Length: " . strlen($postdata) . "\r\n";
      $request .= "\r\n";
      $request .= $postdata;
    } else {
      $request .= "\r\n";
    }
    
	$fp = fsockopen($URL['host'], $URL['port']);
    if (!$fp) {
      die("N�o foi poss�vel estabelecer uma conex�o com o servidor.\n");
    }    
    
    fputs($fp, $request); 
    $response = "";
	
    while (!feof($fp)) {
      $response .= fread($fp, 4096);
    }
    
	fclose($fp);
	
	$this->respostaHTTP = $response;
	$DATA = split("\r\n\r\n", $response, 2);
	$this->cabecalhosHTTP = $DATA[0];
    $this->corpoHTTP = $DATA[1];    
    return $DATA;
  }

	/**
   *   Implementa uma requisi��o HTTP via socket  
   *   http://www.spencernetwork.org/memo/tips-3.php
   */
  function _http_simple_req($url, $method="GET", $headers, $postdata="") {
    
	$URL = parse_url($url);
    if (isset($URL['query'])) {
      $URL['query'] = "?".$URL['query'];
    } else {
      $URL['query'] = "";
    }
    
	if (!isset($URL['port'])) $URL['port'] = 80;
    
    $request = $headers;
    
    if (strtoupper($method) == "POST") {
      $request .= "\r\n";
      $request .= $postdata;
    } else {
      $request .= "\r\n";
    }
    
	$fp = fsockopen($URL['host'], $URL['port']);
    if (!$fp) {
      die("N�o foi poss�vel estabelecer uma conex�o com o servidor.\n");
    }    
    
    fputs($fp, $request); 
    $response = "";
	
    while (!feof($fp)) {
      $response .= fread($fp, 4096);
    }
    
	fclose($fp);
	
	$this->respostaHTTP = $response;
	$DATA = split("\r\n\r\n", $response, 2);
	$this->cabecalhosHTTP = $DATA[0];
    $this->corpoHTTP = $DATA[1];    
    return $DATA;
  }
	
  /**
   *   Implementa uma requisi��o HTTP via socket usando proxy
   */
  function _httpTP($url, $method="GET", $headers="", $post=array("")) {
    
	$URL = parse_url($url);
    if (isset($URL['query'])) {
      $URL['query'] = "?".$URL['query'];
    } else {
      $URL['query'] = "";
    }
    
	if (!isset($URL['port'])) $URL['port'] = 80;
    
	$request  = "$method  $url HTTP/1.0\r\n";
    $request .= "Host: ".$URL['host']."\r\n";
    $request .= "User-Agent: PHP/".phpversion()."\r\n";
    if (isset($URL['user']) && isset($URL['pass'])) {
      $request .= "Authorization: Basic " . base64_encode($URL['user'] . ":" . $URL['pass']) . "\r\n";
    }
    $request .= $headers;
    
	if (strtoupper($method) == "POST") {
      while (list($name, $value) = each($post)) {
        $POST[] = $name . "=" . urlencode($value);
      }
      $postdata = implode("&", $POST);
      $request .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $request .= "Content-Length: " . strlen($postdata) . "\r\n";
      $request .= "\r\n";
      $request .= $postdata;
    } else {
      $request .= "\r\n";
    }
    $c = 0;
    $fp = 0;
    while (!$fp && $c < 10) {
	$c++;
	$randProxy = rand(0,count($this->$proxyList)-1);
	//$randProxy = 0;
	//echo $randProxy;
	$pl = $this->$proxyList;
	$proxy = $pl[$randProxy];
	$URL = parse_url($proxy);
	if (!isset($URL['port'])) $URL['port'] = 80;
	//echo $URL['host']."<BR>";
	//flush();
	$fp = @fsockopen($URL['host'], $URL['port'], $errno, $errstr, 2);
	//break;
    } 
    if (!$fp) die("N�o conseguiu conectar ao proxy");
    
    fputs($fp, $request); 
    $response = "";
	
    while (!feof($fp)) {
      $response .= fread($fp, 4096);
    }
    
	fclose($fp);
	
	$this->respostaHTTP = $response;
	$DATA = split("\r\n\r\n", $response, 2);
	$this->cabecalhosHTTP = $DATA[0];
    $this->corpoHTTP = $DATA[1];    
    return $DATA;
  }

  /**
   * origem caracteresXnumeros: http://gtksms.codigolivre.org.br
   */
  function _decodeClaroImg($img) {
	$c = ord(substr($img,58,1));
	switch ($c) {
		case 73:
			return 0;
		case 57:
			return 1;
		case 56:
			return 2;
		case 244:
			return 3;
		case 21:
			return 4;
		case 178:
			return 5;
		case 134:
			return 6;
		case 168:
			return 7;
		case 104:
			return 8;
		case 153:
			return 9;
		default:
			return -1;
	}
  }


}
?>
