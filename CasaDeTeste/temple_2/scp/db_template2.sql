-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30-Ago-2017 às 20:12
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_template2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bolsista`
--

CREATE TABLE `bolsista` (
  `id` int(11) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `rg` varchar(15) NOT NULL,
  `funcao` varchar(40) NOT NULL,
  `status` varchar(20) NOT NULL,
  `nit` varchar(15) NOT NULL,
  `siape` varchar(10) NOT NULL,
  `nacionalidade` varchar(45) NOT NULL,
  `municipio` varchar(50) NOT NULL,
  `nascimento` date NOT NULL,
  `banco` varchar(60) NOT NULL,
  `agencia` varchar(10) NOT NULL,
  `conta_bancaria` varchar(30) NOT NULL,
  `municipio_de_atuacao` varchar(50) NOT NULL,
  `logradouro` text NOT NULL,
  `cep` varchar(9) NOT NULL,
  `telefone` varchar(16) NOT NULL,
  `email` varchar(100) NOT NULL,
  `curso` varchar(80) NOT NULL,
  `disciplina` varchar(30) NOT NULL,
  `carga_horaria` int(5) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_fim` date NOT NULL,
  `programa` varchar(30) NOT NULL,
  `id_usuario` int(3) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `documento`
--

CREATE TABLE `documento` (
  `id` int(5) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `id_usuario` int(3) NOT NULL,
  `link` varchar(250) NOT NULL,
  `status` varchar(10) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `documento`
--

INSERT INTO `documento` (`id`, `nome`, `id_usuario`, `link`, `status`, `data`) VALUES
(2154, 'abc.doc', 1, 'cba3ef0811abc.doc', 'ativo', '2017-04-04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nivel` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`, `email`, `nivel`, `status`, `cadastro`) VALUES
(1, 'Luis Fernando Mendes', 'npte.luis', '40ebd3f80becbf03234fe0e7d4b60ae8', 'lufemendes@gmail.com', 1, 1, '0000-00-00 00:00:00'),
(50, 'Suporte', 'npte.suporte', '8a543243179cb1d2b67c3d36d1d51c9a', 'npte.suporte@gmail.com', 1, 1, '2017-02-23 10:28:38'),
(51, 'Mauro Anjos', 'npte.mauro', 'de25fee33be832ccb179cd221880260e', 'npte.mauro@gmail.com', 1, 1, '2017-03-08 10:26:05'),
(52, 'Administrador', 'administrador', 'cacf7db59ef5935664dd65dc2c455d04', 'isabelmoreira@gmail.com', 1, 1, '2017-03-09 22:31:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bolsista`
--
ALTER TABLE `bolsista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bolsista`
--
ALTER TABLE `bolsista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `documento`
--
ALTER TABLE `documento`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2155;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `bolsista`
--
ALTER TABLE `bolsista`
  ADD CONSTRAINT `bolsista_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
