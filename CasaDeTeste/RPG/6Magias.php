
<div class="col-sm-6  col-lg-6 col-xs-12">
    <h3>Magias</h3>      
    <h4>0º CIRCULO</h4>
    <h5>CD <?= 10 + 0 + $car ?> </h5>
    <script>
        $(document).ready(function () {
<?PHP for ($i = 0; $i <= 4; $i++) { ?>
                $("#MAGIAA0<?= $i ?>").click(function (evento) {
                    if ($("#MAGIAA0<?= $i ?>").attr("checked")) {
                        $("#MAGIA0<?= $i ?>").css("display", "block");
                    } else {
                        $("#MAGIA0<?= $i ?>").css("display", "none");
                    }
                });
<?PHP } ?>
        });
    </script>
    <ul class="list-group">
        <li class="list-group-item">
            DETECTAR VENENOS   <input type="checkbox" value="1" id="MAGIAA01">
            <ul class="list-group" id="MAGIA01" style="display: none;">
                <li class="list-group-item">
                    Você percebe se uma criatura ou objeto é venenoso ou está envenenado. 
                    Você pode fazer um teste de Ofício (alquimia,CD 20) para determinar o tipo de veneno.
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            LER MAGIAS <input type="checkbox" value="1" id="MAGIAA02">
            <ul class="list-group" id="MAGIA02" style="display: none;">
                <li class="list-group-item">
                    Você pode decifrar inscrições mágicas em pergaminhos, livros, armas, etc.
                    Normalmente esse processo não ativa a magia contida na escrita,
                    mas em alguns casos isso pode ocorrer (por exemplo, com um pergaminho amaldiçoado).
                    Ler magias permite identifcar uma magia do tipo símbolo (como símbolo de proteção) 
                    com um teste de Identifcar Magia (CD 20 + nível da magia).
                    <BR>Duração 1min
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            MENSAGEM <input type="checkbox" value="1" id="MAGIAA03">
            <ul class="list-group" id="MAGIA03" style="display: none;">
                <li class="list-group-item">
                    Você e o alvo podem trocar mensagens telepaticamente.
                    Mensagem transmite som, não signifcado — 
                    ou seja, você e o alvo devem falar um idioma comum para se entenderem
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            RESISTENCIA <input type="checkbox" value="1" id="MAGIAA04">
            <ul class="list-group" id="MAGIA04" style="display: none;">
                <li class="list-group-item">
                    O alvo recebe +1 em testes de resistência
                    <Br>Duração 1min
                </li>
            </ul>
        </li>
    </ul>
    <!--===========================================================================================================-->
    <h4>1º CIRCULO</h4>
    <h5>CD <?= 10 + 1 + $car ?> </h5>
    <script>
        $(document).ready(function () {
<?PHP for ($i = 0; $i <= 5; $i++) { ?>
                $("#MAGIAA1<?= $i ?>").click(function (evento) {
                    if ($("#MAGIAA1<?= $i ?>").attr("checked")) {
                        $("#MAGIA1<?= $i ?>").css("display", "block");
                    } else {
                        $("#MAGIA1<?= $i ?>").css("display", "none");
                    }
                });
<?PHP } ?>
        });
    </script>
    <ul class="list-group">
        <li class="list-group-item">
            arma elemental  <input type="checkbox" value="1" id="MAGIAA11">
            <ul class="list-group" id="MAGIA11" style="display: none;">
                <li class="list-group-item">
                    Esta magia concede o poder especial congelante (+1d6 de dano de frio),
                    elétrica (+1d6 de dano de eletricidade) ou fl amejante(+1d6 de dano de fogo)
                    à arma afetada
                    (Tormenta RPG, páginas 243 a 244). 
                    Arma elemental pode ser lançada sobre armas
                    que já possuem o poder escolhido. 
                    Nesse caso, os efeitos se acumulam, para um total de +2d6 pontos de dano.
                    <br>
                    Duração 1min
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            ATK CERTEIRO <input type="checkbox" value="1" id="MAGIAA12">
            <ul class="list-group" id="MAGIA12" style="display: none;">
                <li class="list-group-item">
                    Você adquire uma compreensão do futuro próximo, que fornece um bônus de +20 na sua próxima jogada de ataque. 
                    Esse ataque também não é afetado pela chance de falha de alvos camuﬂados. 
                    A jogada de ataque deve ser feita até o fm do seu próximo turno
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            MONTARIA ARCANA <input type="checkbox" value="1" id="MAGIAA13">
            <ul class="list-group" id="MAGIA13" style="display: none;">
                <li class="list-group-item">
                    Você invoca um cavalo ou pônei para servir como sua montaria. A montaria não é treinada para guerra
                    <br>duração 1dia
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            TOQUE CHOCANTE <input type="checkbox" value="1" id="MAGIAA14">
            <ul class="list-group" id="MAGIA14" style="display: none;">
                <li class="list-group-item">
                    Raios elétricos envolvem sua mão. Faça um ataque de toque desarmado. 
                    Se acertar, o alvo sofre 2d8 pontos de dano.
                    Se o alvo usa armadura de metal (ou carrega muito metal, de acordo com o mestre), 
                    você recebe +3 na jogada de ataque
                    <br>
                    +2 de dano por causa de ser kobold
                </li>
            </ul>
        </li>
        <li class="list-group-item">
            DISCO Flutuante  <input type="checkbox" value="1" id="MAGIAA15">
            <ul class="list-group" id="MAGIA15" style="display: none;">
                <li class="list-group-item">
                    Você cria um plano de energia côncavo que carrega até 100kg. O disco segue o conjurador com deslocamento de 9m,
                    ﬂutuando 1m acima do chão.
                    O disco desaparece se você se deslocar mais rápido e ele sair do alcance. 
                    Se o disco desaparecer, tudo que ele está carregando cai no chão.
                    <br>Duração: 1 dia
                </li>
            </ul>
        </li>
    </ul>
</div>