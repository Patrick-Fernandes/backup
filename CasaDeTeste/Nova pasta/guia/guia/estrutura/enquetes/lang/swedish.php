<?php

/* Charset */
$lang["charset"]   = "iso-8859-1";

/* General */
$lang["Logout"]    = "Logga Ut";
$lang["FormUndo"]  = "G�r inga �ndringar";
$lang["FromClear"] = "Rensa";
$lang["FormEnter"] = "Var god och skriv r�tt anv�ndarnamn och l�senord";
$lang["FormWrong"] = "Fel anv�ndarnamn och l�senord";
$lang["FormOK"]    = "OK";
$lang["Updated"]   = "�ndringarna har uppdaterats!";
$lang["NoUpdate"]  = "Ett fel har uppst�tt! Inga �ndringar har gjorts!";
$lang["Confirm"]   = "�r du s�ker?";
$lang["NavNext"]   = "N�sta sida";
$lang["NavPrev"]   = "F�reg�ende sida";
$lang["License"]   = "Licens avtal";
$lang["ScrollTxt"] = "Tryck p� PAGE DOWN knappen f�r att l�sa resten av avtalet.";

/* Templates */
$lang["Templates"]  = "Mallar";
$lang["tpl_exist"]  = "Namnet p� mallen existerar redan.";
$lang["tpl_new"]    = "L�gg till en ny upps�ttning av mallar.";
$lang["tpl_succes"] = "Mallen lades till utan problem!"; 
$lang["tpl_bad"]    = "Namnet p� mallen �r ogiltig!";
$lang["tpl_save"]   = "Spara";
$lang["preview"]    = "F�rhandsgranska";
$lang["newtpl"]     = "Ny Upps�ttning Av Mallar";

/* Poll List */
$lang["IndexTitle"]  = "Lista �ver alla omr�stningar";
$lang["IndexQuest"]  = "Fr�ga";
$lang["IndexID"]     = "Omr�stnings ID";
$lang["IndexDate"]   = "Datum";
$lang["IndexDays"]   = "Dagar";
$lang["IndexExp"]    = "F�rfaller";
$lang["IndexExpire"] = "f�rfallit";
$lang["IndexNever"]  = "aldrig";
$lang["IndexStat"]   = "Statistik";
$lang["IndexCom"]    = "Kommentar";
$lang["IndexAct"]    = "�tg�rd";
$lang["IndexDel"]    = "radera";

/* Create A New Poll */
$lang["NewTitle"]  = "Skapa en ny omr�stning";
$lang["NewOption"] = "Val";
$lang["NewNoQue"]  = "Du gl�mde att fylla i f�ltet f�r fr�gan";
$lang["NewNoOpt"]  = "Du gl�mde att fylla i val f�ltet";

/* Poll Edit */
$lang["EditStat"]  = "Status";
$lang["EditText"]  = "�ndra denna omr�stning";
$lang["EditReset"] = "Starta denna omr�stning p� nytt";
$lang["EditOn"]    = "p�";
$lang["EditOff"]   = "av";
$lang["EditHide"]  = "g�md";
$lang["EditLgOff"] = "logga ut";
$lang["EditLgOn"]  = "logga in";
$lang["EditAdd"]   = "L�gg till val";
$lang["EditNo"]    = "Inga val har lagts till!";
$lang["EditOk"]    = "Val har lagts till!";
$lang["EditSave"]  = "Spara �ndringar";
$lang["EditOp"]    = "Du m�ste ha minst tv� val med!";
$lang["EditMis"]   = "Fr�ga och val �r inte exakt!";
$lang["EditDel"]   = "F�r att ta bort ett val se till att rutan �r tom";
$lang["EditCom"]   = "Till�t kommentarer";

/* General Settings */
$lang["SetTitle"]   = "Huvud Inst�llningar";
$lang["SetOption"]  = "Tabell, Font/Teckensnitt och F�rg val";
$lang["SetMisc"]    = "Blandat";
$lang["SetText"]    = "�ndra huvud Inst�llningarna";
$lang["SetURL"]     = "URL till bild katalogen";
$lang["SetBURL"]    = "URL till omr�stnings katalogen";
$lang["SetNo"]      = "Ingen trailing slash";
$lang["SetLang"]    = "Spr�k";
$lang["SetPoll"]    = "Rubrik/Titel p� omr�stningen";
$lang["SetButton"]  = "R�st knapp";
$lang["SetResult"]  = "Resultat l�nk";
$lang["SetVoted"]   = "Har redan r�stat";
$lang["SetComment"] = "Skicka din kommentar";
$lang["SetTab"]     = "Tabell bredd";
$lang["SetBarh"]    = "Bar h�jd";
$lang["SetBarMax"]  = "Max. bar l�ngd";
$lang["SetTabBg"]   = "Bakgrunds f�rg p� tabellen";
$lang["SetFrmCol"]  = "Frame/Ram f�rg";
$lang["SetFontCol"] = "Font/Teckensnitt f�rg";
$lang["SetFace"]    = "Font/Teckensitt face";
$lang["SetShow"]    = "Visa resultat i";
$lang["SetPerc"]    = "procent";
$lang["SetVotes"]   = "r�ster";
$lang["SetCheck"]   = "Kontroll";
$lang["SetNoCheck"] = "ingen kontroll";
$lang["SetIP"]      = "IP tabell";
$lang["SetTime"]    = "locking timeout";
$lang["SetHours"]   = "timmar";
$lang["SetOffset"]  = "Server time offset";
$lang["SetEntry"]   = "Visa kommentarer per sida";
$lang["SetSubmit"]  = "OK/Verkst�ll";
$lang["SetEmpty"]   = "Ej godk�nt v�rde";
$lang["SetSort"]    = "Visa i ordning";
$lang["SetAsc"]     = "stigande";
$lang["SetDesc"]    = "fallande";
$lang["Setusort"]   = "sortera inte";
$lang["SetOptions"] = "Antal val till�tna i nya omr�stningar";
$lang["SetPolls"]   = "Omr�stningar per sida";

/* Change Password */
$lang["PwdTitle"] = "�ndra L�senord";
$lang["PwdText"]  = "�ndra anv�ndarnamnet eller l�senordet";
$lang["PwdUser"]  = "Anv�ndarnamn";
$lang["PwdPass"]  = "L�senord";
$lang["PwdConf"]  = "Bekr�fta L�senordet";
$lang["PwdNoUsr"] = "Du gl�mde att fylla i f�ltet f�r anv�ndarnamnet";
$lang["PwdNoPwd"] = "Du gl�mde att fylla i l�senords f�ltet";
$lang["PwdBad"]   = "L�senordet st�mmer inte";

/* Poll Stats */
$lang["StatCrea"]  = "Skapad";
$lang["StatAct"]   = "Aktiv";
$lang["StatReset"] = "Nollst�ll logg statistiken";
$lang["StatDis"]   = "loggning �r inte i bruk f�r denna omr�stning";
$lang["StatTotal"] = "Antal r�ster";
$lang["StatDay"]   = "r�ster per dag";

/* Poll Comments */
$lang["ComTotal"]  = "Antal kommentarer totalt";
$lang["ComName"]   = "Namn";
$lang["ComPost"]   = "skickat";
$lang["ComDel"]    = "�r du s�ker att du vill radera detta meddelande?";

/* Help */
$lang["Help"]       = "Hj�lp";
$lang["HelpPoll"]   = "F�r att l�gga in denna omr�stning p� en webbsida s�tt in koden h�r nedan";
$lang["HelpRand"]   = "Det �r ocks� m�jligt att visa en omr�stning slumpvis";
$lang["HelpNew"]    = "Visa alltid senaste omr�stningen";
$lang["HelpSyntax"] = "Syntax";

/* Days */
$weekday[0] = "S�ndag";
$weekday[1] = "M�ndag";
$weekday[2] = "Tisdag";
$weekday[3] = "Onsdag";
$weekday[4] = "Torsdag";
$weekday[5] = "Fredag";
$weekday[6] = "L�rdag";

/* Months */
$months[0]  = "Januari";
$months[1]  = "Februari";
$months[2]  = "Mars";
$months[3]  = "April";
$months[4]  = "Maj";
$months[5]  = "Juni";
$months[6]  = "Juli";
$months[7]  = "Augusti";
$months[8]  = "September";
$months[9]  = "Oktober";
$months[10] = "November";
$months[11] = "December";

/* Colors */
$color_array[0]  = "aqua";
$color_array[1]  = "bl�";
$color_array[2]  = "brun";
$color_array[3]  = "m�rkgr�n";
$color_array[4]  = "guld";
$color_array[5]  = "gr�n";
$color_array[6]  = "gr�";
$color_array[7]  = "orange";
$color_array[8]  = "rosa";
$color_array[9]  = "lila";
$color_array[10] = "r�d";
$color_array[11] = "gul";

?>