
<div class="col-sm-6  col-lg-6 col-xs-12">
    <h3>Pericias</h3>
    <table class="table">
        <thead>
            <tr>
                <th><b>Pericia </b></th>
                <th><b>Total</b></th>
                <th><b>GRAD</b></th>
                <th><b>MOD HABIL</b></th>
                <th><b>OUTRO</b></th>
                <th><b>Deligente 
                        <span title="Benefício: você pode gastar uma ação de movimento para se
                              concentrar na tarefa à frente. Se fizer isso, recebe +2 nos testes de
                              perícia realizados até a próxima rodada." > 
                            ?
                        </span></b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>ACROBACIA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $des)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $des ?>DES</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>ADESTRAR ANIMAIS-somente treinada-</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
                <th></th>
            </tr>
            <tr>
                <th>ATLETISMO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $con)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $con ?>FOR</th>
                <th>0</th>
                <th>2</th>
                <th></th>
            </tr>
            <tr>
                <th>ATUAÇÃO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>CAVALGAR</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $des) ." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>" ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $des ?>DES</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ CONHECIMENTO - ARCANO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal) + 3 + $int)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal) + 3 ?></th>
                <th><?= $int ?>INT</th>
                <th>0</th>
                <th>2</th>
            </tr>
                  <tr>
                <th>$$ CONHECIMENTO - Religião</th>
                <th><b><?= $TOTAL = (floor($nivelTotal) + 3 + $int)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal) + 3 ?></th>
                <th><?= $int ?>INT</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>CURA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $sab)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $sab ?>SAB</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>DIPLOMACIA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>" ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ EMGAMAÇÃO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>" ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ FURTIVIDADE</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $des + 4)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $des ?>DES</th>
                <th><span title="PEQUENO" style="color: blue">4</span></th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ IDENTIFICAR MAGIA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $int)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $int ?>INT</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ INICIATIVA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $des)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $des ?>DES</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ INTIMIDAÇÃO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>" ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>INTUIÇÃO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $sab)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $sab ?>SAB</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>JOGATINA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ LADINAGEM</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $des+2)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $des ?>DES</th>
                <th><span title="Kit de Ladrão obra prima" style="color: blue">2</span></th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ OBTER INFORMAÇÃO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $car)." +2 <span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $car ?>CAR</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>OFICIO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $int)." +2 
<span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $int ?>int</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>$$ PERCEPÇÃO</th>
                <th><b><?= $TOTAL = (floor($nivelTotal + 3) + $sab)." +2 
<span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>"  ?></b></th>
                <th><?= floor($nivelTotal + 3) ?></th>
                <th><?= $sab ?>SAB</th>
                <th>0</th>
                <th>2</th>
            </tr>
            <tr>
                <th>SOBREVIVENCIA</th>
                <th><b><?= $TOTAL = (floor($nivelTotal / 2) + $sab)." +2 
<span title='caso esteja em combate,
eu tenho q gastar uma ação
de movimento para ganhar +2' > 
?  </span>" 
       ?>
                    
                    </b></th>
                <th><?= floor($nivelTotal / 2) ?></th>
                <th><?= $sab ?>SAB</th>
                <th>0</th>
                <th>2</th>
            </tr>

        </tbody>

    </table>
</div>