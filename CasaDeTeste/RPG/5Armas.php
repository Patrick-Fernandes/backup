<script>
    $(document).ready(function () {
<?PHP for ($i = 1; $i <= 7; $i++) { ?>
            $("#ARMAELEMENTAL<?= $i ?>").click(function (evento) {
                if ($("#ARMAELEMENTAL<?= $i ?>").attr("checked")) {
                    $("#ARMAELEMENTAL-<?= $i ?>").css("display", "block");
                } else {
                    $("#ARMAELEMENTAL-<?= $i ?>").css("display", "none");
                }
            });
            $("#ATKFURTIVO<?= $i ?>").click(function (evento) {
                if ($("#ATKFURTIVO<?= $i ?>").attr("checked")) {
                    $("#ATKFURTIVO-<?= $i ?>").css("display", "block");
                } else {
                    $("#ATKFURTIVO-<?= $i ?>").css("display", "none");
                }
            });
<?PHP } ?>
    });
</script>
<div class="col-sm-6  col-lg-6 col-xs-12">
    <h3>Armas</h3>   
    <table class="table">
        <thead>
            <tr>
                <th><b>Ataque</b></th>
                <th><b>TOTAL</b></th>
                <th><b><span title="BONUS BASE DE ATAQUE">BBA</SPAN></b></th>
                <th><b>MOD HABIL (for)</b></th>
                <th><b>mod de tamanho</b></th>
                <th><b>---- </b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td  style="background-color: #333;color: #fff">CORPO-A-CORP0</td>
                <td><b><?= $TOTAL = (floor($feit / 2) + 0) + $for + 1 + 1 + 1//(ladino)     ?></b></td>
                <td><?= floor($feit / 2) + 1//(ladino)     ?></td>
                <td><?= $for ?></td>
                <td>1</td>
                <td>1</td>
            </tr>
        </tbody>
        <thead>
            <tr>
                <th><b>Arma</b></th>
                <th><b>BA</b></th>
                <th><b>DANO</b></th>
                <th><b>CRITICO / ARREMESSO</b></th>
                <th><b>TIPO</b></th>
                <th><b>DANO<BR>BONUS</b></th>
            </tr>
        </thead>
        <!--==================================================================-->
        <tbody>
            <tr>
                <td>ESPADA CURTA -NE- 10,5kg</td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d4+3
                    <span id="ARMAELEMENTAL-1" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-1" style="display: none;"> +1d6 </span>
                </td>
                <td>19-20 / </td>
                <td>perf</td>
                <td> 
                    <input type="checkbox" id="ARMAELEMENTAL1"> <br>arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO1"> atk furtivo
                </td>
            </tr>
            <tr>
                <td>ESPADA LONGA 1kg <?php $peso = $peso + 1; ?></td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d6+3
                    <span id="ARMAELEMENTAL-2" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-2" style="display: none;"> +1d6 </span>                  
                </td>
                <td>19-20 / </td>
                <td>CORTE</td>
                <td>  <input type="checkbox" id="ARMAELEMENTAL2"> <br>arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO2"> atk furtivo</td>
            </tr>
            <tr>
                <td>AZAGAIA X2 0,5kg <?php $peso = $peso + 1; ?></td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d4 +3
                    <span id="ARMAELEMENTAL-3" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-3" style="display: none;"> +1d6 </span>
                </td>
                <td>X2 / 9M</td>
                <td>perf</td>
                <td>
                    <input type="checkbox" id="ARMAELEMENTAL3"> <br>arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO3"> atk furtivo
                </td>
            </tr>
            <tr>
                <td>Picareta <?php $peso = $peso + (1.5); ?> </td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d4+3
                    <span id="ARMAELEMENTAL-7" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-7" style="display: none;"> +1d6 </span>
                </td>
                <td>x4 / </td>
                    <td>perf</td>
                <td> 
                    <input type="checkbox" id="ARMAELEMENTAL7"> <br>arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO7"> atk furtivo
                </td>
            </tr>
        </tbody>
        <!--====================================================================================-->
        <thead>
            <tr>
                <th><b>Ataque</b></th>
                <th><b>TOTAL</b></th>
                <th><b><span title="BONUS BASE DE ATAQUE">BBA</SPAN></b></th>
                <th><b>MOD HABIL(DES)</b></th>
                <th><b>mod de tamanho</b></th>
                <th><b></b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="background-color: #333;color: #fff">DISTANCIA</td>
                <td><b><?= $TOTAL = (floor($feit / 2) + 0) + $des + 1 + 1//(ladino)    ?></b></td>
                <td><?= floor($feit / 2) + 1//(ladino)     ?></td>
                <td><?= $des ?></td>
                <td>1</td>
                <td></td>
            </tr>
        </tbody>
        <!--==============================================================================-->
        <thead>
            <tr>
                <th><b>Arma</b></th>
                <th><b>BA</b></th>
                <th><b>DANO</b></th>
                <th><b>CRITICO / DISTANCIA</b></th>
                <th><b>TIPO</b></th>
                <th><b>DANO BONUS</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>ARCO COMPOSTO X60 <?php $peso = $peso + 0.5 + (1.5 * 2); ?></td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d4+3
                    <span id="ARMAELEMENTAL-4" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-4" style="display: none;"> +1d6 </span>
                </td>
                <td>X3 / 18M</td>
                <td>perf</td>
                <td> 
                    <input type="checkbox" id="ARMAELEMENTAL4"> <br>arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO4"> atk furtivo
                </td>
            </tr>
            <tr>
                <td>ADAGA  X5 <?php $peso = $peso + (0.25 * 5); ?></td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d3+2
                    <span id="ARMAELEMENTAL-5" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-5" style="display: none;"> +1d6 </span>
                </td>
                <td> / 18M</td>
                <td>perf</td>
                <td  >
                    <input type="checkbox" id="ARMAELEMENTAL5"> 
                    arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO5"> atk furtivo
                </td>
            </tr>
            <tr>
                <td>Frasco ACIDO X1 <?php $peso = $peso + (1); ?> </td>
                <td><b><?= $TOTAL ?></b></td>
                <td>1d4+2
                    <span id="ARMAELEMENTAL-6" style="display: none;"> +1d6+1 </span>
                    <span id="ATKFURTIVO-6" style="display: none;"> +1d6 </span>
                </td>
                <td>/ 3M</td>
                <td>ACIDO</td>
                <td>
                    <input type="checkbox" id="ARMAELEMENTAL6"> <br>arma elemental<br>
                    <input type="checkbox" id="ATKFURTIVO6"> atk furtivo
                </td>
            </tr>
        </tbody>
    </table>
</div>