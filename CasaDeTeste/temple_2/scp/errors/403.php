
<?php include ('../include/header.php'); ?>

<div id="page-wrapper">
    <div class="container-fluid">
        <?= montaCab("ERROR 403", "Proibido: acesso negado", " ") ?>

        <div class="row">
            <div class="col-lg-12"><div class="form-group">
                    <H2><a href="../HOME" class="btn btn-block btn-danger">Voltar</a></H2> 
                    <img src="403.jpg" alt="ERROR 403" height="100%" width="100%"/>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../include/footer.php')
?>