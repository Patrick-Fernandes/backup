<?php

class BolsistaDAL extends Exceptions implements Template {

    private $resp;
    private $db;

    public function __construct() {
        $this->db = new DB();
    }

    public function getResp() {
        return $this->resp;
    }

    public function buscar($pesquisar = "") {
        try {
            $sql = "SELECT * FROM bolsista " . $pesquisar;
            $this->db->query($sql);
            if ($this->db->quantidadeRegistros() > 0) {
                while ($obj = $this->db->fetchObj()) {
                    $arr[] = new BolsistaTO(
                            $obj->id, $obj->nome, $obj->cpf, $obj->rg, $obj->funcao, $obj->status, $obj->nit, $obj->siape, $obj->nacionalidade, $obj->municipio, $obj->nascimento, $obj->banco, $obj->agencia, $obj->conta_bancaria, $obj->municipio_de_atuacao, $obj->logradouro, $obj->cep, $obj->telefone, $obj->email, $obj->curso, $obj->disciplina, $obj->carga_horaria, $obj->data_inicio, $obj->data_fim, $obj->programa);
                }
                $this->resp = $arr;
                return true;
            } else {
                $arr[] = new BolsistaTO(
                        '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
                $this->resp = $arr;
                return false;
            }
        } catch (Exception $e) {
            $arr[] = new BolsistaTO(
                    $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage());
            $this->resp = $arr;
            return false;
        }
    }

    public function buscarDate($pesquisar = "") {
        try {
            $sql = $pesquisar;
            $this->db->query($sql);
            if ($this->db->quantidadeRegistros() > 0) {
                while ($obj = $this->db->fetchObj()) {
                    $arr[] = new BolsistaTO(
                            $obj->id, $obj->nome, $obj->cpf, $obj->rg, $obj->funcao, $obj->status, $obj->nit, $obj->siape, $obj->nacionalidade, $obj->municipio, $obj->nascimento, $obj->banco, $obj->agencia, $obj->conta_bancaria, $obj->municipio_de_atuacao, $obj->logradouro, $obj->cep, $obj->telefone, $obj->email, $obj->curso, $obj->disciplina, $obj->carga_horaria, $obj->data_inicio, $obj->data_fim, $obj->programa);
                }
                $this->resp = $arr;
                return true;
            } else {
                $arr[] = new BolsistaTO(
                        '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
                $this->resp = $arr;
                return false;
            }
        } catch (Exception $e) {
            $arr[] = new BolsistaTO(
                    $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage(), $e->getMessage());
            $this->resp = $arr;
            return false;
        }
    }

    public function inserir($bolsista) {

        try {
            $dados = "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getId()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getNome()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getCpf()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getRg()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getFuncao()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getStatus()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getNit()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getSiape()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getNacionalidade()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getMunicipio()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getNascimento()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getBanco()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getAgencia()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getConta_bancaria()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getMunicipio_de_atuacao()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getLogradouro()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getCep()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getTelefone()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getEmail()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getCurso()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getDisciplina()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getCarga_horaria()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getData_inicio()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getData_fim()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(), $bolsista->getPrograma()) . "'";

            $sql = "INSERT INTO bolsista VALUES (" . $dados . ")";
            if ($this->db->query($sql)) {
                $this->resp = "Dados inseridos.";
                return true;
            } else {
                echo $sql;
                $this->resp = "Dados n�o inseridos.<br>" . $this->db->getErro();
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionBolsista("BolsistaDAL", "inserirbolsista");
            $this->resp = $e->getMessage();
            return false;
        }
    }

    public function atualizar($bolsista) {
        try {
            $sql = "UPDATE bolsista SET
                nome='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getNome()) . "',
                cpf='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getCpf()) . "',
                rg='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getRg()) . "',
                funcao='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getFuncao()) . "',
                status='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getStatus()) . "',
                nit='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getNit()) . "',
                siape='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getSiape()) . "',
                nacionalidade='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getNacionalidade()) . "',
                municipio='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getMunicipio()) . "',
                nascimento='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getNascimento()) . "',
                banco='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getBanco()) . "',
                agencia='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getAgencia()) . "',
                conta_bancaria='" . mmysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getConta_bancaria()) . "',                   
                municipio_de_atuacao='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getMunicipio_de_atuacao()) . "',
                logradouro='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getLogradouro()) . "',
                cep='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getCep()) . "',
                telefone='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getTelefone()) . "',
                email='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getEmail()) . "',
                curso='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getCurso()) . "',
                disciplina='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getDisciplina()) . "',
                carga_horaria='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getCarga_horaria()) . "',                    
                data_inicio='" . mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getData_inicio()) . "',
                data_fim='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getData_fim()) . "',
                programa='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getPrograma()) . "'
                WHERE id='" .mysqli_real_escape_string($this->db->LigarConexao(),$bolsista->getId()) . "'";

            if ($this->db->query($sql)) {
                $this->resp = "Dados atualizados.";
                return true;
            } else {
                $this->resp = "Dados n�o atualizados.<br>" . $this->db->getErro();
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionBolsista("BolsistaDAL", "atualizarbolsista");
            $this->resp = $e->getMessage();
            return false;
        }
    }

    public function excluir($codigo) {
        try {
            $sql = "DELETE FROM bolsista WHERE id='" . mysqli_real_escape_string($this->db->LigarConexao(),$codigo) . "'";
            if ($this->db->query($sql)) {
                $this->resp = "Dados deletados.";
                return true;
            } else {
                $this->resp = "Dados n�o deletados.<br>" . $this->db->getErro();
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionBolsista("BolsistaDAL", "deletarbolsista");
            $this->resp = $e->getMessage();
            return false;
        }
    }

    public function contar($pesquisar = "") {
        try {
            $sql = "SELECT * FROM bolsista " . $pesquisar;
            $this->db->query($sql);
            if ($this->db->quantidadeRegistros() > 0) {
                $this->resp = $this->db->quantidadeRegistros();
                return true;
            } else {
                $this->resp = "0";
                $this->resp = "Nenhum registro encontrado.";
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionBolsista("BolsistaDAL", "contarbolsista");
            $this->resp = $e->getMessage();
            return false;
        }
    }

}
