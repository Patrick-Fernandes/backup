<?
/**
 *  frog::SMS
 *  http://fr0gSMS.codigolivre.org.br
 * 
 *  Autor: Luiz Geovani Vier (vier at brturbo.com - http://vier.ta-na.net)
 *  Data: Maio/2004
 *  Licen�a: GNU GPL - http://www.gnu.org/copyleft/gpl.html
 */

$configFile = "../config.php";
file_exists($configFile) || die ("Arquivo $configFile n�o encontrado. Leia o README, man�!! ;-)");
include($configFile);

include("sms.class.php");

$tituloPadrao = "fr0g::SMS";
$selecionarContato = "";

$conn = 0;
$msg = "";
$msgRetorno = "";
$usuario = "";
$naoLogar = false;
$agenda = array();
$contatoDefault = "";
$tituloPagina = "";
$efetuouLogin = false;

if ($acao == "login") {
	
	connect();
	session_start();
	$senha = md5($senhaLogin);
	
	if (login($usuarioLogin, $senha)) {
		$usuario = $usuarioLogin;
		$msgRetorno = "Login efetuado com sucesso";
		
		session_register("usuarioLogado");
		$usuarioLogado = $usuario;
		
		if ($setarCookie == "true") {
			setarCookiesLogin($usuarioLogin, $senha);
		} else {
			removerCookiesLogin();
		}
	} else {
		$msgRetorno = "Login inv�lido";
		logout();
	}
	session_write_close();
	
} elseif ($acao == "logout") {

	session_start();
	logout();
	session_write_close();

} elseif ($acao == "cadastrarUsuario") {

	connect();
	// verifica se o usu�rio nao existe ainda	
	$sql = "SELECT id FROM sms_usuarios WHERE id = '$usuarioCadastro'";
	$rs = mysql_query($sql) or die ("ERRO: ".mysql_error());
	
	if ($row = mysql_fetch_object($rs)) {
		$msgRetorno = "Erro: Usu�rio j� cadastrado";
		mysql_free_result($rs);
	} else {
		mysql_free_result($rs);
		session_start();
		$senha = md5($senhaCadastro);
		$sql = "
		INSERT INTO sms_usuarios
		( id
		, senha
		, email
		, newsletter ) 
		VALUES 
		( '$usuarioCadastro'
		, '$senha'
		, '$emailCadastro'
		, '".( $newsCadastro == "true" ? 1 : 0 )."' )";
		mysql_query($sql) or die ("ERRO: ".mysql_error());
		
		$usuario = $usuarioCadastro;
		$msgRetorno = "Cadastro efetuado com sucesso";
		
		session_register("usuarioLogado");
		$usuarioLogado = $usuario;
		
		if ($setarCookie == "true") {
			setarCookiesLogin($usuarioCadastro, $senha);
		} else {
			removerCookiesLogin();
		}
		
		session_write_close();
	}
}

if (!$naoLogar && $usuario == "") {
	session_start();
	if (session_is_registered("usuarioLogado")) {
		$usuario = $usuarioLogado;
		$tituloPagina = $tituloPaginaSessao;
		$contatoDefault = $contatoDefaultSessao;
	} elseif ($HTTP_COOKIE_VARS["usuarioSMS"] != "" && $HTTP_COOKIE_VARS["senhaSMS"] != "") {
		$usuarioCookie = $HTTP_COOKIE_VARS["usuarioSMS"];
		$senhaCookie = $HTTP_COOKIE_VARS["senhaSMS"];
		if ($conn == 0) connect();
		if (login($usuarioCookie, $senhaCookie)) {
			$usuario = $usuarioCookie;
			session_register("usuarioLogado");
			$usuarioLogado = $usuario;
			// reescreve o cookie para modificar a data de expira��o
			setarCookiesLogin($usuarioLogado, $senhaCookie);
		}
	}
	session_write_close();
}


// envio da mensagem
if ($acao == "enviar") {
	$smsSender = new SMSSender();
	$sucesso = $smsSender->enviar($operadora, $dddPara, $telPara, $de, $dddDe, $telDe, $mensagem, $check, $msgRetorno);
	if ($sucesso) {
		if ($conn == 0) connect();
		$mensagem = "";
		// contabiliza o envio
		if ($usuario) {
			if ($conn == 0) connect();
			$sql = "
			UPDATE sms_agenda 
			SET    qtdEnviadas = (qtdEnviadas + 1) 
			WHERE  operadora = '$operadora' 
			AND    ddd = '$dddPara' 
			AND    telefone = '$telPara' 
			AND    idUsuario = '$usuario' ";
			mysql_query($sql) or die ("ERRO: ".mysql_error());
			$sql = "
			UPDATE sms_usuarios 
			SET    qtdEnviadas = (qtdEnviadas + 1) 
			WHERE  id = '$usuario' ";
			mysql_query($sql) or die ("ERRO: ".mysql_error());
		}
		$sql = "
		INSERT INTO sms_log 
		(idUsuario, horaEnvio, operadora, ip)
		VALUES
		('$usuario', now(), '$operadora', '$REMOTE_ADDR') ";
		mysql_query($sql) or die ("ERRO: ".mysql_error());
		// count msg (pura balaca :-)
		$msgRetorno = str_replace("Mensagem", "Mensagem n�mero ".mysql_insert_id(), $msgRetorno);
	}
}

// dados da agenda
if ($usuario) {
	if ($conn == 0) connect();
	
	if ($acao == "excluirContato") {
		
		excluirContato($idContato, $usuario);
		$msgRetorno = "Contato exclu�do com sucesso!";
	
	} elseif ($acao == "incluirContato") {
		
		excluirContato($nomePara, $usuario);
		
		$sql = "
		INSERT INTO sms_agenda
		( id
		, idUsuario
		, operadora
		, ddd
		, telefone
		, de
		, mensagemPadrao
		, qtdEnviadas ) 
		VALUES 
		( '$nomePara'
		, '$usuario'
		, '$operadora'
		, '$dddPara'
		, '$telPara'
		, '$de'
		, '$mensagem'
		, 0 )";
		mysql_query($sql) or die ("ERRO: ".mysql_error());
		$msgRetorno = "Contato inclu�do com sucesso!";
		
	} elseif ($acao == "alterarContato") {
		
		$sql = "
		UPDATE sms_agenda
		SET    id = '$nomePara'
		,      operadora = '$operadora'
		,      ddd = '$dddPara'
		,      telefone = '$telPara'
		,      de = '$de'
		,      mensagemPadrao = '$mensagem'
		WHERE  id = '$contatoAlterado' 
		AND    idUsuario = '$usuario' ";
		mysql_query($sql) or die ("ERRO: ".mysql_error());
		$msgRetorno = "Contato alterado com sucesso!";
		
	} elseif ($acao == "personalizar") {

		session_start();
		
		$sql = "
		UPDATE sms_usuarios 
		SET    tituloPagina = '$novoTitulo'
		,      contatoDefault = '$novoContatoPadrao' 
		WHERE  id = '$usuario' ";
		mysql_query($sql) or die ("ERRO: ".mysql_error());
		
		session_register("contatoDefaultSessao");
		$contatoDefaultSessao = $novoContatoPadrao;
		session_register("tituloPaginaSessao");
		$tituloPaginaSessao = $novoTitulo;
		
		$tituloPagina = $tituloPaginaSessao;
		$contatoDefault = $contatoDefaultSessao;
		
		session_write_close();
	
	}
	
	$sql = "SELECT * FROM sms_agenda WHERE idUsuario = '$usuario' ORDER BY qtdEnviadas DESC, id ASC";
	$rs = mysql_query($sql) or die ("ERRO: ".mysql_error());
	
	while ($row = mysql_fetch_object($rs)) {
		$agenda[] = $row;
	}
	
	mysql_free_result($rs);
}

disconnect();

if (!$msgRetorno) {
	if (!$usuario) {
		$msgRetorno = "<font style=\"color:blue\">Dica: Cadastre-se ou fa�a login para usar a agenda ;-)</font>";
	}
}

if (!$tituloPagina) $tituloPagina = $tituloPadrao;
if (!$operadora && $contatoDefault) $selecionarContato = $contatoDefault;


/*********************/
function logout () {
	global $naoLogar, $usuario, $usuarioLogado, $contatoDefaultSessao, $tituloPaginaSessao;
	session_unregister("usuarioLogado");
	session_unregister("contatoDefaultSessao");
	session_unregister("tituloPaginaSessao");
	removerCookiesLogin();
	$naoLogar = true;
	$usuario = "";
}
function login ($usuario, $senha) {
	global $selecionarContato, $contatoDefaultSessao, $tituloPaginaSessao, $tituloPagina, $contatoDefault, $efetuouLogin;
	$retorno = false;
	$sql = "SELECT tituloPagina, contatoDefault FROM sms_usuarios WHERE id = '$usuario' AND senha = '$senha'";
	$rs = mysql_query($sql) or die ("ERRO: ".mysql_error());
	
	if ($row = mysql_fetch_object($rs)) {
		$retorno = true;
		session_register("contatoDefaultSessao");
		$contatoDefaultSessao = $row->contatoDefault;
		session_register("tituloPaginaSessao");
		$tituloPaginaSessao = $row->tituloPagina;
		
		$tituloPagina = $tituloPaginaSessao;
		$contatoDefault = $contatoDefaultSessao;
		
		$efetuouLogin = true;
	}
	mysql_free_result($rs);
	
	return $retorno;
}
function setarCookiesLogin ($usuario, $senha) {
	$exp = time() + (3600 * 24 * 60); // 60 dias
	setcookie("usuarioSMS", $usuario, $exp);
	setcookie("senhaSMS", $senha, $exp);
}
function removerCookiesLogin() {
	global $HTTP_COOKIE_VARS;
	if ($HTTP_COOKIE_VARS["usuarioSMS"] != "") setcookie("usuarioSMS", "", time() - 3600);
	if ($HTTP_COOKIE_VARS["senhaSMS"] != "") setcookie("senhaSMS", "", time() - 3600);
}
function excluirContato($idContato, $idUsuario) {
	$sql = "
	DELETE FROM sms_agenda
	WHERE  id = '$idContato'
	AND    idUsuario = '$idUsuario' ";
	mysql_query($sql) or die ("ERRO: ".mysql_error());
}
function connect () {
	global $conn, $dbUser, $dbPasswd, $db;
	$conn = mysql_connect("localhost", $dbUser, $dbPasswd) or die("n�o conseguiu conectar");
	mysql_select_db($db) or die("n�o conseguiu selecionar o banco de dados");
}
function disconnect () {
	global $conn;
	if ($conn) {
		mysql_close($conn);
		$conn = 0;
	}
}
/***********************/
?>
