<?
/**
 *  frog::SMS
 *  http://fr0gSMS.codigolivre.org.br
 * 
 *  Autor: Luiz Geovani Vier (vier at brturbo.com - http://vier.ta-na.net)
 *  Data: Maio/2004
 *  Licen�a: GNU GPL - http://www.gnu.org/copyleft/gpl.html
 */
?>
<LINK rel="STYLESHEET" href="sms.css" type="text/css">
<script language="JavaScript">
	// nao permite carregar em frames
	if (window.self != window.top) window.top.location = window.self.location; 
	
	var execTimeout = <?= (($execTimeout + 5)*1000) ?>;
	var usuario = "<?= $usuario ?>";
	var currHREF = '<?= $PHP_SELF ?>';
</script>
<script language="JavaScript" src="sms.js"></script>
<script language="JavaScript" src="menu.js"></script>
<table id="tbSMS" width="372" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><img src="sms_imgs/spacer.gif" width="7" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="4" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="13" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="325" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="11" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="3" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="8" height="1" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="1" border="0" alt=""></td>
  </tr>
  <tr onselectstart="return false;"> 
    <td colspan="4"><img name="wnd_r1_c1" src="sms_imgs/wnd_r1_c1.png" width="25" height="20" border="0" alt=""></td>
    <td background="sms_imgs/wnd_r1_c5.png" align="left" style="padding-top:2px;">
	<!-- titulo -->
	<b>&nbsp;<font class="txtwhite" id="tituloEnviarMSG" style="display:">Enviar Mensagem <? if ($usuario) echo "($usuario)"; ?>
	</font><font class="txtwhite" id="tituloIncluirContato" style="display:none">Incluir Contato</font></b>
	<!-- fim titulo -->
	</td>
    <td colspan="3"><a href="#" onClick="recarregar();this.blur();return false;"><img name="wnd_r1_c6" alt="Cancelar a��o atual" src="sms_imgs/wnd_r1_c6.png" width="22" height="20" border="0" alt=""></a></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="20" border="0" alt=""></td>
  </tr>
  <tr> 
    <td colspan="8"><img name="wnd_r2_c1" src="sms_imgs/wnd_r2_c1.png" width="372" height="3" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="3" border="0" alt=""></td>
  </tr>
  <tr> 
    <td rowspan="4"><img name="wnd_r3_c1" src="sms_imgs/wnd_r3_c1.png" width="7" height="244" border="0" alt=""></td>
    <td colspan="6" bgcolor="#FFFFFF" nowrap>
	<!-- menu -->
	  <div class="menuBar"><a class="menuButton" href="#" onclick="return buttonClick(event, 'menuOpcoes');" onmouseover="buttonMouseover(event, 'menuOpcoes');">Op��es</a> 
        <? if ($usuario) { ?>
			<a class="menuButton" href="#" onclick="return buttonClick(event, 'menuCadastro');" onmouseover="buttonMouseover(event, 'menuCadastro');">Cadastros</a> 
			<? if (count($agenda) > 0) { ?> 
        		<a class="menuButton" href="#" onclick="return buttonClick(event, 'menuAgenda');" onmouseover="buttonMouseover(event, 'menuAgenda');">Agenda</a> 
        <? 	} 
		 } ?>
	<a class="menuButton" href="#" onclick="return sobre();this.blur();" onmouseover="buttonMouseover(event, 'menuSobre');">Sobre</a> 
      </div>
	<!-- fim menu -->
	</td>
    <td rowspan="4"><img name="wnd_r3_c8" src="sms_imgs/wnd_r3_c8.png" width="8" height="244" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="20" border="0" alt=""></td>
  </tr>
  <tr> 
    <td colspan="6"><img name="wnd_r4_c2" src="sms_imgs/wnd_r4_c2.png" width="357" height="6" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="6" border="0" alt=""></td>
  </tr>
  <tr> 
    <td rowspan="2" colspan="2"><img name="wnd_r5_c2" src="sms_imgs/wnd_r5_c2.png" width="5" height="218" border="0" alt=""></td>
    <td colspan="3" bgcolor="#FFFFFF" valign="middle">
		<!--- conteudo -->
		<form action="<?= $PHP_SELF ?>" method="POST" name="formSMS" onSubmit="return validarEnvio();">
			<input type="hidden" name="acao" value="enviar">
			<input type="hidden" name="contatoAlterado" value="">
			  <table id="tbConteudo" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr> 
				  <td><div align="right"><strong>Operadora*</strong>:&nbsp;</div></td>
				  <td> <select name="operadora" valorAnterior="" onFocus="this.valorAnterior = this.value" onBlur="if (this.value != valorAnterior) trocaOperadora(this.value);" tabindex="1">
					  <option value="">...</option>
					  <option value="claro"<?= ($operadora == "claro" ? " selected" : "") ?>>Claro</option>
					  <option value="tim"<?= ($operadora == "tim" ? " selected" : "") ?>>TIM</option>
					  <option value="oi"<?= ($operadora == "oi" ? " selected" : "") ?>>Oi</option>
					  <option value="vivo-rs"<?= ($operadora == "vivo-rs" ? " selected" : "") ?>>Vivo-RS</option>
					</select> 
					<a id=linkPing href="#" onClick="ping();this.blur();return false;">verificar status</a>
					</td>
				<tr id="linhaNomePara" style="display:none"> 
				  <td><div align="right"><strong>Nome:*</strong>:&nbsp;</div></td>
				  <td> <input type="text" name="nomePara" size="20" maxlength="30" onKeyUp="nextFocus(this);" tabindex="2"> 
				  </td>
				</tr>
				<tr> 
				  <td><div align="right" id="labelTelPara"><strong>Para*</strong>:&nbsp;</div></td>
				  <td> <input type="text" name="dddPara" size="3" maxlength="2" value="<?= $dddPara ?>" onKeyPress="return(soNumeros(this,event))" onKeyUp="nextFocus(this);" tabindex="3"> 
					<input type="text" name="telPara" size="10" maxlength="8" value="<?= $telPara ?>" onKeyPress="return(soNumeros(this,event))" onKeyUp="nextFocus(this);" tabindex="4"> 
					<input type="button" class="botao" name="limpar" value="Limpar" onClick="limparFormSMS();" tabindex="9">
				  </td>
				</tr>
				<tr> 
				  <td><div align="right"><strong>De</strong>:&nbsp;</div></td>
				  <td><input type="text" name="de" size="10" maxlength="8" value="<?= $de ?>" onfocus="contaCaracteres();" onChange="contaCaracteres()" onKeyDown="contaCaracteres()" onKeyUp="contaCaracteres(); nextFocus(this);" tabindex="5"></td>
				</tr>
				<tr> 
				  <td><div id="labelMensagem" align="right"><strong>Mensagem</strong>*:&nbsp;</div></td>
				  <td> <textarea cols="29" rows="4" name="mensagem" wrap="PHYSICAL" style="overflow:hidden" onfocus="contaCaracteres();" onChange="contaCaracteres()" onKeyDown="contaCaracteres()" onKeyUp="contaCaracteres()" tabindex="6"><?= $mensagem ?></textarea> 
				  </td>
				</tr>
				<tr id="trConfirmacao" style="display:none" align="center">
				  <td colspan="2" valign="middle" height="30">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="2" align="center" valign="bottom" style="padding-top: 2px;">
				  <input type="text" name="chars" size="3" maxlength="3" onKeyDown="return false" onKeyUp="return false" onFocus="this.form.enviar.focus()" onClick="return false" onSelect="return false" onSelectStart="return false" tabindex="100"> 
					caracteres dispon�veis 
					<div id="enterOK" style="display:none"><br></div>
					<input type="submit" class="botao" name="enviar" value="&nbsp;Vai!&nbsp;" tabindex="8">
					<input style="display:none" type="button" class="botao" name="cancelar" value="&nbsp;Cancelar&nbsp;" onClick="recarregar();" tabindex="9">
					</td>
				</tr>
			  </table>
		</form>
		<!-- fim conteudo -->
	</td>
    <td rowspan="2"><img name="wnd_r5_c7" src="sms_imgs/wnd_r5_c7.png" width="3" height="218" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="211" border="0" alt=""></td>
  </tr>
  <tr> 
    <td colspan="3"><img name="wnd_r6_c4" src="sms_imgs/wnd_r6_c4.png" width="349" height="7" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="7" border="0" alt=""></td>
  </tr>
  <tr> 
    <td rowspan="2" colspan="2"><img name="wnd_r7_c1" src="sms_imgs/wnd_r7_c1.png" width="8" height="29" border="0" alt=""></td>
    <td colspan="5" bgcolor="#FFFFFF" nowrap style="padding:4px;">
	<!-- barra status -->	
		<div id="status"><?= ($msgRetorno ? $msgRetorno : "") ?></div>
	<!-- fim barra status -->
	</td>
    <td rowspan="2"><img name="wnd_r7_c8" src="sms_imgs/wnd_r7_c8.png" width="8" height="29" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="21" border="0" alt=""></td>
  </tr>
  <tr> 
    <td colspan="5"><img name="wnd_r8_c3" src="sms_imgs/wnd_r8_c3.png" width="356" height="8" border="0" alt=""></td>
    <td><img src="sms_imgs/spacer.gif" width="1" height="8" border="0" alt=""></td>
  </tr>
</table>
<iframe id="ifrReq" src="javascript:false;" scrolling="no" frameborder="0" style="position:absolute; top:0px; left:0px; display:none;"></iframe>
<!-- Main menus. -->
<div id="menuOpcoes" class="menu" onmouseover="menuMouseover(event)">
	<? if ($usuario) { ?>
		<a class="menuItem" href="#" onclick="return false;" onmouseover="menuItemMouseover(event, 'menuPersonalizar');"
		><span class="menuItemText">Personalizar</span><span class="menuItemArrow"><img src="sms_imgs/seta.gif" border=0></span></a>
		<a class=menuItem href="#" onClick="logout();return false;">Logout</a>
	<? } else { ?>
		<a class="menuItem" href="#" onclick="return false;" onmouseover="menuItemMouseover(event, 'menuLogin');"
		><span class="menuItemText">Login</span><span class="menuItemArrow"><img src="sms_imgs/seta.gif" border=0></span></a>
		<a class="menuItem" href="#" onclick="return false;" onmouseover="menuItemMouseover(event, 'menuCadastro');"
		><span class="menuItemText">Cadastrar</span><span class="menuItemArrow"><img src="sms_imgs/seta.gif" border=0></span></a>
	<? } ?>
</div>
<? if ($usuario) { ?>
<div id="menuAgenda" class="menu" onmouseover="menuMouseover(event)">
	<?
	for ($i = 0; $i < count($agenda); $i++) {
		echo "<a class=menuItem href=\"#\" onClick=\"menuHide();cda($i);return false;\">".$agenda[$i]->id."</a>";
	}
	?>
</div>
<div id="menuCadastro" class="menu" onmouseover="menuMouseover(event)">
	<a class=menuItem href="#" onClick="menuHide();incluirContato();return false;">Incluir Contato</a>
	<? if (count($agenda) > 0) { ?> 
		<a class="menuItem" href="#" onclick="return false;" onmouseover="menuItemMouseover(event, 'menuAlterarContato');"
		><span class="menuItemText">Alterar Contato</span><span class="menuItemArrow"><img src="sms_imgs/seta.gif" border=0></span></a>
		<a class="menuItem" href="#" onclick="return false;" onmouseover="menuItemMouseover(event, 'menuExcluirContato');"
		><span class="menuItemText">Excluir Contato</span><span class="menuItemArrow"><img src="sms_imgs/seta.gif" border=0></span></a>
	<? } ?>
</div>
<div id="menuExcluirContato" class="menu" onmouseover="menuMouseover(event)">
	<?
	for ($i = 0; $i < count($agenda); $i++) {
		echo "<a class=menuItem href=\"#\" onClick=\"exc($i);return false;\">".$agenda[$i]->id."</a>";
	}
	?>
</div>
<div id="menuAlterarContato" class="menu" onmouseover="menuMouseover(event)">
	<?
	for ($i = 0; $i < count($agenda); $i++) {
		echo "<a class=menuItem href=\"#\" onClick=\"menuHide();altc($i);return false;\">".$agenda[$i]->id."</a>";
	}
	?>
</div>
<? } ?>
<div id="menuLogin" class="menu" onmouseover="menuMouseover(event)"
><form name="formLogin" method="post" action="<?= $PHP_SELF ?>" onSubmit="return validarLogin()"
><input type="hidden" name="acao" value="login"
><table class="tbMenu"
><tr><td align="right">usu�rio*: </td><td><input type="text" name="usuarioLogin" maxlength="30" size="8" tabindex="30"></td></tr
><tr><td align="right">senha*: </td><td><input type="password" name="senhaLogin" maxlength="8" size="8" tabindex="31"></td></tr
><tr><td colspan="2"><input type="checkbox" id="setarCookieLogin" name="setarCookie" checked value="true" tabindex="32"> <label for="setarCookieLogin">lembrar (cookie)</label></td></tr
><tr><td></td><td align="right"><input type="submit" class="botao" name="okLogin" value="OK"  tabindex="33"></td></tr
></table
></form
></div>
<div id="menuCadastro" class="menu" onmouseover="menuMouseover(event)"
><form name="formCadastro" method="post" action="<?= $PHP_SELF ?>" onSubmit="return validarCadastro()"
><input type="hidden" name="acao" value="cadastrarUsuario"
><table class="tbMenu"
><tr><td align="right">usu�rio*: </td><td><input type="text" name="usuarioCadastro" maxlength="30" size="8" tabindex="40"></td></tr
><tr><td align="right">senha*: </td><td><input type="password" name="senhaCadastro" maxlength="8" size="8" tabindex="41"></td></tr
><tr><td align="right">repita*: </td><td><input type="password" name="senhaCadastro2" maxlength="8" size="8" tabindex="42"></td></tr
><tr><td align="right">e-mail: </td><td><input type="text" name="emailCadastro" maxlength="50" size="20" tabindex="43"></td></tr
><tr><td colspan="2"><input type="checkbox" id="newsCadastro" name="newsCadastro" checked value="true" tabindex="44"> <label for="newsCadastro">notificar atualiza��es</label></td></tr
><tr><td colspan="2"><input type="checkbox" id="setarCookieCadastro" name="setarCookie" checked value="true" tabindex="45"> <label for="setarCookieCadastro">lembrar login (cookie)</label></td></tr
><tr><td></td><td align="right"><input type="submit" class="botao" name="okCadastro" value="OK" tabindex="46"></td></tr
></table
></form
></div>
<div id="menuPersonalizar" class="menu" onmouseover="menuMouseover(event)"
><form name="formPersonalizar" method="post" action="<?= $PHP_SELF ?>"
><input type="hidden" name="acao" value="personalizar"
><table class="tbMenu"
><tr><td align="right">T�tulo da P�gina: </td><td><input type="text" name="novoTitulo" maxlength="50" size="15" value="<?= $tituloPagina ?>" tabindex="50"></td></tr>
<? if (count($agenda) > 0) { ?> 
	<tr><td align="right">Contato Default: </td><td><select name="novoContatoPadrao" donthide=true tabindex="51">
	<option value="">...</option>
	<?
	for ($i = 0; $i < count($agenda); $i++) {
		echo "<option".($contatoDefault == $agenda[$i]->id ? " selected" : "").">".$agenda[$i]->id."</option>";
	}
	?>
	</select></td></tr>
<? } ?>
<tr><td></td><td align="right"><input type="submit" class="botao" name="okPersonalizar" value="OK" tabindex="52"></td></tr
></table
></form
></div>
<div id="menuSobre"></div>
<script language="JavaScript">
	<?
	for ($i = 0; $i < count($agenda); $i++) {
		echo "addA('".$agenda[$i]->id."', '".$agenda[$i]->operadora."','".$agenda[$i]->ddd."','".$agenda[$i]->telefone."','".$agenda[$i]->de."','".$agenda[$i]->mensagemPadrao."','".$agenda[$i]->qtdEnviadas."');";
	}
	?>
	carregarContatoDefault('<?= $selecionarContato ?>', <?= $efetuouLogin ? 1 : 0 ?>);
	carregandoPagina = false;
</script>
