Hi.

In the first place, I hope that you excuse my poor English.

If what you wish it is to know how to install this script go directly to
the directory ' docs'.

If you have some interest in its history you can continue reading (it is
brief)



Version History. ___________________


Version 1.1.2 (actual version)

Some bugs corrected.



Version 1.1

Added administration with IP banning and user kicking. Added bad world
filtering. Corrected some characters display. Authomatic deletion of
"gosht users" More bugs corrected. Better sound control.



Version 1.0

Some bugs corrected



Version 0.9

First public release with basic chat, sound, private messages and
reviewing messages.


First steps

I was looking for a chat script working under php3 to place in my web. I
found one with a flash interface and a simple script whose author was
Nicola del Bono. The flash interface was developed by Joost van Brug.

This script was originally very simple with only two files (the script
itself and a text file to save the messages). It had several erros. If
you want to see the original and compare you can get it (included flash
interface) at http://www.mycgiserver.com/~icknay/download.php


Although the script lacked capacities that I wanted, I thought that it
could be a good starting point.


I put hands to the work and I added control over the users, hour,
private messages, sound, review of messages... everything you have now
in your hands. (By the way, I have lowered the 24k of the original flash
file to 13k)


I hope that it'll be useful to you.


Juan Carlos Pos�

juancarlos@develooping.com