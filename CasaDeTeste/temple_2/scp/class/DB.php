<?php

class DB {

    private $server;
    private $usuario;
    private $senha;
    private $banco;
    private $conn;
    private $msgErroQuery;
    private $query;

    // inicializa as variaveis para a conex�o com o banco
    public function __construct() {
        self::escreveDados($this->server, "localhost");
        self::escreveDados($this->usuario, "root");
        self::escreveDados($this->senha, "");
        self::escreveDados($this->banco, "db_template2");
      
        self::conexao();
    }

    // cria uma conex�oo
    private function conexao() {

        $conect = mysqli_connect($this->server, $this->usuario, $this->senha) or
                die("N�o foi possivel conectar ao servidor mysql.<br>" . mysqli_error($this->conn));
        $this->conn = $conect;
        self::selecionaDB();
    }
    public function LigarConexao() {
        return $this->conn;
    }
    public function exitConexao() {
        return mysqli_close($this->conn);
    }

    // seleciona o banco
    private function selecionaDB() {
        mysqli_select_db($this->conn,$this->banco) or
                die("N�o foi possivel selecionar a base de dados.<br>" . mysqli_error($this->conn));
    }

    // escreve dados para as variaveis
    private function escreveDados(&$var, $param) {
        return $var = $param;
    }

    // faz uma query
    public function query($sql) {
        $query = mysqli_query($this->conn,$sql);
        $msgerr = mysqli_error($this->conn);
        if ($query) {
            $this->query = $query;
            return true;
        } else {
            $this->msgErroQuery = $msgerr;
            return false;
        }
    }

    // retorna o fetchObject da ultima consulta
    public function fetchObj() {
        return mysqli_fetch_object($this->query);
    }

    // retorna o id do insert referido
    public function ultimoId() {
        return mysqli_insert_id($this->query);
    }

    // retorna a quantidade de registro encontrados
    public function quantidadeRegistros() {
        return mysqli_num_rows($this->query);
    }

    // mostra mensagem de erro na query
    public function getErro() {
        return $this->msgErroQuery;
    }

    // retorna query
    public function getQuery() {
        return $this->query;
    }

}

?>
