<?
/*   CONFIGURA VARIABLES (Develooping flash Chat version 1.1)  */
/*   _______________________________________________________   */


/*   Variables de Administracion */
/*   ___________________________ */

$admin_name = "admin"; //nombre del administrador (max. 12 caracteres)

$admin_password = "admin"; // clave del administrador (max. 12 caracteres)


/*   Variables numericas del chat    */
/*   _____________________________   */

$correct_time = 0;//diferencia en segundos con el tiempo en el servidor

$chat_lenght = 15;//numero de mensajes mostrados en la sala

$review_lenght = 500;//numero de mensajes mostrados al revisar mensajes

$total_lenght = 1000;//numero de mensajes almacenados por el sistema


/*   Frases para traducir en el chat   */
/*   ________________________________  */

$private_message_expression = "\(para (.*)\)"; // VER NOTA AL FINAL. expresion regular para indicar mensaje privado

$not_here_string = "-El destinatario no se encuentra-"; //el receptor del mensaje privado no esta en la sala

$bye_string = "Vuelve cuando quieras";//despedida para el usuario

$enter_string = "(acaba de entrar en la sala)";//mensaje que avisa que un usuario entra en la sala.

$bye_user = "(acaba de salir de la sala)";//mensaje que avisa que un usuario abandona la sala.

$kicked_user = "---Has sido expulsado de la sala---";//mensaje mostrado en el chat al usuario expulsado.

$bye_kicked_user = "Si vuelves, procura comportarte";//despedida para el usuario expulsado

$bye_banned_user = "Te ha sido denegada la entrada a esta sala";//despedida para el usuario inhabilitado

$banned_user = "Lo siento, no eres bienvenido en este lugar";//mensaje mostrado en el chat al usuario inhabilitado


/*   Frases para traducir en administracion   */
/*   _______________________________________  */

$no_users = "No hay usuarios en la sala";//No hay usuarios en la sala

$text_for_kick_button = "Expulsar";//Texto del boton para expulsar usuarios

$text_for_bann_button = "Inhabilitar";//Texto del boton para inhabilitar IPs

$no_ips = "No hay ninguna IP inhabilitada";//No hay IPs inhabilitadas

$text_for_pardon_button = "Perdonar";//texto del boton para perdonar una IP

$ip_link = "Administrar IPs inhabilitadas";//texto del v�nculo a administracion de IP

$users_link = "Administrar lista de usuarios";//texto del vinculo a administracion de usuarios


/*   Palabras para filtrar   */
/*   ______________________  */

$worlds_to_filter = array("mierda", "joder", "follar", "jodan", "gilipollas", "capullo");//lista de palabrotas para filtar, pon las que quieras

$replace_by = "*@#!";//expresion para reemplazar a las palabrotas


/*   NOTA:   */
/*   ______  */

/*   
     Si reemplazas la expresi�n para los mensajes privados (variable $private_message_expression)
     deber�s hacerlo tambi�n en el ActionScript asociado al bot�n "Enviar" de la pantalla de usuarios
     del archivo "chat.fla" y volver a exportar la movie "chat.swf" con el valor cambiado
     
     Ejemplo: Si pones $private_message_expression = "\(destinado a (.*)\)";
     en el Action script mencionado deber�s poner
     
     on (release, keyPress "<Enter>") {
	if (mensajeprivado ne "") {
		if (destinatario ne "") {
			message = "(destinado a " add destinatario add ") ....
     
    En caso de duda mejor no toques esta variable.
    
     */   
?>