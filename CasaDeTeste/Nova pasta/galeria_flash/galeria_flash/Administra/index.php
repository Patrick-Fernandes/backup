<?php require_once('../Connections/conSimple.php'); ?>
<?php require_once('../ScriptLibrary/incPureUpload.php'); ?>
<?php require_once('../ScriptLibrary/incResize.php'); ?>
<?php
// Pure PHP Upload 2.1.2
if (isset($HTTP_GET_VARS['GP_upload'])) {
	$ppu = new pureFileUpload();
	$ppu->path = "../images";
	$ppu->extensions = "GIF,JPG,JPEG,BMP,PNG";
	$ppu->formName = "Foto";
	$ppu->storeType = "file";
	$ppu->sizeLimit = "500";
	$ppu->nameConflict = "over";
	$ppu->requireUpload = "true";
	$ppu->minWidth = "";
	$ppu->minHeight = "";
	$ppu->maxWidth = "";
	$ppu->maxHeight = "";
	$ppu->saveWidth = "";
	$ppu->saveHeight = "";
	$ppu->timeout = "600";
	$ppu->progressBar = "fileCopyProgress.htm";
	$ppu->progressWidth = "300";
	$ppu->progressHeight = "100";
	$ppu->checkVersion("2.1.2");
	$ppu->doUpload();
}
$GP_uploadAction = $HTTP_SERVER_VARS['PHP_SELF'];
if (isset($HTTP_SERVER_VARS['QUERY_STRING'])) {
  if (!eregi("GP_upload=true", $HTTP_SERVER_VARS['QUERY_STRING'])) {
		$GP_uploadAction .= "?".$HTTP_SERVER_VARS['QUERY_STRING']."&GP_upload=true";
	} else {
		$GP_uploadAction .= "?".$HTTP_SERVER_VARS['QUERY_STRING'];
	}
} else {
  $GP_uploadAction .= "?"."GP_upload=true";
}

// Smart Image Processor 1.0.3
if (isset($HTTP_GET_VARS['GP_upload'])) {
  $sip = new resizeUploadedFiles($ppu);
  $sip->component = "GD";
  $sip->resizeImages = "true";
  $sip->aspectImages = "true";
  $sip->maxWidth = "640";
  $sip->maxHeight = "480";
  $sip->quality = "80";
  $sip->makeThumb = "true";
  $sip->pathThumb = "../thumbs";
  $sip->aspectThumb = "true";
  $sip->naming = "suffix";
  $sip->suffix = "";
  $sip->maxWidthThumb = "100";
  $sip->maxHeightThumb = "75";
  $sip->qualityThumb = "70";
  $sip->checkVersion("1.0.3");
  $sip->doResize();
}

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$currentPage = $_SERVER["PHP_SELF"];

$editFormAction = $HTTP_SERVER_VARS['PHP_SELF'];
if (isset($HTTP_SERVER_VARS['QUERY_STRING'])) {
  $editFormAction .= "?" . $HTTP_SERVER_VARS['QUERY_STRING'];
}

if (isset($editFormAction)) {
  if (isset($HTTP_SERVER_VARS['QUERY_STRING'])) {
	  if (!eregi("GP_upload=true", $HTTP_SERVER_VARS['QUERY_STRING'])) {
  	  $editFormAction .= "&GP_upload=true";
		}
  } else {
    $editFormAction .= "?GP_upload=true";
  }
}

if ((isset($HTTP_POST_VARS["MM_insert"])) && ($HTTP_POST_VARS["MM_insert"] == "Foto")) {
  $insertSQL = sprintf("INSERT INTO galeriaflash (Foto) VALUES (%s)",
                       GetSQLValueString($HTTP_POST_VARS['Foto'], "text"));

  mysql_select_db($database_conSimple, $conSimple);
  $Result1 = mysql_query($insertSQL, $conSimple) or die(mysql_error());

  $insertGoTo = "index.php";
  if (isset($HTTP_SERVER_VARS['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $HTTP_SERVER_VARS['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$maxRows_rsFotos = 10;
$pageNum_rsFotos = 0;
if (isset($_GET['pageNum_rsFotos'])) {
  $pageNum_rsFotos = $_GET['pageNum_rsFotos'];
}
$startRow_rsFotos = $pageNum_rsFotos * $maxRows_rsFotos;

mysql_select_db($database_conSimple, $conSimple);
$query_rsFotos = "SELECT * FROM galeriaflash ORDER BY IDFoto DESC";
$query_limit_rsFotos = sprintf("%s LIMIT %d, %d", $query_rsFotos, $startRow_rsFotos, $maxRows_rsFotos);
$rsFotos = mysql_query($query_limit_rsFotos, $conSimple) or die(mysql_error());
$row_rsFotos = mysql_fetch_assoc($rsFotos);

if (isset($_GET['totalRows_rsFotos'])) {
  $totalRows_rsFotos = $_GET['totalRows_rsFotos'];
} else {
  $all_rsFotos = mysql_query($query_rsFotos);
  $totalRows_rsFotos = mysql_num_rows($all_rsFotos);
}
$totalPages_rsFotos = ceil($totalRows_rsFotos/$maxRows_rsFotos)-1;

$queryString_rsFotos = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsFotos") == false && 
        stristr($param, "totalRows_rsFotos") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsFotos = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsFotos = sprintf("&totalRows_rsFotos=%d%s", $totalRows_rsFotos, $queryString_rsFotos);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administração Fotos</title>
<link href="css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
#apDiv1 {
	position:absolute;
	left:356px;
	top:498px;
	width:321px;
	height:300px;
	z-index:1;
}
.style1 {
	color: #FFFFFF
}
.style2 {color: #FF0000}
.style3 {color: #010101}
-->
</style>
<script language='javascript' src='../ScriptLibrary/incPureUpload.js'></script>
<script src="../SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<link href="../SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style4 {
	color: #000000;
	font-size: 14em;
}
.style5 {font-size: 14px}
-->
</style>
</head>

<body>
<div id="apDiv1">
  <table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="25">&nbsp;</td>
      <td width="100" align="center">&nbsp;</td>
      <td width="30">&nbsp;</td>
    </tr>
    <tr>
      <td width="25">&nbsp;</td>
      <td align="center">&nbsp;
        <table border="0">
          <tr>
            <td><?php if ($pageNum_rsFotos > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_rsFotos=%d%s", $currentPage, 0, $queryString_rsFotos); ?>"><img src="First.gif" border="0" /></a>
                  <?php } // Show if not first page ?>
            </td>
            <td><?php if ($pageNum_rsFotos > 0) { // Show if not first page ?>
                  <a href="<?php printf("%s?pageNum_rsFotos=%d%s", $currentPage, max(0, $pageNum_rsFotos - 1), $queryString_rsFotos); ?>"><img src="Previous.gif" border="0" /></a>
                  <?php } // Show if not first page ?>
            </td>
            <td><?php if ($pageNum_rsFotos < $totalPages_rsFotos) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_rsFotos=%d%s", $currentPage, min($totalPages_rsFotos, $pageNum_rsFotos + 1), $queryString_rsFotos); ?>"><img src="Next.gif" border="0" /></a>
                  <?php } // Show if not last page ?>
            </td>
            <td><?php if ($pageNum_rsFotos < $totalPages_rsFotos) { // Show if not last page ?>
                  <a href="<?php printf("%s?pageNum_rsFotos=%d%s", $currentPage, $totalPages_rsFotos, $queryString_rsFotos); ?>"><img src="Last.gif" border="0" /></a>
                  <?php } // Show if not last page ?>
            </td>
          </tr>
        </table></td>
      <td width="30">&nbsp;</td>
    </tr>
    <tr>
      <td width="25">&nbsp;</td>
      <td width="100" align="center">&nbsp;</td>
      <td width="30">&nbsp;</td>
    </tr>
    <?php if ($totalRows_rsFotos > 0) { // Show if recordset not empty ?>
      <?php do { ?>
        <tr>
          <td width="25">&nbsp;</td>
          <td width="100" align="center"><img src="../thumbs/<?php echo $row_rsFotos['Foto']; ?>" /> </td>
          <td width="30"><a href="excluir.php?IDFoto=<?php echo $row_rsFotos['IDFoto']; ?>"><img src="../Interface/excluir.png" width="25" height="26" border="0" /></a></td>
        </tr>
        <tr>
          <td width="25">&nbsp;</td>
          <td width="100" align="center">&nbsp;</td>
          <td width="30">&nbsp;</td>
        </tr>
        <?php } while ($row_rsFotos = mysql_fetch_assoc($rsFotos)); ?>
      <?php } // Show if recordset not empty ?>
    </table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10" height="49" background="../Interface/Top.jpg">&nbsp;</td>
    <td height="49" background="../Interface/Top.jpg" class="Titulo">ADMINISTRAÇÃO</td>
    <td width="10" height="49" background="../Interface/Top.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="58"><a href="../buildgallery.php"><img src="../Interface/gerar.png" width="46" height="45" border="0" /></a></td>
        <td width="10">&nbsp;</td>
        <td width="174" class="Titulo"><a href="#" class="style1" onclick="MM_openBrWindow('../buildgallery.php','FOTOSGERADAS','width=50,height=50')">GERAR FOTOS</a></td>
        <td width="317" class="Titulo"><a href="../index.html" target="_blank" class="style1">Visualizar Galeria</a></td>
      </tr>
    </table></td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr bgcolor="#CCCCCC">
    <td height="40" bgcolor="#CCCCCC"><span class="style3"></span></td>
    <td height="40" bgcolor="#CCCCCC"><div id="CollapsiblePanel1" class="CollapsiblePanel">
      <div class="CollapsiblePanelTab style4 style5 style2 style2 style2" tabindex="0">Atenção !</div>
      <div class="CollapsiblePanelContent"><span class="style3">Sempre que você incluir fotos ou excluir fotos clique em <span class="style2">GERAR FOTOS,</span> para que a galeria seja atualizada. Se você não clicar as novas fotos não aparecerão.</span></div>
    </div></td>
    <td height="40"><span class="style3"></span></td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A" class="texto"><a href="../index.html" target="_blank"></a></td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#666666">&nbsp;</td>
    <td bgcolor="#666666">&nbsp;</td>
    <td width="10" bgcolor="#666666">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#666666">&nbsp;</td>
    <td align="center" bgcolor="#666666" class="texto"><form action="<?php echo $editFormAction; ?>" method="post" enctype="multipart/form-data" name="Foto" id="Foto" onsubmit="checkFileUpload(this,'GIF,JPG,JPEG,BMP,PNG',true,500,'','','','','','');showProgressWindow('fileCopyProgress.htm',300,100);return document.MM_returnValue">
        FOTO: 
            <input name="Foto" type="file" id="Foto" onchange="checkOneFileUpload(this,'GIF,JPG,JPEG,BMP,PNG',true,500,'','','','','','')" />
            <br />    
            <br />
        <input type="submit" name="button" id="button" value="POSTAR FOTO" />
        <input type="hidden" name="MM_insert" value="Foto">
    </form>    </td>
    <td width="10" bgcolor="#666666">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#666666">&nbsp;</td>
    <td bgcolor="#666666">&nbsp;</td>
    <td bgcolor="#666666">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <?php if ($totalRows_rsFotos > 0) { // Show if recordset not empty ?>
    <tr>
      <td bgcolor="#4A4A4A">&nbsp;</td>
      <td align="center" bgcolor="#4A4A4A">&nbsp;<?php echo $totalRows_rsFotos ?>  Foto(s) ao Total</td>
      <td bgcolor="#4A4A4A">&nbsp;</td>
    </tr>
    <?php } // Show if recordset not empty ?>

  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td align="center" bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <?php if ($totalRows_rsFotos == 0) { // Show if recordset empty ?>
    <tr>
      <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
      <td align="center" bgcolor="#4A4A4A"><span class="style2">Sem Fotos postadas no Momento</span></td>
      <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    </tr>
    <?php } // Show if recordset empty ?>

  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td rowspan="21" bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
  <tr>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
    <td bgcolor="#4A4A4A">&nbsp;</td>
    <td width="10" bgcolor="#4A4A4A">&nbsp;</td>
  </tr>
</table>
<script type="text/javascript">
<!--
var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($rsFotos);
?>
