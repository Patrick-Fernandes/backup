<?
/*
##########################################
#     Estat�sticas de Acessos 3.0        #
# DESENVOLVIDO POR:                      #
# ROBERTO JUNDI FURUTANI                 #
# email: betolinux@bol.com.br            #
# �ltimo UpDate    20/12/2003            #
#    Favor N�o Remover                   #
##########################################
*/
// Conectando com o banco de dados
include ("connect.php");
include ("lib_area.php");

if (!defined($area)&& ($area == ""))
$area = $HTTP_GET_VARS['area'];

$info = getenv("HTTP_USER_AGENT");

//Cria um cookie.
//Evita que a visita seja contada mais de uma vez.
if ((!IsSet($visitou)) || ($visitou != "SIM")){
setcookie("visitou","SIM");
//Define qual o browser do visitante
if((ereg("Nav", $info)) || (ereg("Gold", $info)) || (ereg("X11", $info)) || (ereg("Mozilla", $info)) || (ereg("Netscape", $info)) AND (!ereg("MSIE", $info) AND (!ereg("Konqueror", $info)))) $browser = "Netscape/Mozilla";
elseif(ereg("MSIE", $info)) $browser = "MSIE";
elseif(ereg("Lynx", $info)) $browser = "Lynx";
elseif(ereg("Opera", $info)) $browser = "Opera";
elseif(ereg("WebTV", $info)) $browser = "WebTV";
elseif(ereg("Konqueror", $info)) $browser = "Konqueror";
elseif(ereg("Google", $info)) $browser = "Google";
elseif((eregi("bot", $info)) || (ereg("Slurp", $info)) || (ereg("Scooter", $info)) || (eregi("Spider", $info)) || (eregi("Infoseek", $info))) $browser = "Rob�s de Busca";
else $browser = "Desconhecido";

//Agora o Sistema Operacional
if((ereg("Windows 95", $info))) $so = "Windows 95";
elseif(ereg("Windows 98", $info)||ereg("Win98", $info)) $so = "Windows 98";
elseif(ereg("Windows NT 5.1", $info)) $so = "Windows XP";
elseif(ereg("Windows 2000", $info)|| ereg("Windows NT 5.0", $info)) $so = "Windows 2000";
elseif(ereg("Windows NT", $info)) $so = "Windows NT";
elseif(ereg("Linux", $info)) $so = "Linux";
elseif(ereg("Mac", $info)) $so = "Mac";
elseif(ereg("SunOS", $info)) $so = "SunOS";
elseif(ereg("IRIX", $info)) $so = "IRIX";
elseif(ereg("OS/2", $info)) $so = "OS/2";
elseif(ereg("AIX", $info)) $so = "AIX";
else $so = "Outros";

$data = date("Y-m-d");

//Insere os dados do visitantes
//Browser, Sist. Operacional e a data do site na tabela acessos
$sql = "INSERT INTO acessos(browser,so,data) VALUES ('$browser','$so','$data')";
$insere = mysql_query($sql);
}

//Se a area n�o estiver definida, a area ser� tratada como indefinida
if (!defined($area)&& ($area == ""))
$area = "indefinida";

//Verifica se a area existe
if (isArea($area)){
//Atualiza a quantidade de visitas por Area na tabela areas
$sql = "UPDATE areas SET num_acessos = num_acessos+1 WHERE area='$area'";
$insere = mysql_query($sql);
@mysql_free_result($insere);
}
else
echo "A area [$area] n�o esta cadastrada";
mysql_close();
?>
