<?php

class DocumentoDAL extends Exceptions implements Template {

    private $resp;
    private $db;

    public function __construct() {
        $this->db = new DB();
    }

    public function getResp() {
        return $this->resp;
    }

    public function buscar($extra="") {
        try {
            $sql = "SELECT * FROM documento " . $extra;

            $this->db->query($sql);
            if ($this->db->quantidadeRegistros() > 0) {
                while ($obj = $this->db->fetchObj()) {
                    //$codigo="",$secao="",$material="",$quantidade="",$preco="",$datacad="",$codgrupo="",$codusuario="",$destaque="") {
                    $arr[] = new DocumentoTO(
                                    $obj->id,
                                    $obj->nome,
                                    $obj->id_usuario,
                                    $obj->link,
                                    $obj->status,
                                    $obj->data);
                }
                $this->resp = $arr;
                return true;
            } else {
                $arr[] = new DocumentoTO('0', '0','0','0', '0', '0');
                $this->resp = $arr;
                return false;
            }
        } catch (Exception $e) {
            $arr[] = new DocumentoTO($e->getMessage(),$e->getMessage(), $e->getMessage(), 
                                    $e->getMessage(), $e->getMessage(), $e->getMessage());
            $this->resp = $arr;
            return false;
        }
    }

   
    public function inserir($documento) {
        try {
            $dados = "'" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getId()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getNome()) . "',";
         $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getId_usuario()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getLink()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getStatus()) . "',";
            $dados .= "'" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getData()) . "'";

            $sql = "INSERT INTO documento VALUES (" . $dados . ")";
            if ($this->db->query($sql)) {
                $this->resp = "Dados inseridos.";
                //echo $sql;
                return true;
            } else {
                $this->resp = "Dados n�o inseridos.<br>" . $this->db->getErro();
                echo $sql;
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionDocumento("DocumentoDAL", "inserirDocumento");
            $this->resp = $e->getMessage();
            return false;
        }
    }

    public function atualizar($documento) {
        try {
            $sql = "UPDATE documento SET
                    nome='" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getNome()) . "',
                    id_usuario='" .mysqli_real_escape_string($this->db->LigarConexao(),$documento->getId_usuario()) . "',
                    link='" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getLink()) . "',
                    status='" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getStatus()) . "',
                    data='" .mysqli_real_escape_string($this->db->LigarConexao(),$documento->getData()) . "'
                WHERE id='" . mysqli_real_escape_string($this->db->LigarConexao(),$documento->getId()) . "'";
            if ($this->db->query($sql)) {
                $this->resp = "Dados atualizados.";
              //  echo $sql;
                return true;
            } else {
                $this->resp = "Dados n�o atualizados.<br>" . $this->db->getErro();
               // echo $sql;
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionDocumento("DocumentoDAL", "atualizarDocumento");
            $this->resp = $e->getMessage();
            return false;
        }
    }

    public function excluir($codigo) {
        try {
            
            $sql = "DELETE FROM documento WHERE id='" .mysqli_real_escape_string($this->db->LigarConexao(),$codigo) . "'";
    
            if ($this->db->query($sql)) {
                $this->resp = "Dados deletados.";
                return true;
            } else {
                $this->resp = "Dados n�o deletados.<br>" . $this->db->getErro();
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionDocumento("DocumentoDAL", "deletaDocumento");
            $this->resp = $e->getMessage();
            return false;
        }
    }

    public function contar($extra="") {
        try {
            $sql = "SELECT * FROM documento " . $extra;
            $this->db->query($sql);
            if ($this->db->quantidadeRegistros() > 0) {
                $this->resp = $this->db->quantidadeRegistros();
                return true;
            } else {
                $this->resp = "0";
                $this->resp = "Nenhum registro encontrado.";
                return false;
            }
        } catch (Exception $e) {
            parent::exceptionDocumento("DocumentoDAL", "contarDocumento");
            $this->resp = $e->getMessage();
            return false;
        }
    }
    
    public function status($id, $status) {
		try {
			if ($status == "Ativo")
			$sql = "UPDATE documento SET status='Inativo' WHERE id=$id";
			
			 else
			 $sql = "UPDATE documento SET status='Ativo' WHERE id=$id";
			 
			if($this->db->query($sql)) {
				$this->resp = "Dados atualizados.";
				return true;
			} else {
				$this->resp = "Dados n�o atualizados.<br>".$this->db->getErro();
				return false;
			}
		} catch (Exception $e) {
			parent::exceptionCurso("CursoDAL","atualizarCurso");
			$this->resp = $e->getMessage();
			return false;
		}
	}

}

?>