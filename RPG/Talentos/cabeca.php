<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, maximum-scale=1">
        <title>Coordenadoria de Produção e Tecnologia Educacional</title>
        <link rel="icon" href="img/small-logo.png" type="image/png">
        <link rel="shortcut icon" href="img/favicon.ico" type="img/x-icon">
        <link href="Estilos/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="Estilos/css/style.css" rel="stylesheet" type="text/css">
        <link href="Estilos/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="Estilos/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="Estilos/css/animate.css" rel="stylesheet" type="text/css">
    </head>
    <body>

        <style>
            .gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter , tics, p{
                text-align: left;
            }
            section.paddind div.container div.row div.gallery_product{
                height: 50px;
            }

            section.paddind div.container div.row div.gallery_product p a{
                font-size: 1.6em;
            }
            @media screen and (max-width: 1128px) {
                section.paddind div.container div.row div.gallery_product p a{
                    font-size: 1.3em;
                }

                section.paddind div.container div.row div.gallery_product{
                    height: 35px;
                }

            }
            @media screen and (max-width: 680px) {
                section.paddind div.container div.row div.gallery_product p a{
                    font-size: 1em;
                }
                section.paddind div.container div.row div.gallery_product{
                    height: 30px;
                }

            }

        </style>
<?php include './conexao.php';?>