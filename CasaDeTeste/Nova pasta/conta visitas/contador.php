<?php
/****************************************************************
*@Author: Eduardo Stuart - eduardo@eduardostuart.com
*@Web: http://www.eduardostuart.com
*@Description: Simples contador de visitas que utiliza db-mysql
*@E-mail: eduardo@eduardostuart.com
****************************************************************/

//Class conexao com db.
class conexao
{	var $userdb	= "root";
	var $passdb = "";
	var $hostdb = "localhost";
	var $namedb = "banco";

	function conecta()
	{		$conecta = mysql_connect($this->hostdb,$this->userdb,$this->passdb)or die(mysql_error());
		mysql_select_db($this->namedb,$conecta)or die(mysql_error());
	}
}
//Class contador
class contador extends conexao
{	var $ip;
	var $data;
    var $tabela = "contador";

	function criaConexao()
	{		$conexao = new conexao;
		$conexao->conecta();
	}
	function pegaData()
	{		$this->data = date("d/m/Y");
	}
	function pegaIp()
	{		$this->ip   = $_SERVER['REMOTE_ADDR'];
	}
	function visitaHoje()
	{		$this->criaConexao();
		$sql = mysql_query("SELECT ip,data FROM $this->tabela WHERE data='$this->data' AND ip='$this->ip'") or die(mysql_error());
	    if(mysql_num_rows($sql) <= "0")
	    {	    	mysql_query("INSERT INTO $this->tabela (id,data,ip) VALUES ('','$this->data','$this->ip')")or die(mysql_error());
	    }
	 }
	 function mostraVisitantes()
	 {	 	$this->criaConexao();
	 	$sql  = mysql_query("SELECT * FROM $this->tabela")or die(mysql_error());
	 	$total= mysql_num_rows($sql);
		echo "Total de visitas: ".$total;
	}
	function mostraVisitantesHoje()
	{		$this->criaConexao();
		$sql   = mysql_query("SELECT * FROM $this->tabela WHERE data='$this->data'") or die(mysql_error());
		$total = mysql_num_rows($sql);
		echo "Visitas hoje: ".$total;
	}
}
?>