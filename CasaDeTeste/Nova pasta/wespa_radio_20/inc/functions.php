 <?php
// Cria espa�o na config 
function section_break() { return "\n\n"; }

// Cria comentarios da config 
function comment_string( $comment ) {
   return '/* ' . $comment . ' */' . "\n";
}

// Cria as variaveis da config 
function set_val( $val_name, $value , $comment = '' ) {
   $val_string = $val_name;
   for( $i = strlen( $val_name ); $i < 23; $i++ ) {
      $val_string .= ' ';
   }
   $val_string .= ' = ';

   if ( is_string( $value ) && $value != 'true' && $value != 'false' ) { 
   $val_string .= '"' . $value . '";' . " " ;
   } else {
   $val_string .= $value . ';' . " " ;
   }

   if ( $comment  != '' ) {
      for( $i = strlen( $comment ) ; $i < 15; $i++ ) {
         $val_string .= ' ';
      }
      $val_string .= '// ' . $comment . "\n";
   }

   return $val_string;
} 

// Arruma as informa��es para a config
function ContentConfig() {
   global $_POST;

   $config = '';
   $config .= '<?php' . "\n";
   
   $config .= comment_string( 'WESPA R�dio Arquivo de configura��o.' );
   $config .= comment_string( 'Este arquivo � criado com a instala��o do Script.' );
   $config .= comment_string( 'N�o delete-o nem modifique manualmente.' );
   $config .= section_break();

   $config .= comment_string( 'Variaveis do Script:' );
   $config .= set_val( '$ext', $_POST['ext'], 'Exten��o que ser� utilizada para o script' );
   $config .= set_val( '$pasta', $_POST['pasta'], 'Pasta onde ficam armazenadas as musicas.' );
   $config .= section_break();

   $config .= comment_string( 'Variaveis do Banco de Dados:' );
   $config .= set_val( '$host', $_POST['host'], 'Host do servidor MySQL' );
   $config .= set_val( '$user_bd', $_POST['user_bd'], 'Usuario do servidor MySQL' );
   $config .= set_val( '$senha_bd', $_POST['senha_bd'], 'Senha do servidor MySQL' );
   $config .= set_val( '$banco', $_POST['banco'], 'Nome do Banco de dados a ser utilizado' );
   $config .= section_break();
   
   $config .= '?>' . "\n";
   return $config;
}

// Arruma as informa��es para o backup 
function ContentBackup() {
   require_once("../inc/config.php");
   require_once("../inc/functions.php");
   con();
     
   $backup = '';
   $backup .= comment_string( 'WESPA R�dio Backup do banco de dados.' );
   $backup .= comment_string( 'N�o delete-o nem modifique manualmente.' );
   $backup .= section_break();

   $backup .= comment_string( 'Tabela de Locutores e Autores (radio_artistas)' );  
   $query = "SELECT * FROM radio_artistas";  
   $resultado = mysql_query($query);  
   while($valores = mysql_fetch_array($resultado)) {  
   $nome = $valores[nome];  
   $nome = addslashes($nome);  
   $backup .= "INSERT INTO radio_artistas VALUES ('$valores[id]', '$nome');\n"; }  
   $backup .= section_break();  
  
   $backup .= comment_string( 'Tabela de Arquivos de �udio (radio_musicas)' );  
   $query2 = "SELECT * FROM radio_musicas";  
   $resultado2 = mysql_query($query2);  
   while($valores2 = mysql_fetch_array($resultado2)) {  
   $artista = $valores2[artista];  
   $artista = addslashes($artista);  
   $nome = $valores2[nome];  
   $nome = addslashes($nome);  
   $arquivo = $valores2[arquivo];  
   $arquivo = addslashes($arquivo);  
   $backup .= "INSERT INTO radio_musicas VALUES ('$valores2[id]', '$artista', '$nome', '$arquivo');\n"; }  
   $backup .= section_break();
       
   $backup .= comment_string( 'Tabela de Usu�rios (radio_users)' );    
   $query3 = "SELECT * FROM radio_users";      
   $resultado3 = mysql_query($query3);      
   while($valores3 = mysql_fetch_array($resultado3)){     
   $backup .= "INSERT INTO radio_users VALUES ('$valores3[id]', '$valores3[nome]', '$valores3[login]', '$valores3[senha]', '$valores3[email]');\n";}    
   $backup .= section_break();  
   
   return $backup;
}

// Grava a config 
function SaveConfig(){
        global $config;
		$config = ContentConfig();
		$fTempName = @tempnam("../inc/", "../inc/temp_");   if ($fTempName === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel criar o arquivo tempor�rio!</font></center>"); }
		$fFile = @fopen($fTempName, "w");         if ($fFile === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel abrir o arquivo tempor�rio!</font></center>"); }
		$fAct = @fputs($fFile, $config);        if ($fAct === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel gravar no arquivo tempor�rio!</font></center>"); }
		$fAct = @fclose($fFile);                  if ($fAct === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel fechar o arquivo tempor�rio!</font></center>"); }
		$fAct = @copy($fTempName, "../inc/config.php");  if ($fAct === FALSE && !@file_exists($fName)) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel copiar do arquivo tempor�rio para o config.php!</font></center>"); }
		$fAct = @unlink($fTempName);              if ($fAct === FALSE && @file_exists($fTempName)) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel remover o arquivo temporario!</font></center>"); }

}

// Grava o backup 
function SaveBackup(){
        global $backup;
		$backup = ContentBackup();
		$fTempName = @tempnam("../inc/", "../inc/temp_");   if ($fTempName === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel criar o arquivo tempor�rio!</font></center>"); }
		$fFile = @fopen($fTempName, "w");         if ($fFile === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel abrir o arquivo tempor�rio!</font></center>"); }
		$fAct = @fputs($fFile, $backup);        if ($fAct === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel gravar no arquivo tempor�rio!</font></center>"); }
		$fAct = @fclose($fFile);                  if ($fAct === FALSE) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel fechar o arquivo tempor�rio!</font></center>"); }
		$fAct = @copy($fTempName, "../inc/backup.sql");  if ($fAct === FALSE && !@file_exists($fName)) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel copiar do arquivo tempor�rio para o backup.sql!</font></center>"); }
		$fAct = @unlink($fTempName);              if ($fAct === FALSE && @file_exists($fTempName)) {echo("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel remover o arquivo temporario!</font></center>"); }

}

 // Conecta com o Banco de Dados
 function con() {   
 global $host; global $user_bd; global $senha_bd; global $banco; 
 $connect = @mysql_connect("$host", "$user_bd", "$senha_bd") or die ("<font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel conectar ao MySQL!</font>"); 
 mysql_select_db("$banco") or die("<center><font face=\"Verdana, Arial,Helvetica\" size=\"1\"><font color=\"red\" size=\"1\"><b>Erro:</b></font> N�o � possivel selecionar o Banco de Dados!</font></center>"); 
 }
 
 ?>