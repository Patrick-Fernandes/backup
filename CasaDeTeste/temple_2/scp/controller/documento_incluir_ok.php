<?php
include ('../include/header.php');

date_default_timezone_set("America/Sao_Paulo");
$data = date("Y-m-d");
$time = date("Ymd");

$tit = $_FILES['arquivo']['name'];
$lixo = array("select", "select", "insert", "update", "delete", "--", "'", "<", ">", "union", "php");

if (preg_match(sprintf('/%s/i', implode('|', $lixo)), $tit)) {
    echo "<script>alert('Verifique o nome de seu arquivo');history.go(-1);</script>";
    exit;
}

//fim alterao

$diretorio = "../files/";

if (!is_dir($diretorio)) {
    echo "Pasta $diretorio no existe";
} else {

    $arquivo = isset($_FILES['arquivo']) ? $_FILES['arquivo'] : FALSE;
    $arquivo1 = $arquivo['name'];

    if (!empty($arquivo1)) {

        $randon = substr(hash('md5',time().rand()),0,10);
        $destino = $diretorio . $randon.$arquivo1;

        $nome = $arquivo1;
        
        
        $status = 'Ativo';
        $id_usuario = $_SESSION["login"]->getId();
        $link = $randon.$arquivo1;

        if ($arquivo['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                $arquivo['type'] == "application/msword") {

            if (move_uploaded_file($arquivo['tmp_name'], $destino)) {

                $documento = new DocumentoTO();


                
              
                $documento->setNome($nome);
                $documento->setId_usuario($id_usuario);
                $documento->setLink($link);
                $documento->setStatus($status);
                $documento->setData($data);

                $documentoDAL = new DocumentoDAL();
                $retorno = $documentoDAL->inserir($documento);

                if ($retorno == true) {

                    echo "<script>
                         alert('Cadastrado com sucesso!');
                         history.go(-2);
                         </script>";
                } else {
                    echo "<script>
                            alert('No foi possivel carregar o documento');
                            </script>";
                }
            } else {
                echo "<script>
            alert('Erro ao Mover o arquivo!');
            </script>";
            }
        } else {
            ?>

<div id="page-wrapper">
    <div class="container-fluid">

        <?= montaCab("Erro!", "<i class='fa fa-upload'></i>Arquivo invalido"," ") ?>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php
                                echo "<br />O arquivo<o style='color: red;'> " . $arquivo1 . " </o> invalido.<br />So permitidos apenas documentos de texto. (.doc e .docx).";
                                ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" onClick="history.go(-1)" class="btn btn-default">Voltar</button>
                    <br><br>
                </div>
            </div>


            <?php
        }
    } else {
        echo "<script>
                         alert(' preciso anexar um arquivo!');
                         history.go(-1);
                         </script>";
    }
} // fecha else
?>

<?php include ('../include/footer.php'); ?>
