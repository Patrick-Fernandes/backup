CREATE TABLE `admin` (
  `id` int(5) NOT NULL default '0',
  `login` varchar(255) NOT NULL default '',
  `senha` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

INSERT INTO `admin` VALUES (1, 'admin', 'admin');

CREATE TABLE `cat` (
  `id` int(5) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=48 ;

INSERT INTO `cat` VALUES (1, 'Adultas');
INSERT INTO `cat` VALUES (2, 'Advogados');
INSERT INTO `cat` VALUES (3, 'An�es');
INSERT INTO `cat` VALUES (4, 'Argentinos');
INSERT INTO `cat` VALUES (5, 'B�bados');
INSERT INTO `cat` VALUES (6, 'Bichas');
INSERT INTO `cat` VALUES (7, 'Caipiras');
INSERT INTO `cat` VALUES (8, 'Casamento');
INSERT INTO `cat` VALUES (9, 'Charadas');
INSERT INTO `cat` VALUES (10, 'Cornos');
INSERT INTO `cat` VALUES (11, 'C�mulos');
INSERT INTO `cat` VALUES (12, 'Curtas');
INSERT INTO `cat` VALUES (13, 'Empregados');
INSERT INTO `cat` VALUES (14, 'Feministas');
INSERT INTO `cat` VALUES (15, 'Futebol');
INSERT INTO `cat` VALUES (16, 'Gagos');
INSERT INTO `cat` VALUES (17, 'Galos');
INSERT INTO `cat` VALUES (18, 'Ga�chos');
INSERT INTO `cat` VALUES (19, 'Idosos');
INSERT INTO `cat` VALUES (21, 'Jo�ozinho');
INSERT INTO `cat` VALUES (22, 'Judeus');
INSERT INTO `cat` VALUES (23, 'Ladr�o');
INSERT INTO `cat` VALUES (24, 'Loiras');
INSERT INTO `cat` VALUES (25, 'Loucos');
INSERT INTO `cat` VALUES (26, 'Machistas');
INSERT INTO `cat` VALUES (27, 'M�dicos');
INSERT INTO `cat` VALUES (28, 'Mineiro');
INSERT INTO `cat` VALUES (29, 'Nome do Filme');
INSERT INTO `cat` VALUES (30, 'Papagaios');
INSERT INTO `cat` VALUES (31, 'Pol�ticos');
INSERT INTO `cat` VALUES (32, 'Outras');
INSERT INTO `cat` VALUES (33, 'Pontinhos');
INSERT INTO `cat` VALUES (34, 'Portugu�s');
INSERT INTO `cat` VALUES (35, 'Secret�rias');

CREATE TABLE `espera` (
  `id` int(5) NOT NULL auto_increment,
  `id_cat` bigint(20) NOT NULL default '0',
  `titulo` varchar(255) NOT NULL default '',
  `piada` text NOT NULL,
  `enviado` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

CREATE TABLE `piadas` (
  `id` int(5) NOT NULL auto_increment,
  `id_cat` bigint(20) NOT NULL default '0',
  `titulo` varchar(255) NOT NULL default '',
  `piada` text NOT NULL,
  `enviado` varchar(255) NOT NULL default '',
  `cliques` bigint(20) NOT NULL default '0',
  `data` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=27 ;