<!--ATEN��O ANTES DE ALTERAR AS URL VERIFIQUE OS ARQUIVOS .HTACCESS-->


<?
if (!is_numeric($_GET["cod"])) {
    ?>
    <script language="javascript">
        history.back(-1);
    </script>
    <?
}
$cod = $_GET["cod"];

include("../include/header.php");
//Busca o grupo
$uDAL = new UsuarioDAL();
$retorno = $uDAL->buscar("where id=$cod");
if ($retorno) {
    $resp = $uDAL->getResp();
    $usuario = $resp[0];
} else {
    ?>
    <script language="javascript">
        alert("Registro n�o encontrado");
        history.back(-1);
    </script>
    <?
}

if ($usuario->getStatus() == 0) {
    $optionsSts = "<option value='0' selected>Inativo</option>
<option value='1'>Ativo</option>";
} else {
    $optionsSts = "<option value='1' selected>Ativo</option>
<option value='0'>Inativo</option>";
}


if ($usuario->getNivel() == 2) {
    $optionsNivelUsuario = "<option value='2' selected>Usu�rio</option>
                        <option value='1'>Administrador</option>";
} else {
    $optionsNivelUsuario = "<option value='2'>Usu�rio</option>
                        <option value='1' selected>Administrador</option>";
}



$nivel = $usuario->getNivel();
if ($nivel == 1) {
    $nome = "Administador";
} else {
    $nome = "Usu�rio";
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
                    <?= montaCab("Usuario", "Alterar"," ")?>
    
        <div class="row">
            <div class="col-lg-6">
                <form method="post" action="../Usuario_Alteracao">

         
            <p><label> Nome</label>
                <input type="text" size=80 name="txtNome" id="txtNome" class="w3-input w3-border w3-round tam-max" value="<?= $usuario->getNome() ?>"/>
            </p>

            <p ><label> E-mail</label>
                <input type="text" class="w3-input w3-border w3-round tam-max" name="txtEmail" id="txtEmail" value="<?= $usuario->getEmail() ?>"/></p>
            
            <br>
            <p><label> Login</label>
                <input type="text" class="w3-input w3-border w3-round tam-max-senha" name="txtLogin" id="txtLogin" value="<?= $usuario->getLogin() ?>"/>
            </p>

            <p><label> Alterar senha</label>
                <input type="password" class="w3-input w3-border w3-round tam-max-senha" name="txtSenha" id="txtSenha"/></p>

            <p><label> Repita sua senha: </label>
                <input type="password" class="w3-input w3-border w3-round tam-max-senha" name="txtConfereSenha" id="txtConfereSenha"/></p>

            <br>
            <p>N�vel atual do usu�rio :<b style="color: red">"<?= $nome ?>"</b>
            </p>

            <p><label> N�vel</label>
                <select name="txtNivel" id="txtSts" class="w3-input w3-border w3-round tam-max-option"><?= $optionsNivelUsuario ?></select><br />


            </p>

            <p>
                <label class="label_cad">Status:</label>
                <select name="txtSts" id="txtSts" class="w3-input w3-border w3-round tam-max-option"><?= $optionsSts ?></select><br />
            </p>

            <input type="hidden" name="cod" id="cod" value="<?= $usuario->getId() ?>"/>
            <input type="submit" value="Alterar"  class="btn btn-default" />
            <input type="button"  class="btn btn-default" value="Voltar" onClick="history.go(-1)"> 
       
                </form>
                <br>
            </div>
        </div>
    </div>
</div>

<?php include("../include/footer.php"); ?>
