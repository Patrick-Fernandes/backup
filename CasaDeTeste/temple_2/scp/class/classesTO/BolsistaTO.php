<?php

class BolsistaTO {

    private $id;
    private $nome;
    private $cpf;
    private $rg;
    private $funcao;
    private $status;
    private $nit;
    private $siape;
    private $nacionalidade;
    private $municipio;
    private $nascimento;
    private $banco;
    private $agencia;
    private $conta_bancaria;
    private $municipio_de_atuacao;
    private $logradouro;
    private $cep;
    private $telefone;
    private $email;
    private $curso;
    private $disciplina;
    private $carga_horaria;
    private $data_inicio;
    private $data_fim;
    private $programa;

    function __construct($id = "", $nome = "", $cpf = "", $rg = "", $funcao = "", $status = "", $nit = "", $siape = "", $nacionalidade = "", $municipio = "", $nascimento = "", $banco = "", $agencia = "", $conta_bancaria = "", $municipio_de_atuacao = "", $logradouro = "", $cep = "", $telefone = "", $email = "", $curso = "", $disciplina = "", $carga_horaria = "", $data_inicio = "", $data_fim = "", $programa = "") {
        $this->id = $id;
        $this->nome = $nome;
        $this->cpf = $cpf;
        $this->rg = $rg;
        $this->funcao = $funcao;
        $this->status = $status;
        $this->nit = $nit;
        $this->siape = $siape;
        $this->nacionalidade = $nacionalidade;
        $this->municipio = $municipio;
        $this->nascimento = $nascimento;
        $this->banco = $banco;
        $this->agencia = $agencia;
        $this->conta_bancaria = $conta_bancaria;
        $this->municipio_de_atuacao = $municipio_de_atuacao;
        $this->logradouro = $logradouro;
        $this->cep = $cep;
        $this->telefone = $telefone;
        $this->email = $email;
        $this->curso = $curso;
        $this->disciplina = $disciplina;
        $this->carga_horaria = $carga_horaria;
        $this->data_inicio = $data_inicio;
        $this->data_fim = $data_fim;
        $this->programa = $programa;
    }

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getRg() {
        return $this->rg;
    }

    function getFuncao() {
        return $this->funcao;
    }

    function getStatus() {
        return $this->status;
    }

    function getNit() {
        return $this->nit;
    }

    function getSiape() {
        return $this->siape;
    }

    function getNacionalidade() {
        return $this->nacionalidade;
    }

    function getMunicipio() {
        return $this->municipio;
    }

    function getNascimento() {
        return $this->nascimento;
    }

    function getBanco() {
        return $this->banco;
    }

    function getAgencia() {
        return $this->agencia;
    }

    function getConta_bancaria() {
        return $this->conta_bancaria;
    }

    function getMunicipio_de_atuacao() {
        return $this->municipio_de_atuacao;
    }

    function getLogradouro() {
        return $this->logradouro;
    }

    function getCep() {
        return $this->cep;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getEmail() {
        return $this->email;
    }

    function getCurso() {
        return $this->curso;
    }

    function getDisciplina() {
        return $this->disciplina;
    }

    function getCarga_horaria() {
        return $this->carga_horaria;
    }

    function getData_inicio() {
        return $this->data_inicio;
    }

    function getData_fim() {
        return $this->data_fim;
    }

    function getPrograma() {
        return $this->programa;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setRg($rg) {
        $this->rg = $rg;
    }

    function setFuncao($funcao) {
        $this->funcao = $funcao;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setNit($nit) {
        $this->nit = $nit;
    }

    function setSiape($siape) {
        $this->siape = $siape;
    }

    function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;
    }

    function setMunicipio($municipio) {
        $this->municipio = $municipio;
    }

    function setNascimento($nascimento) {
        $this->nascimento = $nascimento;
    }

    function setBanco($banco) {
        $this->banco = $banco;
    }

    function setAgencia($agencia) {
        $this->agencia = $agencia;
    }

    function setConta_bancaria($conta_bancaria) {
        $this->conta_bancaria = $conta_bancaria;
    }

    function setMunicipio_de_atuacao($municipio_de_atuacao) {
        $this->municipio_de_atuacao = $municipio_de_atuacao;
    }

    function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setCurso($curso) {
        $this->curso = $curso;
    }

    function setDisciplina($disciplina) {
        $this->disciplina = $disciplina;
    }

    function setCarga_horaria($carga_horaria) {
        $this->carga_horaria = $carga_horaria;
    }

    function setData_inicio($data_inicio) {
        $this->data_inicio = $data_inicio;
    }

    function setData_fim($data_fim) {
        $this->data_fim = $data_fim;
    }

    function setPrograma($programa) {
        $this->programa = $programa;
    }

}
