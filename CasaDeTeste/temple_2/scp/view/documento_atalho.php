<?
$cab = "Incluir Documentos";
include ('../include/header.php');

if (!is_numeric($_GET["cod"])) {
    ?>
    <script language="javascript">
        history.back(-1);
    </script>
    <?
}
$cod = $_GET["cod"];

//Busca o grupo
$cDAL = new CursoDAL();
$retorno = $cDAL->buscar("where id=$cod");
if ($retorno) {
    $resp = $cDAL->getResp();
    $curso = $resp[0];
} else {
    ?>
    <script language="javascript">
        alert("Registro n�o encontrado");
        history.back(-1);
    </script>
    <?
}

$cursoDAL = new CursoDAL();
$retorno = $cursoDAL->buscar(" order by nome");
if ($retorno == true) {
    $cursos = $cursoDAL->getResp();
    $optionsCUR = "";
    for ($i = 0; $i < count($cursos); $i++) {
        if ($curso->getId() == $cursos[$i]->getId()) {
            $optionsCUR .="<option value='" . $cursos[$i]->getId() . "' selected>" . $cursos[$i]->getNome() . " / " . $cursos[$i]->getNomeCampus() . " / " . $cursos[$i]->getNomeForma() . " / " . $cursos[$i]->getNomeModalidade() . "</option>";
        } else {
            $optionsCUR .="<option value='" . $cursos[$i]->getId() . "'>" . $cursos[$i]->getNome() . " / " . $cursos[$i]->getNomeCampus() . " / " . $cursos[$i]->getNomeForma() . " / " . $cursos[$i]->getNomeModalidade() . "</option>";
        }
    }
}

$anoDAL = new AnoDAL();
$retorno = $anoDAL->buscar();
if ($retorno == true) {
    $ano = $anoDAL->getResp();
    $optionsANO = "";
    for ($i = 0; $i < count($ano); $i++) {
        $optionsANO .="<option value='" . $ano[$i]->getAno() . "'>" . $ano[$i]->getAno() . "</option>";
    }
}

$vigenciaDAL = new VigenciaDAL();
$retorno = $vigenciaDAL->buscar();
if ($retorno == true) {
    $vigencia = $vigenciaDAL->getResp();
    $optionsVIGI = "";
    for ($i = 0; $i < count($vigencia); $i++) {
        $optionsVIGI .="<option value='" . $vigencia[$i]->getSemestre() . "'>" . $vigencia[$i]->getSemestre() . "</option>";
    }
}
?>



<div id="divConteudo">
    <div id="divConteudoTitulo">
        <?= $cab ?>
    </div>

    <form action="../Usuario_incluir" name="documento" method="post" enctype="multipart/form-data">

        <p class="linhasForm"><label for="txtCurso"> Curso:</label> 
            <select name="txtCurso" id="txtCurso"><?= $optionsCUR ?></select>
        </p>

        <div class="campo">
            <p class="campoInterno">

                <label for="Vigencia"> Data</label> 
                <select name="txtAno[]" id="txtAno"><?= $optionsANO ?></select>&nbsp;
                <select name="txtVigi[]" id="txtVigi"><?= $optionsVIGI ?></select>&nbsp;<br />

                <label for="file"> Arquivo:</label> 
                <input type="file" name="arquivo[]" id="arquivo" /><br />

                <label>Status</label>
                <select name ="txtStatus[]" id="txtStatus">
                    <option value="Ativo">Ativo</option>
                    <option value="Inativo">Inativo</option>
                </select>

                <button type="button" style="background: white; border-radius: 5px; border: 1px solid #F6C739;" class="removerCampo">- Remover campo</button>
            </p>
        </div>

        <p><button type="button" class="adicionarCampo">+ Adicionar campo</button>
        </p>



        <input type="hidden" size=40 name="txtLogin" id="txtlogin" maxlength="20" value="
               <? echo $_SESSION["login"]->getId(); ?>"/>

        <div class="bt">
            <div class="bt-cadastrar" onclick="documento.submit()"></div>
            <div class="bt-limpar" onclick="documento.reset()"></div>
            <div class="bt-cancelar" onclick="window.close();"></div><br />
        </div>

    </form>


</div>
<? include('../include/footer.php'); ?>

</body>
</html>