<?php

class DocumentoTO {

    private $id;
    private $nome;
    
    private $id_usuario;
    private $link;
    private $status;
    private $data;

    function __construct($id="", $nome="", $id_usuario="", $link="", $status="", $data="") {
        $this->id = $id;
        $this->nome = $nome;
        $this->id_usuario = $id_usuario;
        $this->link = $link;
        $this->status = $status;
        $this->data = $data;
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

        
    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function getLink() {
        return $this->link;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function getNomeEnsino() {
        $cod = "";
        if ($this->id_curso != "") {
            $cursoDAL = new CursoDAL();

            $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
            if ($ret == true) {
                $linha = $cursoDAL->getResp();
                $cod = $linha[0]->getId_ensino();

                $nome = "";
                if ($cod != "") {
                    $ensinoDAL = new EnsinoDAL();

                    $ret = $ensinoDAL->buscar("where id=" . $cod);
                    if ($ret == true) {
                        $linha = $ensinoDAL->getResp();
                        $nome = $linha[0]->getNome();
                    }
                }
            }
        }

        return $nome;
    }

    public function getNomeCampus() {
        $cod = "";
        if ($this->id_curso != "") {
            $cursoDAL = new CursoDAL();

            $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
            if ($ret == true) {
                $linha = $cursoDAL->getResp();
                $cod = $linha[0]->getId_campus();

                $nome = "";
                if ($cod != "") {
                    $campusDAL = new CampusDAL();

                    $ret = $campusDAL->buscar("where id=" . $cod);
                    if ($ret == true) {
                        $linha = $campusDAL->getResp();
                        $nome = $linha[0]->getNome();
                    }
                }
            }
        }

        return $nome;
    }
    
    public function getSiglaCampus() {
        $cod = "";
        if ($this->id_curso != "") {
            $cursoDAL = new CursoDAL();

            $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
            if ($ret == true) {
                $linha = $cursoDAL->getResp();
                $cod = $linha[0]->getId_campus();

                $nome = "";
                if ($cod != "") {
                    $campusDAL = new CampusDAL();

                    $ret = $campusDAL->buscar("where id=" . $cod);
                    if ($ret == true) {
                        $linha = $campusDAL->getResp();
                        $nome = $linha[0]->getSigla();
                    }
                }
            }
        }

        return $nome;
    }
 
    public function getSiglaCurso() {
            $nome = "";
            if ($this->id_curso != "") {
                $cursoDAL = new CursoDAL();

                $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
                if ($ret == true) {
                    $linha = $cursoDAL->getResp();
                    $nome = $linha[0]->getSigla();
                }
            }

            return $nome;
        }
    
    public function getNomeCurso() {
        $nome = "";
        if ($this->id_curso != "") {
            $cursoDAL = new CursoDAL();

            $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
            if ($ret == true) {
                $linha = $cursoDAL->getResp();
                $nome = $linha[0]->getNome();
            }
        }

        return $nome;
    }
    
    public function getNomeUsuario() {
        $nome = "";
        if ($this->id_usuario != "") {
            $usuarioDAL = new UsuarioDAL();

            $ret = $usuarioDAL->buscar("where id=" . $this->id_usuario);
            if ($ret == true) {
                $linha = $usuarioDAL->getResp();
                $nome = $linha[0]->getNome();
            }
        }

        return $nome;
    }
    
    public function getNomeForma() {
        $cod = "";
        if ($this->id_curso != "") {
            $cursoDAL = new CursoDAL();

            $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
            if ($ret == true) {
                $linha = $cursoDAL->getResp();
                $cod = $linha[0]->getId_forma();

                $nome = "";
                if ($cod != "") {
                    $formaDAL = new FormaDAL();

                    $ret = $formaDAL->buscar("where id=" . $cod);
                    if ($ret == true) {
                        $linha = $formaDAL->getResp();
                        $nome = $linha[0]->getNome();
                    }
                }
            }
        }

        return $nome;
    }
    
    public function getNomeModalidade() {
        $cod = "";
        if ($this->id_curso != "") {
            $cursoDAL = new CursoDAL();

            $ret = $cursoDAL->buscar("where id=" . $this->id_curso);
            if ($ret == true) {
                $linha = $cursoDAL->getResp();
                $cod = $linha[0]->getId_modalidade();

                $nome = "";
                if ($cod != "") {
                    $modalidadeDAL = new ModalidadeDAL();

                    $ret = $modalidadeDAL->buscar("where id=" . $cod);
                    if ($ret == true) {
                        $linha = $modalidadeDAL->getResp();
                        $nome = $linha[0]->getNome();
                    }
                }
            }
        }

        return $nome;
    }

}

?>