<?php

/* Charset */
$lang["charset"]   = "iso-8859-1";

/* General */
$lang["Logout"]    = "Sair";
$lang["FormUndo"]  = "Restaurar Mudan�as";
$lang["FromClear"] = "RESET";
$lang["FormEnter"] = "Por favor, digite seu <strong>Usu�rio</strong> e sua <strong>Senha</strong>";
$lang["FormWrong"] = "Usu�rio ou Senha inv�lido";
$lang["FormOK"]    = "OK";
$lang["Updated"]   = "As mudan�as foram efetuadas com sucesso!!";
$lang["NoUpdate"]  = "Ocorreu um erro! N�o foram efetuadas altera��es!";
$lang["Confirm"]   = "Tens certeza?";
$lang["NavNext"]   = "Pr�xima &raquo;";
$lang["NavPrev"]   = "&laquo; Anterior";
$lang["License"]   = "Termos de Utiliza��o";
$lang["ScrollTxt"] = "Puxa o texto para baixo para ler o resto dos termos.";

/* Templates */
$lang["Templates"]  = "Templates";
$lang["tpl_exist"]  = "J� existe o nome que escolheste.";
$lang["tpl_new"]    = "Adiciona uma nova template.";
$lang["tpl_succes"] = "Foi adicionado com sucesso!"; 
$lang["tpl_bad"]    = "O nome da template � inv�lido!";
$lang["tpl_save"]   = "Salvar";
$lang["preview"]    = "Ver Exemplo";
$lang["newtpl"]     = "Nova Template";

/* Poll List */
$lang["IndexTitle"]  = "Lista de Enquetes";
$lang["IndexQuest"]  = "Pergunta";
$lang["IndexID"]     = "ID";
$lang["IndexDate"]   = "Data";
$lang["IndexDays"]   = "Dias";
$lang["IndexExp"]    = "Expira��o";
$lang["IndexExpire"] = "expirada";
$lang["IndexNever"]  = "nunca";
$lang["IndexStat"]   = "Estat�sticas";
$lang["IndexCom"]    = "Coment�rios";
$lang["IndexAct"]    = "A��o";
$lang["IndexDel"]    = "Apagar";

/* Create A New Poll */
$lang["NewTitle"]  = "Criar Nova Enquete";
$lang["NewOption"] = "Op��o";
$lang["NewNoQue"]  = "Preencha a pergunta";
$lang["NewNoOpt"]  = "Preencha as op��es";

/* Poll Edit */
$lang["EditStat"]  = "Estado";
$lang["EditText"]  = "Editar esta Enquete";
$lang["EditReset"] = "Voltar a 0";
$lang["EditOn"]    = "Ativa";
$lang["EditOff"]   = "Desativada";
$lang["EditHide"]  = "Invisivel";
$lang["EditLgOff"] = "logging off";
$lang["EditLgOn"]  = "logging on";
$lang["EditAdd"]   = "Adicionar Op��es";
$lang["EditNo"]    = "N�o foram adicionadas novas op��es!";
$lang["EditOk"]    = "Op��es adicionadas!";
$lang["EditSave"]  = "Salvar Altera��es";
$lang["EditOp"]    = "S�o OBRIGAT�RIAS pelo menos 2 op��es!";
$lang["EditMis"]   = "A Pergunta e as Op��es n�o foram definidas!";
$lang["EditDel"]   = "Para remover uma op��o deixa o campo em branco";
$lang["EditCom"]   = "Permite Coment�rios?";

/* General Settings */
$lang["SetTitle"]   = "Configura��es Gerais";
$lang["SetOption"]  = "Tabelas, Fontes e Cores";
$lang["SetMisc"]    = "V�rios";
$lang["SetText"]    = "Configura��es Gerais";
$lang["SetURL"]     = "URL para o diret�rio das imagens";
$lang["SetBURL"]    = "URL para o diret�rio da enquetes";
$lang["SetNo"]      = "No trailing slash";
$lang["SetLang"]    = "Idioma";
$lang["SetPoll"]    = "T�tulo da Enquete";
$lang["SetButton"]  = "Bot�o de Voto";
$lang["SetResult"]  = "Link dos Resultados";
$lang["SetVoted"]   = "Para quem j� votou...";
$lang["SetComment"] = "Coment�rios";
$lang["SetTab"]     = "Largura da Tabela";
$lang["SetBarh"]    = "Altura da Barra";
$lang["SetBarMax"]  = "Max. bar length";
$lang["SetTabBg"]   = "Cor Fundo da Tabela";
$lang["SetFrmCol"]  = "Cor do T�tulo";
$lang["SetFontCol"] = "Font color";
$lang["SetFace"]    = "Font face";
$lang["SetShow"]    = "Display result as";
$lang["SetPerc"]    = "percentagem";
$lang["SetVotes"]   = "n� de votos";
$lang["SetCheck"]   = "Verificar";
$lang["SetNoCheck"] = "no checking";
$lang["SetIP"]      = "IP table";
$lang["SetTime"]    = "tempo entre votos";
$lang["SetHours"]   = "horas";
$lang["SetOffset"]  = "Server time offset";
$lang["SetEntry"]   = "Coment�rio por p�gina";
$lang["SetSubmit"]  = "Submeter Altera��ess";
$lang["SetEmpty"]   = "Valor Inv�lido";
$lang["SetSort"]    = "Ordem";
$lang["SetAsc"]     = "Ascendente";
$lang["SetDesc"]    = "Descendente";
$lang["Setusort"]   = "nao ordenar";
$lang["SetOptions"] = "N�mero de Op��es permitidas por Enquete";
$lang["SetPolls"]   = "Enquetes por p�gina";

/* Change Password */
$lang["PwdTitle"] = "Mudar Senha";
$lang["PwdText"]  = "Mudar o Usu�rio ou a Senha";
$lang["PwdUser"]  = "Usu�rio";
$lang["PwdPass"]  = "Senha";
$lang["PwdConf"]  = "Confirmar Senha";
$lang["PwdNoUsr"] = "Preencha o campo do Usu�rio";
$lang["PwdNoPwd"] = "Preencha o campo da Senha";
$lang["PwdBad"]   = "Reconfirme sua Senha";

/* Poll Stats */
$lang["StatCrea"]  = "Criada a";
$lang["StatAct"]   = "Ativa";
$lang["StatReset"] = "Recome�ar o Log das Estat�sticas";
$lang["StatDis"]   = "logging is disabled for this poll";
$lang["StatTotal"] = "Total de Votos";
$lang["StatDay"]   = "votos por dia";

/* Poll Comments */
$lang["ComTotal"]  = "Total de Coment�rios";
$lang["ComName"]   = "Nome";
$lang["ComPost"]   = "colocada";
$lang["ComDel"]    = "Tem certeza que deseja apagar este coment�rio?";

/* Help */
$lang["Help"]       = "Ajuda";
$lang["HelpPoll"]   = "To emded a poll into a webpage insert the code snippet from below";
$lang["HelpRand"]   = "Tamb�m � poss�vel colocar uma sondagem aleat�ria";
$lang["HelpNew"]    = "Mostra sempre a �ltima sondagem";
$lang["HelpSyntax"] = "Syntax";

/* Days */
$weekday[0] = "Domingo";
$weekday[1] = "Segunda";
$weekday[2] = "Ter�a";
$weekday[3] = "Quarta";
$weekday[4] = "Quinta";
$weekday[5] = "Sexta";
$weekday[6] = "S�bado";

/* Months */
$months[0]  = "Janeiro";
$months[1]  = "Fevereiro";
$months[2]  = "Mar�o";
$months[3]  = "Abril";
$months[4]  = "Maio";
$months[5]  = "Junho";
$months[6]  = "Julho";
$months[7]  = "Agosto";
$months[8]  = "Setembro";
$months[9]  = "Outubro";
$months[10] = "Novembro";
$months[11] = "Dezembro";

/* translating this array does not change the reference */
$color_array[0]  = "azul claro";
$color_array[1]  = "azul escuro";
$color_array[2]  = "castanho";
$color_array[3]  = "verde escuro";
$color_array[4]  = "ouro";
$color_array[5]  = "verde claro";
$color_array[6]  = "cinzento";
$color_array[7]  = "laranja";
$color_array[8]  = "rosa";
$color_array[9]  = "violeta";
$color_array[10] = "vermelho";
$color_array[11] = "amarelo";

?>
<!--
/*
##################################
#     Guia Mais vers�o 8.11      #
# ADAPTADO POR:                  #
# PLUNDER - Mercado Livre        #
# email: wmwebsites@yahoo.com.br #
# �ltimo Upgrade   07/07/2007    #
#      - Favor N�o Remover -     #
##################################
*/
-->
