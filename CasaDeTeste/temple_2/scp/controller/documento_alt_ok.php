<?
include ('../include/header.php');
//include('validar_nome.php');
$cab = "Documentos inseridos";


date_default_timezone_set("America/Sao_Paulo");
$data = date("Y-m-d");
$time = date("Ymd");

if (!is_numeric($_POST["cod"])) {
    ?>
    <script language="javascript">
        alert('Ooops!');
        history.back(-1);
    </script>
    <?
}
$cod = $_POST["cod"];
?>
<div id="page-wrapper">
    <div class="container-fluid">

        <?= montaCab("Documentos!", "<i class='fa fa-upload'></i>$cab", " ") ?>

        <a href="../view/Documento" class="btn btn-default">In�cio</a> <br><br>
    </div>

<?
$diretorio = "../files/";

if (!is_dir($diretorio)) {
    echo "Pasta $diretorio n�o existe";
} else {

    $arquivo = isset($_FILES['arquivo']) ? $_FILES['arquivo'] : FALSE;

    $somaString = "";
    $total = 0;

    $arquivo1 = $arquivo['name'];

    if (empty($arquivo1)) {

        //tratar nome para banco
        $nome = $_POST["txtcurso"];
        $status = $_POST["txtStatus"];
        $id_usuario = $_SESSION["login"]->getId();

        $documentoDAL = new DocumentoDAL();
        $retorno = $documentoDAL->buscar("where id=$cod");

        if ($retorno) {
            $resp = $documentoDAL->getResp();
            $documento = $resp[0];
            $documento->setNome($nome);
            $documento->setId_usuario($id_usuario);
            $documento->setStatus($status);

            $documento->setData($data);

            $retorno = $documentoDAL->atualizar($documento);

            if ($retorno) {
                echo "documentos adicionados!<br>";

                $msg = "<script>
                         alert('Cadastrado com sucesso!');
                         history.back(-1);
                         </script>";
            } else {
                "<script>
            alert('N�o foi possivel carregar o documento');
            </script>";
            }
        }
    } else {
        $arquivo_tratado_inc = $arquivo1;
       // $id_curso = $_POST['cod']; //adicionar codigo do curso ao nome do arquivo

        if ($arquivo['type'] == "application/pdf") {
            echo "Arquivo okay!<br />";
        } else {
            echo "<br />O arquivo<b style='color: red;'> " . $arquivo1 . " </b>� inv�lido.<br />
                       Selecione um aquivo PDF!";
            echo "</div>";
            include('../include/footer.php');
            echo "</body></html>";
            exit;
        }

            $nomeDoc = explode('.', $arquivo_tratado_inc);
        $nomeFile = $nomeDoc[0];
        $complemento = $nomeFile . ".pdf";

        $arquivo_tratado = $complemento;

        //verificar se arquivo com mesmo nome existe
        $dir = str_replace("\\", "/", dirname(__FILE__));
        if (file_exists($dir . "../files/" . $arquivo_tratado)) {
            echo "<script>
                        alert('Arquivo duplicado! Detectado: $arquivo_tratado j� existe nos arquivos.');
                        history.go(-1);
                 </script>";
            exit;
        } else {
            $destino = $diretorio . "/" . $arquivo_tratado;
        }

        //tratar nome para banco
        $nome = $nomeFile;
        //$nome_validado = validar_nome($nome);
      $id_usuario = $_SESSION["login"]->getId();
        $link = $arquivo_tratado;
        $status = $_POST["txtStatus"];

        $documentoDAL = new DocumentoDAL();
        $retorno = $documentoDAL->buscar("where id=$cod");

        if ($retorno) {
            $resp = $documentoDAL->getResp();
            $documento = $resp[0];

            $linkArquivo = $resp[0]->getLink();
            $link_deletar = "../files/" . $linkArquivo;
            unlink($link_deletar);

            if (move_uploaded_file($arquivo['tmp_name'], $destino)) {

                $documento->setNome($nome);
                $documento->setId_usuario($id_usuario);
                $documento->setLink($link);
                $documento->setStatus($status);
                $documento->setData($data);

                $retorno = $documentoDAL->atualizar($documento);

                if ($retorno) {
                 //   echo "Documento editado com sucesso!<br>";
                } else {
                    "<script>
                            alert('N�o foi possivel carregar o documento');
                         </script>";
                }
            } else {
                echo "<script>
                             alert('Erro ao Mover o arquivo!');
                          </script>";
            }
        }
    }
} // fecha else
?>

</div>
<?php include ('../include/footer.php'); ?>
