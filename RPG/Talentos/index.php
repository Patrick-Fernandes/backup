<?php
include 'cabeca.php';
include './scripts.php';
?>
<?php
header('Content-type: text/html; charset=UTF-8');
?>

<!--<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />-->
<?php // header("Content-Type: text/html; charset=ISO-8859-1");  ?>
<section class="main-section paddind fadeInUp delay-04s" id="moodle">
    <!--main-section-start-->
    <div class="container">
        <div class="row">
            <div class="gallery">
                <h2  class="gallery-title">Talentos</h2>
            </div>
            <!--<h6>Fresh portfolio of designs that will keep you wanting more.</h6>-->
            <div class="portfolioFilter">
                <ul class="Portfolio-nav wow fadeIn delay-02s">
                    <li><a href="#" class="btn btn-default filter-button current" data-filter="COMBATE">COMBATE</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="PERICIA">PERICIA</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="MAGIA">MAGIA</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="DESTINO">DESTINO</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="CONCEDIDO" class="">CONCEDIDO</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="TORMENTA" class="">TORMENTA</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="MM" class="">META MAGICO</a></li>
                    <li><a href="#" class="btn btn-default filter-button" data-filter="all" class="">Todos</a></li>


                </ul>
                <ul class="Portfolio-nav wow fadeIn delay-02s">
                    <?PHP
                    //include 'combate.php';

                    $sql = "SELECT DISTINCT(`letra`) as l FROM `talentosrpg`";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <li><a href="#" class="btn btn-default filter-button current" data-filter="<?= strtoupper($row["l"]) ?> "><?= strtoupper($row["l"]) ?> </a></li>
                            <?php
                        }
                    } else {
                        echo "0 results";
                    }
                    ?>
                </ul>
            </div>
            <!--<div class="portfolioContainer wow fadeInUp delay-04s">-->
            <?PHP
            //include 'combate.php';

            $sql2 = "SELECT `ID`, `titulo`, `tipo`, `descricao`, `preRequisito`, `beneficio`, `especial`, "
                    . "`metaMagico`, `letra` FROM `talentosrpg` ORDER BY titulo,letra ";
            $result2 = $conn->query($sql2);
            if ($result2->num_rows > 0) {
                // output data of each row
                while ($row2 = $result2->fetch_assoc()) {
                    ?>
                    <div style="margin:3px 0;padding: 1px; border: #<?= contaCaracter($row2["ID"]) ?> 2px solid; " class="col-sm-12  col-lg-12 col-xs-12">
                        <div  style="color: #000;" class="filter <?= strtoupper($row2["tipo"]) ?> <?php
                              if (limpa_neve($row2["metaMagico"]) == 1) {
                                  echo 'MM';
                              }
                              ?> <?= strtoupper($row2["letra"]) ?> ">
                            <h2><?= limpa_neve($row2["titulo"]) ?> </h3>
                            <P>
                                <?php
                                if ($row2["descricao"] != NULL) {
                                    echo limpa_neve($row2["descricao"]) . '<br>';
                                }
                                if ($row2["preRequisito"] != NULL) {
                                    echo '<b> Pré-requisito:' . limpa_neve($row2["preRequisito"]) . '</b> <br>';
                                }
                                echo '<B>Benéficio: </B> ' . ($row2["beneficio"]) . '<br>';
                                if (limpa_neve($row2["especial"]) != NULL) {
                                    echo '<b></b>' . limpa_neve($row2["especial"])
                                    ;
                                }
                                ?>
                            </P>
                            <br>
                        </div>              
                    </div>

                    <?php
                }
            } else {
                echo "0 results";
            }
            ?>













        </div>
    </div>
</section>
<!--main-section-end-->

<!------ Include the above in your HEAD tag ---------->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
    $(document).ready(function () {
//        $(".filter").not('.tics').hide('3000');
//        $('.filter').filter('.tics').show('3000');
        $(".filter-button").click(function () {
            var value = $(this).attr('data-filter');
            if (value == "all")
            {
                //$('.filter').removeClass('hidden');
                $('.filter').show('1000');
            } else
            {
                //            $('.filter[filter-item = "'+value+'"]').removeClass('hidden');
                //            $(".filter").not('.filter[filter-item = "'+value+'"]').addClass('hidden');
                $(".filter").not('.' + value).hide('3000');
                $('.filter').filter('.' + value).show('3000');
            }
        });
        if ($(".filter-button").removeClass("active")) {
            $(this).removeClass("active");
        }
        $(this).addClass("active");

    });
</script>

<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href = 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel = 'stylesheet' type = 'text/css'>
<script type = "text/javascript" src = "Estilos/js/jquery.1.8.3.min.js"></script>
<script type="text/javascript" src="Estilos/js/bootstrap.js"></script>
<script type="text/javascript" src="Estilos/js/jquery-scrolltofixed.js"></script>
<script type="text/javascript" src="Estilos/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="Estilos/js/jquery.isotope.js"></script>
<script type="text/javascript" src="Estilos/js/wow.js"></script>
<script type="text/javascript" src="Estilos/js/classie.js"></script>
<script src="Estilos/contactform/Estilos/contactform.js"></script>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#test').scrollToFixed();
        $('.res-nav_click').click(function () {
            $('.main-nav').slideToggle();
            return false

        });

    });
</script>

<script>
    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>


<script type="text/javascript">
    $(window).load(function () {

        $('.main-nav li a, .servicelink').bind('click', function (event) {
            var $anchor = $(this);

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 102
            }, 1500, 'easeInOutExpo');
            /*
             if you don't want to use the easing effects:
             $('html, body').stop().animate({
             scrollTop: $($anchor.attr('href')).offset().top
             }, 1000);
             */
            if ($(window).width() < 768) {
                $('.main-nav').hide();
            }
            event.preventDefault();
        });
    })
</script>

<script type="text/javascript">
    $(window).load(function () {


        var $container = $('.portfolioContainer'),
                $body = $('body'),
                colW = 375,
                columns = null;


        $container.isotope({
            // disable window resizing
            resizable: true,
            masonry: {
                columnWidth: colW
            }
        });

        $(window).smartresize(function () {
            // check if columns has changed
            var currentColumns = Math.floor(($body.width() - 30) / colW);
            if (currentColumns !== columns) {
                // set new column count
                columns = currentColumns;
                // apply width to container manually, then trigger relayout
                $container.width(columns * colW)
                        .isotope('reLayout');
            }

        }).smartresize(); // trigger resize to set container width
        $('.portfolioFilter a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
            });
            return false;
        });

    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-92953930-1', 'auto');
    ga('send', 'pageview');

</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>


</body>

</html>