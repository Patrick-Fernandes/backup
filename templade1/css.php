        <style id='page-skin-1' type='text/css'><!--
            /*
            -----------------------------------------------
            Blogger Template Style
            ----------------------------------------------- */
            body#layout .container {
                width: 800px;
            }
            body#layout {
                clear: both;
                overflow: hidden;
            }
            body#layout .page {
                width: 800px;
                margin: 0;
                padding: 0;
            }
            body#layout .containerwrap {
                width: 800px;
                margin: 0;
                padding: 0;
            }
            body#layout header .container {
                width: 800px;
                margin: 0;
                padding: 0;
            }
            body#layout .logowrap {
                width: 100%;
                margin: 0;
                float: none;
            }
            body#layout #logo {
                height: auto;
            }
            body#layout .headerright, body#layout .headerad-wrap {
                display: none;
            }
            body#layout .main-container {
                clear: both;
            }
            body#layout .main-wrapper {
                width: 67.5%;
            }
            body#layout #main {
                float: none;
                width: auto;
            }
            body#layout .sidebar-wrapper {
                float: left;
                width: 30%;
            }
            body#layout .footer-widgets {
                padding: 0;
                width: auto;
            }
            body#layout .footer {
                display: inline-block;
                width: 19.8%;
                float: left;
            }
            html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;
            }
            html {
                overflow-x: hidden;
                -webkit-font-smoothing: antialiased;
            }
            article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block }
            ol, ul { list-style: none }
            blockquote, q { quotes: none }
            table {
                border-collapse: collapse;
                border-spacing: 0;
            }
            button, input, select, textarea { margin: 0 }
            :focus { outline: 0 }
            img, video, object {
                max-width: 100%;
                height: auto!important;
            }
            iframe { max-width: 100% }
            strong, b { font-weight: bold }
            em, i, cite { font-style: italic }
            small { font-size: 100% }
            figure { margin: 10px 0 }
            code, pre {
                font-weight: normal;
                font-style: normal;
            }
            pre {
                margin: 5px 0 20px 0;
                line-height: 1.3em;
                padding: 8px 10px;
                overflow: auto;
            }
            code {
                padding: 0 8px;
                line-height: 1.5;
            }
            h1, h2, h3, h4, h5, h6 {
                font-weight: bold;
                margin: 12px 0;
                color: #444;
            }
            h1 {
                font-size: 28px;
                line-height: 1.4;
            }
            h2 {
                line-height: 1.4;
                    font-size:30px;
            }
            h3 {
                font-size: 22px;
                line-height: 1.4;
            }
            h4 {
                font-size: 20px;
                line-height: 1.4;
            }
            h5 {
                font-size: 18px;
                line-height: 1.4;
            }
            h6 {
                font-size: 16px;
                line-height: 1.4;
            }
            h1,h2,h3,h4,h5,h6 {
                font-family:'Varela Round';font-weight:normal;color:#444
            }
            body {
                font-family: 'Open Sans';
                font-weight: normal;
                font-size: 15px;
                color: #868686;
                background-image: url(http://1.bp.blogspot.com/-vxiYOQHymFg/UzqWTux5exI/AAAAAAAABjM/LhR21L1FhP4/s1600/nobg.png);
                font: 14px/23px Open Sans;
                background-color: #fff;
            }
            ul {
                -webkit-padding-start: 0px;
            }
            p { margin-bottom: 20px }
            a {
                text-decoration: none;
                color: #EE210B;
                -webkit-transition: all 0.2s linear;
                -moz-transition: all 0.2s linear;
                transition: all 0.2s linear 0.2s linear;
            }
            a:hover { color: #EE210B }
            a:link, a:visited, a:active { text-decoration: none }
            .rainbow {
                height: 6px;
                background: url(http://4.bp.blogspot.com/-4Zg97W_NoIU/UzvPtuKxQvI/AAAAAAAABjc/FhBgHxDtZMI/s1600/rainbow.png) repeat-x;
            }
            header {
                background-color: #FFF;  background-image: url(http://1.bp.blogspot.com/-_lVnct4WnYM/Uz70I6shJJI/AAAAAAAABkM/0zxUNLoIfEs/s1600/1.jpg); background-size:cover;  border-bottom: 1px solid rgba(0, 0, 0, 0.12);  position: relative;  float: left;  width: 100%;  z-index: 99;
            }
            .container {
                position: relative;  margin-right: auto;  margin-left: auto;
                max-width: 1250px;  min-width: 240px;
                overflow: hidden;
            }
            header .container {
                overflow: inherit;
            }
            #header {
                position: relative;  width: 100%;  float: left;  margin: 0;  padding: 0;
            }
            .logowrap {
                margin: 0 0 0 2%;
                overflow: hidden;
                float: left;
                margin-bottom: -1px;
            }
            #logo {
                margin: 0;
                background-color: #5bc4be;
                overflow: hidden;
                height: 88px;
            }
            #logo h1 {
                font-size: 44px;    display: inline-block; position: relative;  margin: 0;
                color: #fff;
                float: left;
                line-height: 0.6;
                padding: 21px 25px;
            }
            #logo img {
                vertical-align: middle;
                padding: 21px 25px;
            }
            #logo h1 a {
                color: #fff;
            }
            .widget {
                line-height: 0;
                margin: 0;
            }
            .headerright {
                float: right;
                display: block;  float: right;  max-width: 100%;  margin-right: 2%;
            }
            .headerad-wrap { width: 100%; text-align: center; margin-top: 25px; float: left; }
            .mobilemenu {
                display: none;
            }
            #navigation {
                margin: 26px auto 15px;  font-size: 13px;  float: left;  width: 100%;
            }
            #navigation .menu {
                float: left;
            }
            #navigation ul {
                margin: 0 auto;
            }
            a#pull {
                display: none;
            }
            a#pull{
                background-color: #446cb3;
                color: #fff!important;
            }
            #navigation ul li {
                float: left;
                position: relative;
                line-height: 1.6;
                margin-top: 6px;
            }
            #navigation ul li:first-child a {
                margin-left: 0;
            }
            #navigation ul li a, #navigation ul li a:link, #navigation ul li a:visited {
                overflow: hidden;
                display: block;
                text-transform: uppercase;
            }
            .menu li a {
                font-family: 'Varela Round';
                font-weight: normal;
                font-size: 14px;
                color: #fff;
                text-align: left;
                text-decoration: none;
                margin: 0 20px 0;
                box-sizing: border-box;
            }
            .menu li a:hover {
                border-bottom: 1px solid #446cb3;
            }
            #navigation ul > li > ul {
                position: absolute;
                background: #fff;
                border: 1px solid;
                border-bottom: none;
                border-color: #e5e5e5;
                -moz-box-shadow: 0 1px 10px rgba(0,0,0,0.15);
                -webkit-box-shadow: 0 1px 10px rgba(0,0,0,0.15);
                box-shadow: 0 1px 10px rgba(0,0,0,0.15);
                top: 100%;
                left: 0;
                min-width: 180px;
                padding: 0;
                z-index: 99;
                margin-top: 0;
                visibility: hidden;
                opacity: 0;
                -webkit-transform: translateY(10px);
                -moz-transform: translateY(10px);
                transform: translateY(10px);
            }
            #navigation ul > li > ul > li {
                display: block;
                float: none;
                text-align: left;
                position: relative;
                border-bottom: 1px solid;
                border-top: none;
                border-color: #e5e5e5;
                vertical-align: middle;
                padding: 0;
                margin: 0;
                transition: background .3s;
                list-style: none;
            }
            #navigation ul > li > ul > li a {
                font-size: 11px;
                display: block;
                color: #827E79;
                line-height: 35px;
                text-transform: uppercase;
                text-decoration: none;
                margin: 0;
                padding: 0 12px;
                border-right: 0;
                border: 0;
                font-weight: 400;
                letter-spacing: 2px;
                transition: color .3s;
            }
            #navigation ul > li:hover > ul {
                opacity: 1;
                visibility: visible;
                -webkit-transform: translateY(0);
                -moz-transform: translateY(0);
                transform: translateY(0);
            }
            .search_li {
                float: right;
                margin-left: 20px;
            }
            #header #searchform {
                max-width: 100%;
                float: right;
                width: auto;
                min-width: 206px;
            }
            #searchform {
                padding: 0;
                float: left;
                clear: both;
                width: 100%;
                position: relative;
            }
            nav #searchform fieldset {
                border: 0 none;
                clear: both;
                display: block;
                float: left;
                min-width: 240px;
                width: 100%;
            }
            #header #s {
                margin: 0 !important;
                padding: 7px 15px 7px 9px;
                width: 75%;
                height: 32px;
            }
            #s {
                width: 73%;
                background-color: #fff;
                float: left;
                padding: 9px 15px 9px 9px;
                border: 1px solid rgba(0, 0, 0, 0.08);
                border-right: none;
                height: 36px;
            }
            input, textarea, select {
                padding: 8px 12px;
                font-size: 12px;
                color: #A5A5A5;
                border: 1px solid rgba(0, 0, 0, 0.12);
                font-family: inherit;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            #header .sbutton {
                line-height: 32px;
                height: 32px;
                background-color: #446cb3;
                color: #fff!important;
                padding: 0;
                float: right;
                width: 29.2%;
                cursor: pointer;
                text-align: center;
                -webkit-transition: all 0.25s linear;
                -moz-transition: all 0.25s linear;
                transition: all 0.25s linear;
                z-index: 10;
                border: 0;
                vertical-align: top;
                text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
                position: absolute;
                top: 0;
                right: 0;
            }
            .containerwrap {
                width: 1250px;  margin: 0 auto;  background: #fff;  clear: both;  display: block;  padding: 0;
                max-width: 100%;
            }
            .page {
                padding-top: 25px;
                min-height: 100px;
                float: none;
                clear: both;
                padding: 25px 2% 0 2%;
                display: inline-block;
                width: 100%;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .page, .container {
                max-width: 1250px;
                min-width: 240px;
            }
            .page, .container {
                position: relative;
                margin-right: auto;
                margin-left: auto;
            }
            .main-wrapper {
                width: 70.8%;  float: left;  margin-bottom: 0;
            }
            #main {
                margin: 0;
                float: left;  width: 100%;
            }
            .post.hentry {
                width: 100%;  float: left;  margin-bottom: 15px;
            }
            .hover_img {
                margin-bottom: 11px;
                float: left;  margin-right: 2.4%;  width: 35.1%;  margin-top: 7px;  position: relative;
            }
            h2.post-title {
                font-family: 'Varela Round';  font-weight: normal;  font-size: 30px;  color: #444;
                display: block;  line-height: 1.4;
                margin-bottom: 5px;
            }
            .post-title a {
                color: #444;
            }
            .post-title a:hover {
                color: #446cb3;
            }
            .postbody {
                font-family: 'Open Sans';  font-size: 15px;
                line-height: 23px;
            }
            .more {
                color: #446cb3;
                font-family: 'Open Sans';
                font-size: 15px;
            }
            .post-body {
                font-family: 'Open Sans';  font-size: 15px;
                line-height: 23px;
            }
            .post-info {
                display: block;
                color: #999;
                font-family: 'Open Sans';
                font-size: 14px;
                overflow: hidden;
                border-bottom: 1px solid rgba(0, 0, 0, 0.12);
                border-top: 1px solid rgba(0, 0, 0, 0.12);
                padding: 4px 0;
            }
            .post-info span {
                margin-right: 7px;
            }
            .post-info a {
                color: #999;
                margin-left:5px;
            }
            .post-info a:hover {
                color: #446cb3;
            }
            .thecategory {
                margin-left: 5px;
            }
            .metacom {
                border-top: 1px solid rgba(0, 0, 0, 0.12);
                margin-top: 15px;
                width: 100%;
            }
            .post-header {
                margin-bottom: 11px;
            }
            .post-comment-link,.post-icons,.post-labels,.date-header {
                display: none;
            }
            .breadcrumbs {
                margin-bottom: 20px;
                font-size: 15px;
                line-height: 23px;
            }
            .breadcrumbs a {
                color: #446cb3;
            }
            .postauthor {
                margin: 20px 0 0px 0;
                padding: 0;
                float: left;
                clear: both;
                width: 100%;
            }
            .postauthor h4{
                margin-bottom: 12px;
            }
            .postauthor h4 {
                font-weight: bold;
                font-size: 18px;
            }
            .postauthor img {
                float: left;
                margin: 4px 20px 0 0;
            }
            .postauthor h5 {
                color: #446cb3;
                font-weight: bold;
                font-size: 16px;
                padding: 0;
                margin: 0;
                line-height: 24px;
                text-transform: capitalize;
            }
            .postauthor h5 a{
                color: #446cb3;
            }
            .postauthor p {
                padding-top: 0px;
                margin-top: 8px;
                margin-bottom: 0;
            }
            .shareit {
                position: relative!important;
                width: 100%!important;
                top: 0!important;
                padding-left: 0!important;
                padding-right: 0!important;
                margin: 0!important;
                padding-top: 10px!important;
                border: none !important;
                border-left: 0;
                overflow: hidden;
                padding: 5px;
                border-right: 0;
                left: auto;
                z-index: 0;
                clear:both;
            }
            .share-item {
                display: block;
                margin: 2px;
                float: left;
                height: 25px;
            }
            .share-item.gplusbtn {
                width: 85px;
            }
            .share-item.facebookbtn {
                margin-top: -1px;
                width: 100px;
                height: 28px;
            }
            .share-item.facebookbtn, .share-item.linkedinbtn, .share-item.stumblebtn {
                width: 92px;
            }
            .share-item.pinbtn {
                margin-top: 2px;
            }
            .rthumb {
                margin-bottom: 8px;
                width: 100%;
                overflow: hidden;
            }
            .rthumb img {
                margin-top: 5px;
                margin-bottom: 8px;
                float: left;
            }
            #related-posts {
                float: left;  width: 100%;  margin-top: 27px;
            }
            #related-posts h3 {
                margin-bottom: 10px;
                font-size: 18px;  margin: 0;
                font-weight: 700;
            }
            .relatedcontainer{
                margin-left: 0;
                padding: 0;
                margin: 0;
                overflow: hidden;
                margin-top: 0;
            }
            .relatedpost {
                margin: 0 2% 0 0;
                width: 23%;
                float: left;
                line-height: 1.4;
            }
            .relatedpost a {
                color: #868686;
            }
            .relatedpost a:hover {
                color: #446cb3;
            }
            .relatedtitle {
                float: left;  width: 100%;
                line-height: 21px;
                font-size: 15px;
            }
            #comments {
                padding: 0;
                margin-top: 27px;
                float: left;
                clear: both;
                width: 100%;
                font-family: 'Open Sans';
                line-height: 23px;
                font-size: 15px;
            }
            #comments h4 {
                color: #444;  font-size: 18px;  font-weight: bold;
                margin-bottom: 12px;
            }
            #comment-holder {
                margin-left: 0;  margin-top: 13px;  overflow: hidden;
                margin: 1.5em 0 1.571em 1.9em;
            }
            #comment-holder li {
                margin: 0 0 30px 0;  padding-left: 0;  float: left;  width: 100%;  position: relative;
            }
            .comments .avatar-image-container {
                float: left;  margin-right: 17px;  max-width: 45px;
                max-height: 45px;
                width: 45px;
                height: 45px;
            }
            .comments .avatar-image-container img {
                max-width: 45px;
                border: 1px solid rgba(0, 0, 0, 0.12);  float: left; padding: 4px;
            }
            .comments .comment-block {
                border: 1px solid rgba(0, 0, 0, 0.12);  overflow: hidden;  padding: 14px;
            }
            .comments .comments-content .user {
                line-height: 1.375em;  font-weight: bold;  text-transform: uppercase;
            }
            .comments .comments-content .user a {
                color: #868686;
            }
            .comments .comments-content .datetime {
            }
            .comments .comment .comment-actions a {
                margin-top: 10px;  padding-bottom: 0px;  overflow: hidden;  width: auto;  float: right;
                background-color: #446cb3;  color: #fff!important;
                margin-left: 10px;  padding: 6px 12px;  font-size: 12px;  line-height: 1;  border-radius: 3px;  -webkit-transition: all 0.25s linear;  -moz-transition: all 0.25s linear;  transition: all 0.25s linear;
            }
            .comments .comment .comment-actions a:hover {
                background: #444;
            }
            .sidebar-wrapper {
                float: right;  width: 25%;  line-height: 20px;  max-width: 300px;  padding-left: 2%;
                padding-right: 0;
            }
            .sidebar .widget {
                margin-bottom: 27px;
                float: left;  clear: both;  width: 100%;
            }
            #sidebar {
                margin: 0px!important;
            }
            .sidebar h2 {
                font-size: 18px;  font-weight: 400;  line-height: 18px;  margin: 0px 0 15px 0;
                font-family: 'Varela Round';
                color: #444;
            }
            .sidebar a {
                color: #696868;
            }
            .sidebar a:hover {
                color: #446cb3;
            }
            .sidebar .widget-content {
                font-family: 'Open Sans';  font-size: 15px;
            }
            footer {
                background: #f5f5f5;  overflow: hidden;  width: 100%;
            }
            footer .container {
                padding: 0;
            }
            .footer-widgets {
                overflow: hidden;
                padding: 30px 2% 10px;
                width: 96%;
                margin: 0;
            }
            .footer-widgets .footer {
                width: 23.5%;
                margin-right: 2%;
                float: left;
                position: relative;
                margin-bottom: 0;
                margin-top: 10px;
            }
            .footer-widgets #footer4{
                margin-right:0px!important;
            }
            .section {
                margin: 0;
            }
            .footer .widget {
                float: left;  clear: both;  width: 100%;
                margin-bottom: 20px;
                padding: 1px 0 0 0;
            }
            .footer h2 {
                padding-bottom: 10px;  margin-bottom: 10px;  padding-top: 0;
                margin: 0;
                font-size: 18px;  font-weight: 400;  line-height: 18px;
            }
            .footer-widgets a {
                color: #555;
            }
            .footer-widgets a:hover {
                color: #446cb3;
            }
            .FollowByEmail .follow-by-email-inner {
                overflow: hidden;  position: relative;
            }
            .FollowByEmail .follow-by-email-inner input[type="text"] {
                -webkit-transition: all 0.25s linear;
                -moz-transition: all 0.25s linear;
                transition: all 0.25s linear;
                width: 73%;
                float: left;
                padding: 9px 0;
                text-indent: 10px;
                border-right: 0;
                font-size: 12px;  color: #A5A5A5;  border: 1px solid rgba(0, 0, 0, 0.12);  font-family: inherit;
                width: 206px;
                height: 37px;
            }
            .FollowByEmail .follow-by-email-inner .follow-by-email-submit {
                background-color: #446cb3;  color: #fff!important;
            }
            .FollowByEmail .follow-by-email-inner .follow-by-email-submit:hover {
                background-color: #5bc4be;
            }
            .FollowByEmail .follow-by-email-inner input[type="submit"] {
                cursor: pointer;
                color: white!important;
                padding: 0;
                border: 0;
                float: left;
                height: 36px;
                line-height: 36px;
                text-align: center;
                width: 29.2%;
                -webkit-transition: all 0.25s linear;
                -moz-transition: all 0.25s linear;
                transition: all 0.25s linear;
                text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
                position: absolute;
                right: 0;
                top: 0;
            }
            .copyrights {
                border-top: 1px solid rgba(0, 0, 0, 0.06);  float: left;  width: 100%;  clear: both;  font-size: 12px;  padding-bottom: 10px;  padding-top: 12px;  color: #BBB;
            }
            #copyright-note {
                width: 1200px;
                margin: 0 auto;
                clear: both;
                max-width: 96%;
            }
            .copyrights a {
                color: #535353;
            }
            .copyrights a:hover {
                color: #F33E0F;
            }
            .top {
                float: right;
                position: relative;
            }
            #copyright-note span {
                float: left;
            }
            .mobilemenu{
            display: none;
            visibility: hidden;
            }
            .mobilemenu #navigation a#pull {
                display: none!important;
            }
            .mobilemenu #navigation .search_li {
                display: none!important;
            }
            .fb_iframe_widget, .fb_iframe_widget span {
                max-width: 100%;
                width: 100%!important;
            }
            iframe {
                max-width: 100%;
            }
            .Label li {
                display: inline-block;
                margin-bottom: 10px!important;
                margin-right: 7px!important;
                padding: 0px!important;
            }
            .Label span {
                color: #fff;  -webkit-box-shadow: 0px 0px 4px rgba(50, 50, 50, 0.43);  -moz-box-shadow: 0px 0px 4px rgba(50, 50, 50, 0.43);  box-shadow: 0px 0px 4px rgba(50, 50, 50, 0.43);  font-size: 14px;
                background-color: #5bc4be;
                padding-top: 4px;
                padding-bottom: 4px;  padding-left: 8px;  padding-right: 9px;  float: left;  text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
            }
            .Label a {
                padding-top: 4px;
                padding-bottom: 6px;  padding-left: 8px;
                padding-right: 9px;  float: left;  text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.1);
                background-color: #446cb3;  color: #fff!important;
                font-size: 12px!important;  margin-bottom: 10px;  float: left;
                /* margin-right: 10px; */  display: inline-block;  -webkit-transition: all 0.25s linear;  -moz-transition: all 0.25s linear;  transition: all 0.25s linear;
            }
            .Label {
                font-size: 12px!important;
            }
            .widget-item-control, .blog-feeds, .post-feeds,.datetime , .thread-toggle{
                display: none;
            }
            #blog-pager {
                clear: both;  overflow: hidden;  float: left;  width: 100%;  margin-top: 0;  padding: 35px 0 30px;  border-top: 1px solid rgba(0, 0, 0, 0.12);  -webkit-box-sizing: border-box;  -moz-box-sizing: border-box;  box-sizing: border-box;
            }
            .showpageArea {
                margin: 0;
            }
            .showpageOf {
                display: none;
            }
            .showpagePoint, .showpageNum a:hover, .showpage a:hover {
                border: 1px solid #446cb3;
                background-color: #446cb3;  color: #fff!important;
                margin: 0 5px 0 0;  display: inline-block;  float: left;  line-height: 1.2;  text-decoration: none;  padding: 6px 10px;
                /* background: #EE210B; */
            }
            .showpageNum a,.showpage a {
                margin: 0 5px 0 0;  display: inline-block;  float: left;  line-height: 1.2;  text-decoration: none;  color: #EE210B;  padding: 6px 10px;  -webkit-transition: all 0.25s linear;  -moz-transition: all 0.25s linear;  transition: all 0.25s linear;
                color: #5bc4be;
                border: 1px solid #5bc4be;
            }
            .showpageNum a:hover {}
            .status-msg-hidden, .status-msg-body {
                padding: 1em 0!important;
            }
            /* Contact Form Container */
            .contact-form-widget {
                margin: 0 auto;
                color: #000;
            }
            /* Fields and submit button */
            .contact-form-name, .contact-form-email, .contact-form-email-message {
                width: 100%;
                max-width: 100%;
                margin-bottom: 10px;
                margin-top: 15px;
            }
            /* Submit button style */
            .contact-form-button-submit {
                border-color: #C1C1C1;
                background: #E3E3E3;
                color: #585858;
                width: 20%;
                max-width: 20%;
                margin-bottom: 10px;
            }
            /* Submit button on mouseover */
            .contact-form-button-submit:hover{
                background: #4C8EF9;
                color: #ffffff;
                border: 1px solid #FAFAFA;
            }
            .share-box{margin: 30px 0 10px;}
            .bloggge{display:none;}
            .widd{display:none;}
            .share-title{color:#010101;display:inline-block;padding-bottom:10px;font-size:13px;font-weight:700;position:relative;top:5px;text-transform:uppercase}
            .share-art{float:right;padding:0;padding-top:0;font-size:13px;font-weight:400;text-transform:capitalize}
            .share-art a{color:#fff;padding:3px 8px;margin-left:4px;border-radius:2px;display:inline-block;margin-right:0}
            .share-art a:hover{color:#fff}
            .share-art .fac-art{background:#3b5998}
            .share-art .fac-art:hover{background:rgba(49,77,145,0.7)}
            .share-art .twi-art{background:#00acee}
            .share-art .twi-art:hover{background:rgba(7,190,237,0.7)}
            .share-art .goo-art{background:#db4a39}
            .share-art .goo-art:hover{background:rgba(221,75,56,0.7)}
            .share-art .pin-art{background:#CA2127}
            .share-art .pin-art:hover{background:rgba(202,33,39,0.7)}
            .share-art .lin-art{background:#0077B5}
            .share-art .lin-art:hover{background:rgba(0,119,181,0.7)}

            --></style>
        <style>
            /*-------Typography and ShortCodes-------*/
            .firstcharacter{float:left;color:#27ae60;font-size:75px;line-height:60px;padding-top:4px;padding-right:8px;padding-left:3px}.post-body h1,.post-body h2,.post-body h3,.post-body h4,.post-body h5,.post-body h6{margin-bottom:15px;color:#2c3e50}blockquote{font-style:italic;color:#888;border-left:5px solid #27ae60;margin-left:0;padding:10px 15px}blockquote:before{content:'\f10d';display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;margin-right:10px;color:#888}blockquote:after{content:'\f10e';display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;margin-left:10px;color:#888}.button{background-color:#2c3e50;float:left;padding:5px 12px;margin:5px;color:#fff;text-align:center;border:0;cursor:pointer;border-radius:3px;display:block;text-decoration:none;font-weight:400;transition:all .3s ease-out !important;-webkit-transition:all .3s ease-out !important}a.button{color:#fff}.button:hover{background-color:#27ae60;color:#fff}.button.small{font-size:12px;padding:5px 12px}.button.medium{font-size:16px;padding:6px 15px}.button.large{font-size:18px;padding:8px 18px}.small-button{width:100%;overflow:hidden;clear:both}.medium-button{width:100%;overflow:hidden;clear:both}.large-button{width:100%;overflow:hidden;clear:both}.demo:before{content:"\f06e";margin-right:5px;display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:400;line-height:normal;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.download:before{content:"\f019";margin-right:5px;display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:400;line-height:normal;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.buy:before{content:"\f09d";margin-right:5px;display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:400;line-height:normal;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.visit:before{content:"\f14c";margin-right:5px;display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:400;line-height:normal;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.widget .post-body ul,.widget .post-body ol{line-height:1.5;font-weight:400}.widget .post-body li{margin:5px 0;padding:0;line-height:1.5}.post-body ul li:before{content:"\f105";margin-right:5px;font-family:fontawesome}pre{font-family:Monaco, "Andale Mono", "Courier New", Courier, monospace;background-color:#2c3e50;background-image:-webkit-linear-gradient(rgba(0, 0, 0, 0.05) 50%, transparent 50%, transparent);background-image:-moz-linear-gradient(rgba(0, 0, 0, 0.05) 50%, transparent 50%, transparent);background-image:-ms-linear-gradient(rgba(0, 0, 0, 0.05) 50%, transparent 50%, transparent);background-image:-o-linear-gradient(rgba(0, 0, 0, 0.05) 50%, transparent 50%, transparent);background-image:linear-gradient(rgba(0, 0, 0, 0.05) 50%, transparent 50%, transparent);-webkit-background-size:100% 50px;-moz-background-size:100% 50px;background-size:100% 50px;line-height:25px;color:#f1f1f1;position:relative;padding:0 7px;margin:15px 0 10px;overflow:hidden;word-wrap:normal;white-space:pre;position:relative}pre:before{content:'Code';display:block;background:#F7F7F7;margin-left:-7px;margin-right:-7px;color:#2c3e50;padding-left:7px;font-weight:400;font-size:14px}pre code,pre .line-number{display:block}pre .line-number a{color:#27ae60;opacity:0.6}pre .line-number span{display:block;float:left;clear:both;width:20px;text-align:center;margin-left:-7px;margin-right:7px}pre .line-number span:nth-child(odd){background-color:rgba(0, 0, 0, 0.11)}pre .line-number span:nth-child(even){background-color:rgba(255, 255, 255, 0.05)}pre .cl{display:block;clear:both}#contact{background-color:#fff;margin:30px 0 !important}#contact .contact-form-widget{max-width:100% !important}#contact .contact-form-name,#contact .contact-form-email,#contact .contact-form-email-message{background-color:#FFF;border:1px solid #eee;border-radius:3px;padding:10px;margin-bottom:10px !important;max-width:100% !important}#contact .contact-form-name{width:47.7%;height:50px}#contact .contact-form-email{width:49.7%;height:50px}#contact .contact-form-email-message{height:150px}#contact .contact-form-button-submit{max-width:100%;width:100%;z-index:0;margin:4px 0 0;padding:10px !important;text-align:center;cursor:pointer;background:#27ae60;border:0;height:auto;-webkit-border-radius:2px;-moz-border-radius:2px;-ms-border-radius:2px;-o-border-radius:2px;border-radius:2px;text-transform:uppercase;-webkit-transition:all .2s ease-out;-moz-transition:all .2s ease-out;-o-transition:all .2s ease-out;-ms-transition:all .2s ease-out;transition:all .2s ease-out;color:#FFF}#contact .contact-form-button-submit:hover{background:#2c3e50}#contact .contact-form-email:focus,#contact .contact-form-name:focus,#contact .contact-form-email-message:focus{box-shadow:none !important}.alert-message{position:relative;display:block;background-color:#FAFAFA;padding:20px;margin:20px 0;-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;color:#2f3239;border:1px solid}.alert-message p{margin:0 !important;padding:0;line-height:22px;font-size:13px;color:#2f3239}.alert-message span{font-size:14px !important}.alert-message i{font-size:16px;line-height:20px}.alert-message.success{background-color:#f1f9f7;border-color:#e0f1e9;color:#1d9d74}.alert-message.success a,.alert-message.success span{color:#1d9d74}.alert-message.alert{background-color:#DAEFFF;border-color:#8ED2FF;color:#378FFF}.alert-message.alert a,.alert-message.alert span{color:#378FFF}.alert-message.warning{background-color:#fcf8e3;border-color:#faebcc;color:#8a6d3b}.alert-message.warning a,.alert-message.warning span{color:#8a6d3b}.alert-message.error{background-color:#FFD7D2;border-color:#FF9494;color:#F55D5D}.alert-message.error a,.alert-message.error span{color:#F55D5D}.fa-check-circle:before{content:"\f058"}.fa-info-circle:before{content:"\f05a"}.fa-exclamation-triangle:before{content:"\f071"}.fa-exclamation-circle:before{content:"\f06a"}.post-table table{border-collapse:collapse;width:100%}.post-table th{background-color:#eee;font-weight:bold}.post-table th,.post-table td{border:0.125em solid #333;line-height:1.5;padding:0.75em;text-align:left}@media (max-width: 30em){.post-table thead tr{position:absolute;top:-9999em;left:-9999em}.post-table tr{border:0.125em solid #333;border-bottom:0}.post-table tr + tr{margin-top:1.5em}.post-table tr,.post-table td{display:block}.post-table td{border:none;border-bottom:0.125em solid #333;padding-left:50%}.post-table td:before{content:attr(data-label);display:inline-block;font-weight:bold;line-height:1.5;margin-left:-100%;width:100%}}@media (max-width: 20em){.post-table td{padding-left:0.75em}.post-table td:before{display:block;margin-bottom:0.75em;margin-left:0}}
            .FollowByEmail {
                clear: both;
            }
            .widget .post-body ol {
                padding: 0 0 0 15px;
            }
            .post-body ul li {
                list-style: none;
            }
        </style>
        
        
        <style>
            @media screen and (max-width:1100px) {
                .sidebar-wrapper {
                    float: left;
                }
            }
            @media screen and (max-width:1000px) { 
                .main-wrapper { width: 66.8% }
                .sidebar-wrapper { width: 30% ; }
                .footer{
                    width: 32%;
                    margin-right: 2%;
                }
                .metacom {
                    width: 100%;
                }
            }
            @media screen and (max-width:900px) {
                .mobilemenu {
                    left: 0;
                    float: left;
                    margin-top: 0;
                    width: 100%;
                }
            }
            @media screen and (max-width:865px) { 
                .headerright {
                    left: 0;  margin-right: 0;
                }
                #navigation ul {
                    display: none;
                }
                #navigation ul li:hover{
                    background: rgba(0, 0, 0, 0)!important;
                }
                a#pull {
                    display: block;
                    width: 42px;
                    color: #fff;
                    padding: 10px 0 9px 0!important;
                    float: none;
                    position: absolute;
                    text-indent: -999em;
                    right: 0;
                    top: 28px;
                    z-index: 1000;
                    background-color: #446cb3;
                    color: #fff!important;
                }
                a#pull:after {
                    content: "\f00b";
                    font-family: fontawesome;
                    display: inline-block;
                    font-size: 22px;
                    position: absolute;
                    left: 10px;
                    top: 11px;
                    text-indent: initial;
                }
                .search_li {
                    width: 100%;
                    margin-right: 55px;
                    margin-left: 0;
                    margin-top: 2px;
                }

                #logo {
                    height: auto;
                }
                .logowrap {
                    margin: 0;
                }
                .mobilemenu {
                    display: block;
                    position: absolute!important;
                    min-height: 43px;
                    top: 0px!important;
                    left: auto;
                    right: 0;
                    width: 76%;
                    visibility: hidden;
                    -webkit-transition: visibility 0 linear;
                    -webkit-transition-delay: 0.4s;
                    -moz-transition: visibility 0 linear 0.4s;
                    -o-transition: visibility 0 linear 0.4s;
                    transition: visibility 0 linear 0.4s;
                    z-index: -999;
                    height: 100%;
                }
                .navigation-is-visible a#pull {
                    right: 6px;
                }
                .mobilemenu #navigation {
                    height: 100%;
                    width: 100%;
                    float: left;
                    display: block;
                    margin-top: 0;
                    margin-bottom: 0;
                    background-color: #446cb3;
                    color: #fff!important;
                }
                .mobilemenu #navigation {
                    border: none!important;
                }
                .mobilemenu #navigation .menu {
                    float: left;
                    background: transparent;
                    border-left: 0;
                    display: block;
                }
                .mobilemenu #navigation ul {
                    width: 100%;
                    height: auto;
                    display: block;
                    visibility: visible;
                    opacity: 1;
                    position: static;
                    transform: none;
                }
                .mobilemenu #navigation li, .mobilemenu #navigation ul li li {
                    width: 100%!important;
                    float: left;
                    position: relative;
                    background: rgba(0, 0, 0, 0)!important;
                    z-index: 100;
                }
                .mobilemenu #navigation a {
                    text-align: left;
                }
                .navigation-is-visible .mobilemenu {
                    visibility: visible;
                    z-index: 1;
                    -webkit-transition: z-index 0 linear;
                    -webkit-transition-delay: 0.4s;
                    -moz-transition: z-index 0 linear 0.4s;
                    -o-transition: z-index 0 linear 0.4s;
                    transition: z-index 0 linear 0.4s;
                    border-left: 1px solid rgba(22, 22, 22, 0.13)!important;
                    padding: 0;
                }
                .navigation-is-visible .main-container {
                    -webkit-transform: translateX(-76%);
                    -moz-transform: translateX(-74%);
                    -ms-transform: translateX(-74%);
                    -o-transform: translateX(-74%);
                    transform: translateX(-74%);
                    overflow: hidden;
                }
                .main-container {
                    z-index: 1000;
                    min-height: 350px;
                    height: 100%;
                    -webkit-transform: translateZ();
                    -moz-transform: translateZ();
                    -ms-transform: translateZ();
                    -o-transform: translateZ();
                    transform: translateZ();
                    -webkit-transition: all 0.4s ease-in-out;
                    -moz-transition: all 0.4s ease-in-out;
                    -o-transition: all 0.4s ease-in-out;
                    transition: all 0.4s ease-in-out;
                }
                .navigation-is-visible .mobilemenu #navigation .menu a, .navigation-is-visible #navigation .menu[class^="icon-"] > a, .navigation-is-visible #navigation .menu[class*=" icon-"] > a {
                    color: #fff!important;
                    padding: 0!important;
                    line-height: 45px!important;
                    padding-left: 18px!important;
                    margin-right: 0;
                    width: 100%;
                    text-transform: none;
                    margin-left: 0;
                    font-family: 'Varela Round';
                    font-size: 14.285714149475098px;
                }
                .navigation-is-visible .mobilemenu #navigation li a {
                    border-bottom: 1px solid rgba(22, 22, 22, 0.13)!important;
                    border-top: 0 !important;
                }
            }
            @media screen and (max-width:800px) { 
                .main-wrapper {
                    width: 100%!important;
                }
                .sidebar-wrapper {
                    width: 310px;
                }
                .footer-widgets .footer {
                    width: 95%;
                }
            }
            @media screen and (max-width:750px){
                .headerad-wrap {
                    display:none;
                }
                .post.hentry {
                    margin-bottom: 0;
                }
                h2.post-title {
                    font-size: 20px;
                }
                .postbody {
                    font-size: 13px;
                    line-height: 18px;
                }
            }
            @media screen and (max-width: 550px) {
                .logowrap {
                    width: 100%;
                    text-align: center;
                    margin: 0;
                }
                #related-posts {
                    margin:0;
                }
                #comment-holder {
                    margin: 0;
                }
                .comments .comments-content .comment-replies {
                    margin-left: 0;
                }
                #logo{
                    display: inline-block;
                    float: none; 
                }
                #header h1, #header h2 {
                    display: inline-block;
                    float: none;
                }
                #header h1, #header h2 {
                    font-size: 40px;
                }

                .headerright {
                    width: 100%;  background: #eee;  padding: 2%;  float: none;  overflow: hidden;  -webkit-box-sizing: border-box;  -moz-box-sizing: border-box;  box-sizing: border-box;
                }
                #navigation {
                    margin-top: 0;
                    margin-bottom: 0;
                }
                a#pull {
                    position: relative;
                    top: auto;
                    float: right;
                }
                .search_li {
                    width: 80%;
                    float: left;
                    margin-right: 0;
                    margin-bottom: 0;
                    margin-top: 0;
                }
                #header #searchform {
                    width: 100%;
                }
                .hover_img {
                    float: none;
                    margin-right: 0;
                    width: 100%;
                    margin-top: 0;
                }
            }
            @media screen and (max-width:480px) { 
                .relatedpost {
                    width: 100%;
                    margin: 0;
                }
                #comment-holder li {
                    margin: 0;
                }
            }
            @media screen and (max-width:320px) { 
                nav li a { border-bottom: 1px solid #576979 }
            }

        </style>