<div class="col-sm-6  col-lg-6 col-xs-12 ">
    <h3>Dados</h3>
    <h4><b>Nome:</b>Patrick Fernandes</h4>
    <h4><b>Personagem:</b>Fing Fang </h4>
    <h4><b>Raça:</b> KOBOLD</h4>
    <h4><b>Classe/nivel:</b><?= "LADINO   $ladino / FEITICEIRO $feit / CLERICO $clerico"; ?> </h4>
    <h4><b>Tendencia:</b>CN</h4>
    <h4><b>Sexo:</b>MASCULINO </h4>
    <h4><b>Idade:</b>10</h4>
    <h4><b>Divindade:</b>Kally</h4>
    <h4><b>Tamanho:</b>1,20M</h4>
    <h4><b>Deslocamento:</b>9M</h4>
</div>
<div class="col-sm-6  col-lg-6 col-xs-12 ">
    <h3>STATUS</h3>        
    <table class="table">
        <thead>
            <tr>
                <th><b>HABI</b></th>
                <th><b>VALOR</b></th>
                <th><b>MOD</b></th>
                <th><b>DADO<BR>HABILI</b></th>
                <th><b>EXTRA</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>FOR</b></td>
                <td><?= $for ?></td>
                <td><?= $for = floor(($for - 10) / 2); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><b>DES</b></td>
                <td><?= $des ?></td>
                <td><?= $des = floor(($des - 10) / 2); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><b>CON</b></td>
                <td><?= $con ?></td>
                <td><?= $con = floor(($con - 10) / 2); ?></td>
                <td></td>
                <td></td>

            </tr>
            <tr>
                <td><b>INT</b></td>
                <td><?= $int ?></td>
                <td><?= $int = floor(($int - 10) / 2); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><b>SAB</b></td>
                <td><?= $sab ?></td>
                <td><?= $sab = floor(($sab - 10) / 2); ?></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><b>CAR</b></td>
                <td><?= $car ?></td>
                <td><?= $car = floor(($car - 10) / 2); ?></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

