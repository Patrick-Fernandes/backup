<?
session_start("estatistica");
if(!(session_is_registered("_LOGIN") and session_is_registered("_SENHA"))) {
echo "<h2><p align=\"center\"><font color=\"#FF0000\"> �REA RESTRITA</font><BR>
<a href=\"index.php\">Fa�a o login</a></p></h2>";
}
else{
/*
##########################################
#     Estat�sticas de Acessos 3.0        #
# DESENVOLVIDO POR:                      #
# ROBERTO JUNDI FURUTANI                 #
# email: betolinux@bol.com.br            #
# �ltimo UpDate    20/12/2003            #
#    Favor N�o Remover                   #
##########################################
*/
$modo_vis = $HTTP_GET_VARS['modo_vis'];
?>
<html>
<head>
<title>.:: Estat�sticas de Acessos 3.0 ::.</title>
<link rel=stylesheet href="estilos.css" type="text/css">
</head>
<script>
function redir(url){
window.location=url;
}
</script>
<body>
<table border="0" width="100%">
  <tr>
    <td width="100%" colspan="4" align="center"><p align="left"><b><font size="4" color="blue" face="Verdana"> 
    <? switch ($modo_vis){
       case "so_browser":
       echo "Perfil do Visitante"; break;
       
       case "area":
       echo "Acessos por �rea"; break;
       
       case "top20":
       echo "Top 20"; break;
       
       case "dia":
       echo "Acessos por dia"; break;
       case "mes":
       
       echo "Acessos por M�s"; break;
       default:
       echo ".:: Estat�sticas de Acessos 3.0 ::."; break;
       }
    ?></b></font></td>
  </tr>
  <tr>
    <td width="20%" align="center">
    <input type="button" value="Perfil do Visitante" onClick="redir('<?=$PHP_SELF?>?modo_vis=so_browser');" name="bt0" class="form">
    </td>
    <td width="20%" align="center">
    <input type="button" value="Acessos por �rea" onClick="redir('<?=$PHP_SELF?>?modo_vis=area');" name="bt0" class="form">
    </td>
    <td width="20%" align="center">
    <input type="button" value="Acessos por M�s" onClick="redir('<?=$PHP_SELF?>?modo_vis=mes');" name="bt0" class="form">
    </td>
    <td width="40%" align="center"> 
    <input type="button" value="Acessos por Dia" name="bt2" class="form" onClick="redir('<?=$PHP_SELF?>?modo_vis=dia');">
    <input type="button" value="Top 20" name="bt3" class="form" onClick="redir('<?=$PHP_SELF?>?modo_vis=top20');">
    <input type="button" value="Voltar" onClick="javascript:window.location='menu.php'" name="bt0" class="form">
      </tr>
</table>

<?
// Conectando com o banco de dados
include ("connect.php");

//Total de acessos
$sql = "SELECT * FROM acessos";
$resultado = mysql_query($sql);
$total = mysql_num_rows($resultado);
echo "<p align=\"left\" class=cont><b>$total acessos</b> <BR>
<a href=\"sair.php\" class=cont>�Sair�</a></p>";
// ===============================================================
// A C E S S O S  P O R  S I S T E M A S  O P E R A C I O N A I S
// ===============================================================
function mostrar_so($total){
echo "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
      <td width=\"25%\" bgcolor=\"#808FF\" align=\"center\" class=\"tit\">Sistema Operacional</td>
      <td width=\"25%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Acessos</td>
      <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Porcentagem</td>
      <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Gr�fico</td>
      </tr>";

//Vetor com os SO para serem examinados
$lista_so = array('Linux','Windows 95','Windows 98','Windows NT','Windows XP','Mac','SunOS','AIX','OS/2','Windows 2000','Outros') ;   

//conta quantas incidencias tem de cada elemento do vetor acima
// e joga os valores no vetor audiencia
while(list($cont, $nome_so) = each($lista_so)){
$sql_so = "SELECT so FROM acessos WHERE so='$nome_so'";
$resultado = mysql_query($sql_so);
$conta_so = mysql_num_rows($resultado);
$audiencia_so[$nome_so] = $conta_so;
}

//Ordena o vetor audiencia em ordem decrescente
arsort ($audiencia_so);

//Valor inicial da variavel cor para que apareca primeiro a linha 
//branca depois a cinza
$cor = 1;

//mostra a tabela com os resultados
while (list ($key, $valor) = each ($audiencia_so)) {

//Para que o fundo seja branco
$corbg = "#FFFFFF";

//Se a $cor for par mostra o fundo cinza
    if ($cor % 2 == 0 )
    $corbg = "#F2F2F2";
    
echo "<tr>
    <td width=\"25%\" bgcolor=\"$corbg\" align=\"left\" class=\"cont\">$key</td>
    <td width=\"25%\" bgcolor=\"$corbg\" align=\"center\" class=\"cont\">$valor</td>
    <td width=\"25%\" align=\"right\" bgcolor=\"$corbg\"  class=\"cont\">". number_format($valor / $total * 100, 2)."%</td>
    <td width=\"25%\" align=\"left\" bgcolor=\"$corbg\"  class=\"tit\"><img border=\"0\" src=\"barra.gif\" width=\"". number_format($valor / $total * 100, 2)."\" height=\"10\"></td>
    </tr>";
    
//Soma 1     
$cor++;
}
echo "</table>";
}

// ===================================
// A C E S S O S  P O R  B R O W S E R
// ===================================
function mostrar_browser($total){
echo "<br><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td width=\"25%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Browser</td>
    <td width=\"25%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Acessos</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Porcentagem</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Gr�fico</td>
     </tr>";

$cor = 1;

$lista_browser = array('Netscape/Mozilla','MSIE','Opera','Lynx','WebTV','Konqueror','Google','Rob�s de Busca','Desconhecido') ;

while(list($cont, $nome_browser) = each($lista_browser)){
$sql_browser = "SELECT browser FROM acessos WHERE browser='$nome_browser'";
$resultado_browser = mysql_query($sql_browser);
$conta_browser = mysql_num_rows($resultado_browser);
$audiencia_browser[$nome_browser] = $conta_browser;
}

//Ordena 
arsort ($audiencia_browser);

while (list ($cont, $valor1) = each ($audiencia_browser)) {
$corbg = "#FFFFFF";
    if ($cor % 2 == 0 )
    $corbg = "#F2F2F2";
$cor++;
echo "<tr>
    <td width=\"25%\" bgcolor=\"$corbg\" align=\"left\" class=\"cont\">$cont</td>
    <td width=\"25%\" bgcolor=\"$corbg\" align=\"center\" class=\"cont\">$valor1</td>
    <td width=\"25%\" align=\"right\" bgcolor=\"$corbg\"  class=\"cont\">". number_format($valor1 / $total * 100, 2)."%</td>
    <td width=\"25%\" align=\"left\" bgcolor=\"$corbg\"  class=\"tit\"><img border=\"0\" src=\"barra.gif\" width=\"". number_format($valor1 / $total * 100, 2)."\" height=\"10\"></td>
    </tr>";
}
echo "</table><br>";
}

// ===========================
// A C E S S O S  T O P  2 0
// ===========================
function mostrar_top20($total){
echo "<br><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td width=\"25%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\" title=\"Ordem decrescente de acessos\">Data <font size=\"1\" face=\"Verdana\"  color=\"#000080\">[Top 20]</font></td>
    <td width=\"25%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Acessos</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Porcentagem</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Gr�fico</td>
     </tr>";

$cor = 1;

$sql_datas = "SELECT DISTINCT data FROM acessos";
$resultado_datas = mysql_query($sql_datas);
while($nDatas = mysql_fetch_array($resultado_datas)){
$sql_datas = "SELECT data FROM acessos WHERE data='$nDatas[0]'";
$resultado_data = mysql_query($sql_datas);
$conta_dia = mysql_num_rows($resultado_data);
$audiencia_dia[$nDatas[0]] = $conta_dia;
$limite++;
}

//Ordena 
arsort ($audiencia_dia);

$cor = 1;

while ((list ($cont, $valor1) = each ($audiencia_dia)) && ($cor<=20)) {
$corbg = "#FFFFFF";
    if ($cor % 2 == 0 )
    $corbg = "#F2F2F2";
$cont = explode("-",$cont);
echo "<tr>
    <td width=\"25%\" bgcolor=\"$corbg\" align=\"left\" class=\"cont\">$cor. $cont[2]/$cont[1]/$cont[0]</td>
    <td width=\"25%\" bgcolor=\"$corbg\" align=\"center\" class=\"cont\">$valor1</td>
    <td width=\"25%\" align=\"right\" bgcolor=\"$corbg\"  class=\"cont\">". number_format($valor1 / $total * 100, 2)."%</td>
    <td width=\"25%\" align=\"left\" bgcolor=\"$corbg\"  class=\"tit\"><img border=\"0\" src=\"barra.gif\" width=\"". number_format($valor1 / $total * 100, 2)."\" height=\"10\"></td>
    </tr>";
$cor++;
}
echo "</table><br>";
}

// ===========================
// A C E S S O S  P O R  D I A
// ===========================
function mostrar_dia($total){
echo "<br><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td width=\"30%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\" title=\"Ordem cronol�gica decrescente\">Data <font size=\"1\" face=\"Verdana\"  color=\"#000080\">[Ordem Cronol�gica Decrescente]</font></td>
    <td width=\"20%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Acessos</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Porcentagem</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Gr�fico</td>
     </tr>";

$cor = 1;

$sql_datas = "SELECT DISTINCT data FROM acessos ORDER BY data DESC";
$resultado_datas = mysql_query($sql_datas);
                                    
while($nDatas = mysql_fetch_array($resultado_datas)){
$sql_datas = "SELECT data FROM acessos WHERE data='$nDatas[0]'";
$resultado_data = mysql_query($sql_datas);
$conta_dia = mysql_num_rows($resultado_data);
$audiencia_dia[$nDatas[0]] = $conta_dia;
}

while (list ($cont, $valor1) = each ($audiencia_dia)) {
$corbg = "#FFFFFF";
    if ($cor % 2 == 0 )
    $corbg = "#F2F2F2";
$cor++;

$cont = explode("-",$cont);
echo "<tr>
    <td width=\"30%\" bgcolor=\"$corbg\" align=\"left\" class=\"cont\">$cont[2]/$cont[1]/$cont[0]</td>
    <td width=\"20%\" bgcolor=\"$corbg\" align=\"center\" class=\"cont\">$valor1</td>
    <td width=\"25%\" align=\"right\" bgcolor=\"$corbg\"  class=\"cont\">". number_format($valor1 / $total * 100, 2)."%</td>
    <td width=\"25%\" align=\"left\" bgcolor=\"$corbg\"  class=\"tit\"><img border=\"0\" src=\"barra.gif\" width=\"". number_format($valor1 / $total * 100, 2)."\" height=\"10\"></td>
    </tr>";
}
echo "</table><br>";
}

// ===========================
// A C E S S O S  P O R  M � S
// ===========================

function mostrar_mes($total){
echo "<br><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td width=\"30%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\" title=\"Ordem cronol�gica\">M�s <font size=\"1\" face=\"Verdana\"  color=\"#000080\">[Ordem Cronol�gica Decrescente]</font></td>
    <td width=\"20%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Acessos</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Porcentagem</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Gr�fico</td>
     </tr>";

$cor = 1;

$sql_datas = "SELECT DISTINCT MONTH(data) FROM acessos ORDER BY MONTH(data) DESC";
$resultado_datas = mysql_query($sql_datas);
                                    
while($nDatas = mysql_fetch_array($resultado_datas)){
$sql_datas = "SELECT data FROM acessos WHERE MONTH(data)='$nDatas[0]'";
$resultado_data = mysql_query($sql_datas);
$conta_dia = mysql_num_rows($resultado_data);
$audiencia_dia[$nDatas[0]] = $conta_dia;
}

while (list ($cont, $valor1) = each ($audiencia_dia)) {
$corbg = "#FFFFFF";
    if ($cor % 2 == 0 )
    $corbg = "#F2F2F2";
$cor++;
$mes = array ("Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
$cont = $cont-1;
echo "<tr>
    <td width=\"30%\" bgcolor=\"$corbg\" align=\"left\" class=\"cont\">$mes[$cont]</td>
    <td width=\"20%\" bgcolor=\"$corbg\" align=\"center\" class=\"cont\">$valor1</td>
    <td width=\"25%\" align=\"right\" bgcolor=\"$corbg\"  class=\"cont\">". number_format($valor1 / $total * 100, 2)."%</td>
    <td width=\"25%\" align=\"left\" bgcolor=\"$corbg\"  class=\"tit\"><img border=\"0\" src=\"barra.gif\" width=\"". number_format($valor1 / $total * 100, 2)."\" height=\"10\"></td>
    </tr>";
}
echo "</table><br>";
}

// ======================================
//     A C E S S O S  P O R  A R E A S
// ======================================

function vis_area(){
echo "<br><table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td width=\"30%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">�rea</td>
    <td width=\"20%\" bgcolor=\"#8080FF\" align=\"center\" class=\"tit\">Acessos</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Porcentagem</td>
    <td width=\"25%\" align=\"center\" bgcolor=\"#8080FF\"  class=\"tit\">Gr�fico</td>
     </tr>";

$cor = 1;
$res = mysql_query("SELECT num_acessos FROM areas");
while ($aux = mysql_fetch_array($res))
$totalac += $aux[0];

$resultado = mysql_query("SELECT area,num_acessos,url FROM areas ORDER BY num_acessos DESC");
while ($dados = mysql_fetch_array($resultado)){
echo "<tr>
    <td width=\"30%\" bgcolor=\"$corbg\" align=\"left\" class=\"cont\"><a href=$dados[2] class=\"link\">$dados[0]</a></td>
    <td width=\"20%\" bgcolor=\"$corbg\" align=\"center\" class=\"cont\">$dados[1]</td>
    <td width=\"25%\" align=\"right\" bgcolor=\"$corbg\"  class=\"cont\">". number_format($dados[1] / $totalac * 100, 2)."%</td>
    <td width=\"25%\" align=\"left\" bgcolor=\"$corbg\"  class=\"tit\"><img border=\"0\" src=\"barra.gif\" width=\"". number_format($dados[1] / $totalac * 100, 2)."\" height=\"10\"></td>
    </tr>";
}
echo "</table><br>";
}


if ($total == 0)
echo "<b class=\"cont\">Nenhum Acesso</b>";
else
    //Verifica  qual o modo de visualiza��o
    switch ($modo_vis)
    {
    case "mes":{
    mostrar_mes($total);
    }
    break;
    
    case "area":{
    vis_area();
    }
    break;
    
    case "top20":{
    mostrar_top20($total);
    }
    break;
    
    case "dia":{
    mostrar_dia($total);
    }
    break;
    
    case "so_browser":{
    mostrar_so($total);
    mostrar_browser($total);
    }
    break;
    default:
    echo "<b class=\"cont\"><br><font color=\"red\">O modo de visualiza��o n�o est� definido.</font><BR><BR>Escolha uma das op��es acima.</b>";
    }
@mysql_free_result();
?>
<p align="center"><a href="mailto:betolinux@bol.com.br" title="Contato" class="link">Roberto Jundi Furutani</a></p>
</body>
</html>
<?}?>
