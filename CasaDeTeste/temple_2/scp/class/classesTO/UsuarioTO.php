<?php
class UsuarioTO {

	private $id;
	private $nome;
	private $login;
	private $senha;
        private $email;
        private $nivel;
        private $status;
        private $cadastro;

	function __construct($id="", $nome="", $login="", $senha="", $email="", $nivel="", $status="", $cadastro="") {
            $this->id = $id;
            $this->nome = $nome;
            $this->login = $login;
            $this->senha = $senha;
            $this->email = $email;
            $this->nivel = $nivel;
            $this->status = $status;
            $this->cadastro = $cadastro;
        }

        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getNome() {
            return $this->nome;
        }

        public function setNome($nome) {
            $this->nome = $nome;
        }

        public function getLogin() {
            return $this->login;
        }

        public function setLogin($login) {
            $this->login = $login;
        }

        public function getSenha() {
            return $this->senha;
        }

        public function setSenha($senha) {
            $this->senha = $senha;
        }

        public function getEmail() {
            return $this->email;
        }

        public function setEmail($email) {
            $this->email = $email;
        }

        public function getNivel() {
            return $this->nivel;
        }

        public function setNivel($nivel) {
            $this->nivel = $nivel;
        }

        public function getStatus() {
            return $this->status;
        }

        public function setStatus($status) {
            $this->status = $status;
        }

        public function getCadastro() {
            return $this->cadastro;
        }

        public function setCadastro($cadastro) {
            $this->cadastro = $cadastro;
        }



}
?>