<!--ATEN��O ANTES DE ALTERAR AS URL VERIFIQUE OS ARQUIVOS .HTACCESS-->


<?php
include("../include/header.php");
if ($_SESSION["login"]->getNivel() == 2) {
    echo "<script>window.location='HOME;</script>";
}
?>
<?php
//Para buscar a lista de g�nero priemeiramente instanciamos a classe que realizar� a consulta na tabela
$usuarioDAL = new UsuarioDAL();
//O m�todo buscar retorna um true/false, dependendo se ele encontrou ou n�o encontrou registros na base de dados
$retorno = $usuarioDAL->buscar("order by nome");
$conteudotabela = "";
if ($retorno == true) {
    //joga em um array a listagem de grupos existente na tabela
    $usuario = $usuarioDAL->getResp();
    for ($i = 0; $i < count($usuario); $i++) {
        $linha = $usuario[$i];
        $operacoes = "<a href='Alteracao_de_Usuario_N" . $linha->getId() . "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>"
                . " &nbsp;&nbsp;&nbsp;<a href='Exclusao_de_Usuario_N" . $linha->getId() . "'
 		onclick='return confirmaExclusao(\"" . $linha->getLogin() . "\");'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";

        $stsUsuario = "<td style='width: 150px'><a href='Nivel_de_Usuario_N=" . $linha->getStatus() . "' title='Status'><em style='color: green;'>" . $linha->getStatus() . "</em></a></td>";

        if ($linha->getNivel() == 1) {
            $user = "<td style='width: 150px'><a href='Nivel_de_Usuario_N" . $linha->getId() . "' title='Status'><em style='color: green;'>Administrador</em></a></td>";
        } else if ($linha->getNivel() == 3) {
            $user = "<td style='width: 150px'><a href='#' title='N�o � possivel editar este campo nesta tela'><em style='color: Orange;'>Usu�rio Campus</em></a></td>";
        } else {
            $user = "<td style='width: 150px'><a href='Nivel_de_Usuario_N" . $linha->getId() . "' title='Status'>Usu�rio</a></td>";
        }

        $sts = $linha->getStatus();
        if ($sts == 0) {
            $cor = "style='color: red;'";
            $status_do_usuario = "inativo";
            $alt_usuario = "title='Clique para alterar o status do usu�rio. Este usu�rio N�O tem permiss�o para logar no sistema.'";
        } else {
            $cor = "";
            $status_do_usuario = "ativo";
            $alt_usuario = "title='Clique para alterar o status do usu�rio. Este usu�rio tem permiss�o para logar no sistema.'";
        }

        $conteudotabela .= "<tr>
                            <td>" . $linha->getNome() . "</td>
                            <td>" . $linha->getLogin() . "</td>
                            <td>" . $linha->getEmail() . "</td>
                            " . $user . "
                            <td><a href='Status_de_Usuario_N" . $linha->getId() . "' $alt_usuario $cor>" . $status_do_usuario . "</a></td>
                            <td>" . $linha->getCadastro() . "</td>
                            <td>" . $operacoes . "</td>
                          </tr>";
    }
} else {
    $conteudotabela .= "<tr><td colspan=\"2\">Nenhum registro encontrado</td></tr>";
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <?=montaCab('Usuarios', 'disponiveis', 'Cadastro_de_Usuario')?>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered">
                    <th>Nome</th>
                    <th>Login</th>
                    <th>Email</th>
                    <th>Nivel</th>
                    <th>Status</th>
                    <th>Cadastro</th>
                    <th>Op��es</th>
                    <?= $conteudotabela ?>
                </table>
                <br><br>
                <button onclick="javascript:location.href = 'Usuarios_Inativos'" class='btn btn-default' name='file' >Ver usu�rios inativos</button>
            </div>
        </div>
    </div>
</div>

<?php include("../include/footer.php"); ?>
