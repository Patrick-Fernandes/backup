<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    function modoespera() {
        setInterval("atualiza()", 1000);
    }

    function atualiza() {
        document.getElementById('myForm').innerHTML = location.reload();
    }

</script>

<body onLoad="modoespera();">
    Horário: <div id="myForm"><?php echo date('H:m:s'); ?></div>
</body>-->



<div id="myId">Original</div>

<script>
    var div = document.getElementById("myId");

    // Adicionando ouvinte de clique
    div.addEventListener("click", function (event) {
        div.innerHTML = "Alterado =)";
    });

    // Criando um intervalo
    var interval = setInterval(function () {
        var date = new Date();
        div.innerHTML = date.toLocaleTimeString();

        if (date.getSeconds() % 5 == 0) {
            div.innerHTML = "Intervalo cancelado :)";
            clearInterval(interval);
        }
    }, 1000/* 1s */);

</script>