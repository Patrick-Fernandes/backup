<?
/**
 *  frog::SMS
 *  http://fr0gSMS.codigolivre.org.br
 * 
 *  Autor: Luiz Geovani Vier (vier at brturbo.com - http://vier.ta-na.net)
 *  Data: Maio/2004
 *  Licen�a: GNU GPL - http://www.gnu.org/copyleft/gpl.html
 */
include("sms.business.php");
?>
<html>
<head>
  <title><?= $tituloPagina ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#000000">
<p>&nbsp;<br>
  <? 
include("sms.php");
?>
</p>
  <table border="0" width="600" align="center" style="color:#D0D0D0; font-family: verdana,sans; font-size:10px">
  	<tr>
    <td colspan="5"><font size=2>
	Opa! Blz? <BR>
	Atendendo a pedidos, agora o fr0g::SMS tamb�m envia mensagens para a Oi!<br>
	Respostas para algumas perguntas comuns:<br>
	* O envio p/ a Claro est� ok, isso quando o site da claro funciona, o que � meio raro... :-D<BR>
	* O pessoal tem relatado alguns problemas com a TIM nos f�runs. Vou verificar em breve.<br>
	* N�o h� nenhum tipo de cobranca pelas mensagens enviadas por este utilit�rio.<br>
	* A Claro continua permitindo o envio para outras operadoras e a Vivo-RS tamb�m.<br>
	Por favor, quem j� estiver dominando esse treco aqui, ajude os novatos nos f�runs. Valeu!<br>
	&nbsp;<br>
	[]'s<br>
	fr0g<BR>
	&nbsp;<br>
	* Se tiver d�vidas, sugest�es ou simplesmente precisar falar com algu�m, por favor utilize os f�runs:<BR>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> <a href=http://codigolivre.org.br/forum/forum.php?forum_id=2327 style="color:#DDDDDD" target="_blank">Ajuda</a>: D�vidas, problemas na instalac�o e erros malucos<br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;> <a href=http://codigolivre.org.br/forum/forum.php?forum_id=2326 style="color:#DDDDDD" target="_blank">Uscambau</a>: Sugest�es, dicas, reclamac�es, gambiarras, etc.<br>
	&nbsp;<BR>
	<? /*
	<b><font color=blue>Novidades:</font></b><br>&nbsp;<BR>
	<b>16/08/2004</b><BR>
	* Mas aaaah!! J� foram enviadas mais de <B>100.000</B> mensagens s� aqui pelo site! :)<br>
	* Galera, o envio para celulares da TIM s� funciona para algumas regi�es. Se ao enviar a mensagem aparecer o erro 'N�mero de destino inv�lido' � pq o envio n�o � suportado.<br>
	* Se o fr0g::SMS funciona aqui mas n�o funciona no seu site, tente:<BR>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) Conferir se no php.ini a opc�o register_globals est� ativada<BR>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) Comentar a linha 50 do arquivo sms.class.php: #set_time_limit ($execTimeout);<BR>
	<b>17/07/2004</b><BR>
	O envio para a operadora Claro est� funcionando novamente.<br>
	A prop�sito, <b>a Claro est� permitindo o envio para celulares de outras operadoras!</b> Se vc deseja enviar mensagens para celulares de operadoras n�o suportadas pelo fr0g::SMS, tente enviar usando a operadora Claro.
	<br>&nbsp;<BR>
	</font>
	*/ ?>
	</td></tr>
	<tr>
    <td colspan="5"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;O fr0g::SMS &eacute; 
        um utilit&aacute;rio <a href="http://www.gnu.org/copyleft/gpl.html" style="color:#DDDDDD" target="_blank">open-source (livre)</a> que envia mensagens de texto 
        para celulares de diversas operadoras. <br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Se voc&ecirc; &eacute; desenvolvedor, 
        participe acrescentando suporte a outras operadoras! ;-)<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Se voc&ecirc; n&atilde;o sabe nada 
        de PHP, MySQL e JavaScript, mas quer ajudar, pode criar um help ou divulgar o fr0g::SMS.<br>
        <br>
        * Para obter o c&oacute;digo-fonte, clique <a href="http://codigolivre.org.br/projects/fr0gsms/" style="color:#DDDDDD">aqui</a>.<br>
        <?//* Para visitar o site do autor (Luiz Geovani Vier), clique <a href="http://vier.ta-na.net" style="color:#DDDDDD">aqui</a>.?>
  </td></tr>
	<tr><td colspan="5" valign="bottom"><hr style="color: #1675D0;"></td></tr>
  	<tr valign="top">
		<td width="20%" align="center" valign="top">
			<a target="_blank" href="http://codigolivre.org.br"><IMG src="http://codigolivre.org.br/CLlogo.php?group_id=897" border="0" alt="Logotipo CodigoLivre"></a>
		</td><td width="20%" align="center" valign="top">
			<a target="_blank" href="http://www.brasil.gov.br/"><img src="sms_imgs/bbrasil.gif" width="80" height="15" border="0"></a>
		</td><td width="20%" align="center" valign="top">
			<a target="_blank" href="http://www.php.net/"><img src="sms_imgs/php.gif" border="0" width="80" height="15"></a>
		</td><td width="20%" align="center" valign="top">
			<a target="_blank" href="http://www.mysql.com/"><img src="sms_imgs/mysql.gif" border="0" width="80" height="15"></a> 
		</td>
		<td width="20%" align="center" valign="top">
<!-- Begin Nedstat Basic code -->
<!-- Title: fr0g::SMS -->
<!-- URL: http://fr0gsms.codigolivre.org.br/ -->
<script language="JavaScript" type="text/javascript"
src="http://m1.nedstatbasic.net/basic.js">
</script>
<script language="JavaScript" type="text/javascript" >
<!--
  nedstatbasic("AC++pQPm053ObPv6eJw4gvqGIgGQ", 0);
// -->
</script>
<noscript>
<a target="_blank"
href="http://www.nedstatbasic.net/stats?AC++pQPm053ObPv6eJw4gvqGIgGQ"><img
src="http://m1.nedstatbasic.net/n?id=AC++pQPm053ObPv6eJw4gvqGIgGQ"
border="0" width="18" height="18"
alt="Nedstat Basic - Free web site statistics
Personal homepage website counter"></a><br>
<a target="_blank" href="http://www.nedstatbasic.net/">Free counter</a>
</noscript>
<!-- End Nedstat Basic code -->
		</td>
	</tr>
  </table>
</BODY>
</HTML>
