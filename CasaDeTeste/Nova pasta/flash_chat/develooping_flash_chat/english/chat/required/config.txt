<?
/*   CONFIGURE VARIABLES (Develooping flash Chat version 1.1)  */
/*   _______________________________________________________   */


/*   Administration variables  */
/*   _______________________   */

$admin_name = "admin"; //user name for admin (max. 12 characters)

$admin_password = "admin"; // password for admin (max. 12 characters)


/*   Chat numeric variables    */
/*   _______________________   */

$correct_time = 2850;//difference in seconds with time in server

$chat_lenght = 15;//number of messages displayed in chat room

$review_lenght = 500;//number of messages displayed in review messages

$total_lenght = 1000;//number of messages stored in chat file


/*   Expressions to translate in chat room   */
/*   ______________________________________  */

$private_message_expression = "\(for (.*)\)"; //SEE NOTE AT THE END. regular expression for beginning of private message

$not_here_string = "-The receiver is not in the room-"; //receiver of private message is not in the room

$bye_string = "Bye. We hope see you soon,";//message showed to dimissed user

$enter_string = "(just entered in the room)";//message showed when a new user enters.

$bye_user = "(just leaved the room)";//message showed when a user exits.

$kicked_user = "---You have been kicked from this room---";//message showed in the chat room to kicked user.

$bye_kicked_user = "The next time, try to be polite";//bye for kicked users

$bye_banned_user = "Bye, your entrance to this room have been prohibited";//bye for banned users

$banned_user = "Sorry, you can not enter in this room";//message for banned user trying to enter


/*   Expressions to translate in administration page   */
/*   ________________________________________________  */

$no_users = "There aren't users in the room";//no users in the room

$text_for_kick_button = "Kick";//text fof kick button

$text_for_bann_button = "Bann";//text for button for bannig ips

$no_ips = "There aren't banned IPs in the room";//no banned IPs in the room

$text_for_pardon_button = "Pardon";//text for button to pardon ips

$ip_link = "Administration for banned IPs";//text for link to banned IPs

$users_link = "Administration for user list";//text for link to connected users


/*   Worlds to filter   */
/*   _________________  */

$worlds_to_filter = array("fucking", "fuck", "shit");//list of bad worlds to replace (add more if you want)

$replace_by = "*@#!";//expression to replace bad worlds


/*   NOTE:   */
/*   ______  */

/*   
     If you replace the regexp for private messages ($private_message_expression)
     you must to do the same in ActionScript asociated to the button "Send" in the users screen
     of the file "chat.fla". Then you must export again the movie "chat.swf" with this value changed
     
     Example: If you set $private_message_expression = "\(private to (.*)\)";
     you must to set the mentioned Action script as follow:
     
     on (release, keyPress "<Enter>") {
	if (mensajeprivado ne "") {
		if (destinatario ne "") {
			message = "(private to " add destinatario add ") ....
     
    In case of doubt don't touch this variable.
    
     */   
?>