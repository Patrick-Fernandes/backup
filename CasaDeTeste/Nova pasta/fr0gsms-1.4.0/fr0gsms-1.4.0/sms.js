/**
 *  frog::SMS
 *  http://fr0gSMS.codigolivre.org.br
 * 
 *  Autor: Luiz Geovani Vier (vier at brturbo.com - http://vier.ta-na.net)
 *  Data: Maio/2004
 *  Licen�a: GNU GPL - http://www.gnu.org/copyleft/gpl.html
 */

var controleTimeout = null;
var carregandoPagina = true;
var incluindoContato = false;
var alterandoContato = true;
var statusPing = "";
var operadoraPing = "";
var arrAgenda = new Array();

function trocaOperadora (op) {
	if (incluindoContato) return;
	if (!carregandoPagina) cancelarPing();
	var trConf = document.getElementById("trConfirmacao");
	var tdConf = trConf.cells[0];
	if (op == "vivo-rs") {
		tdConf.innerHTML = '<img style="border: 1px solid #228B22;" height=31 width=99 src="check.php?operadora=vivo-rs&r=' + new Date().getTime() + '">'
		+ ' <font color=black>Repita:</font> <input type="text" name="check" value="" size=4 maxlength=3 tabindex=7>';
		trConf.style.display = '';
	} else if (op == "tim") {
		tdConf.innerHTML = '<img style="border: 1px solid #4A73DD;" height=40 width=100 src="check.php?operadora=tim&r=' + new Date().getTime() + '">'
		+ ' <font color=black>Repita:</font> <input type="text" name="check" value="" size=5 maxlength=4 tabindex=7>';
		trConf.style.display = '';
	} else if (op == "oi") {
		tdConf.innerHTML = '<img style="border: 1px solid #4A73DD;" height=52 width=121 src="check.php?operadora=oi&r=' + new Date().getTime() + '">'
		+ ' <font color=black>Repita:</font> <input type="text" name="check" value="" size=5 maxlength=4 tabindex=7>';
		trConf.style.display = '';
	//} else if (op == "claro") {
	//	var checkSRC = 'check.php?operadora=claro&r=' + new Date().getTime();
	//	tdConf.innerHTML = '<iframe id="ifrCheck" width=80 height=25 src="' + checkSRC + '" scrolling="no" frameborder="0" style="">'
	//	+ '</iframe>'
	//	+ ' <font color=black>Repita:</font> <input type="text" name="check" value="" size=5 maxlength=4 tabindex=7>';
	//	trConf.style.display = '';
	} else {
		tdConf.innerHTML = '';
		trConf.style.display = 'none';
	}
	if (!carregandoPagina) {
		setStatus('');
		pingSeNecessario(op);
	}
}
function pingSeNecessario(op) {
	if (op == "claro") {
		ping();
	}
}

function contaCaracteres () {
	var qtd = 0;
	var disp = 0;
	var limite = 150;
	var qtdOutros = 0;
	var f = document.formSMS;
	qtd += (f.de.value.length*1);
	qtdOutros += (f.de.value.length*1);
	qtd += (f.mensagem.value.length*1);
	disp = limite - qtd;
	f.chars.value = disp < 0 ? 0 : disp;
	if (disp <= 0) {
		f.mensagem.value = f.mensagem.value.substring (0, limite-qtdOutros);
	}
}
function validarEnvio () {
	var f = document.formSMS;
	if (f.operadora && f.operadora.selectedIndex == 0) {
		return pam ('Selecione a operadora', f.operadora);
	}
	if (incluindoContato) {
		if (f.nomePara.value == "") {
			return pam ('Informe o nome do contato', f.nomePara);
		}
	}
	if (f.dddPara && f.dddPara.value == "") {
		return pam ('Informe o DDD', f.dddPara);
	}
	if (isNaN(f.dddPara.value) || f.dddPara.value < 2) {
		return pam ('DDD inv�lido', f.dddPara);
	}
	if (f.telPara && f.telPara.value == "") {
		return pam ('Informe o telefone', f.telPara);
	}
	if (isNaN(f.telPara.value) || f.telPara.value < 8) {
		return pam ('Telefone inv�lido', f.telPara);
	}
	if (!incluindoContato) {
		if (f.mensagem && f.mensagem.value == "") {
			return pam ('Escreva sua mensagem', f.mensagem);
		}
	}
	// s� valida o campo check se estiver visivel
	if (!incluindoContato && document.getElementById("trConfirmacao").style.display != 'none' && f.check != null && f.check.value == "") {
		return pam ('Repita o c�digo de checagem', f.check);
	}
	
	// passou por todos os testes
	f.enviar.disabled = true;
	f.enviar.value = 'Aguarde';
	
	if (!incluindoContato) setStatus('Enviando...');
	return true;
}
function validarLogin () {
	var f = document.formLogin;
	if (f.usuarioLogin.value == "") {
		return pam ('Informe o usu�rio', f.usuarioLogin);
	}
	if (f.senhaLogin.value == "") {
		return pam ('Informe a senha', f.senhaLogin);
	}
	// passou por todos os testes
	f.okLogin.disabled = true;
	
	return true;
}
function validarCadastro () {
	var f = document.formCadastro;
	if (f.usuarioCadastro.value == "") {
		return pam ('Informe o usu�rio', f.usuarioCadastro);
	}
	if (f.senhaCadastro.value == "") {
		return pam ('Informe a senha', f.senhaCadastro);
	}
	if (f.senhaCadastro2.value == "") {
		return pam ('Confirme a senha', f.senhaCadastro2);
	}
	if (f.senhaCadastro.value != f.senhaCadastro2.value) {
		return pam ('As senhas n�o conferem', f.senhaCadastro);
	}
	if (f.emailCadastro.value != "" && !validarEmail(f.emailCadastro.value)) {
		return pam ('Email inv�lido', f.emailCadastro);
	}
	// passou por todos os testes
	f.okCadastro.disabled = true;
	
	return true;
}
function ping () {
	
	if (statusPing == "executando") {
		return;
	}
	
	removerControleTimeout();
	var op = document.formSMS.operadora;
	if (op.selectedIndex == 0) return pam ('Selecione a operadora', op);
	
	operadoraPing = op.options[op.selectedIndex].text;
	
	request('ping.php?operadora=' + op.value + '&r=' + new Date().getTime());
	setStatus('Verificando o status da operadora ' + operadoraPing + '...');
	
	statusPing = "executando";
	adicionarControleTimeout("reportarTimeoutPing();");
}
function cancelarPing() {
	if (statusPing != "executando") return;
	removerControleTimeout();
	statusPing = "";
	request('javascript:false;');
	setStatus('');
}
function tratarRetornoPing (sucesso, mensagem) {
	var msg = 'Status ' + operadoraPing + ': ' + mensagem;
	if (!sucesso) msg = '<div style="color:red"><b>' + msg + '</b></div>';
	statusPing = sucesso ? "" : "erro";
	setStatus(msg);
}
function setarFoco () {
	var f = document.formSMS;
	if (f.operadora.selectedIndex == 0) {
		if (!f.operadora.disabled) f.operadora.focus();
	} else if (f.dddPara.value == "") {
		if (!f.dddPara.disabled) f.dddPara.focus();
	} else if (f.telPara.value == "") {
		if (!f.telPara.disabled) f.telPara.focus();
	} else {
		if (!f.mensagem.disabled) {
			f.mensagem.focus();
			f.mensagem.value = f.mensagem.value; // vai pro final do texto
		}
	}
}
function habilitarCampos (simNao) {
	var f = document.formSMS;
	for (i = 0; i < f.elements.length; i++) {
		if ((el = f.elements[i]).name != 'operadora') {
			el.disabled = !simNao;
		}
	}
}
function pam (msg, campo) {
	alert(msg);
	if (campo) {
		try {
			campo.focus();
		} catch (e) {}
	}
	return false;
}
function setStatus (msg) {
	document.getElementById("status").innerHTML = msg;
}
function request (url) {
	var ifr = document.getElementById("ifrReq");
	if (ifr.src != url) ifr.src = url;
}
function soNumeros(fld, e) {
	var i = j = 0;
	var len = len2 = 0;
	var strCheck = '0123456789';
	var aux = aux2 = '';
	var whichCode = (window.Event) ? e.which : e.keyCode;
	if (whichCode != 0 && whichCode != 8) {
		key = String.fromCharCode(whichCode);
		if (strCheck.indexOf(key) == -1) return false;

	}
}
function nextFocus (obj) {
	/*
	var val = obj.value;
	var maxl = obj.maxLength;
	if (val != "" && maxl > 0 && val.length == maxl) {
		var nextTab = obj.tabIndex+1;
		with (obj.form) {
			for (i = 0; i < elements.length; i++) {
				var el = elements[i];
				if (el.tabIndex == nextTab && !isHidden(el)) {
					try { el.focus(); } catch(e) {}
					return;
				}
			}
		}
	} */
}
function isHidden(obj) {
	if (obj == null) return true;
	if (obj.style == null) return false;
	if (obj.style.display == 'none') return true;
	if (obj.style.visibility == 'hidden') return true;
	return false;
}
function adicionarControleTimeout (funcao) {
	removerControleTimeout ();
	controleTimeout = setTimeout(funcao, execTimeout);
}
function removerControleTimeout () {
	if (controleTimeout) {
		clearTimeout(controleTimeout);
		controleTimeout = null;
	}
}
function reportarTimeoutPing () {
	controleTimeout = null;
	setStatus('');
	request('javascript:false;');
	//execTimeout/1000 s
	tratarRetornoPing(false, 'Indispon�vel');
}
function logout() {
	if (!usuario) return;
	var msg = "Deseja efetuar o logout?";
	if (!confirm(msg)) return;
	document.location = currHREF + '?acao=logout&r=' + new Date().getTime();
}
function recarregar () {
	document.location = currHREF + '?r=' + new Date().getTime();
}
function findPosX(obj) {
	var curleft = 0;
 	if (document.getElementById || document.all) {
 		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
 	} else if (document.layers) curleft += obj.x;
 	return curleft;
}
function findPosY(obj) {
	var curtop = 0;
	if (document.getElementById || document.all) {
		while (obj.offsetParent) {
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	} else if (document.layers) curtop += obj.y;
 	return curtop;
}
function validarEmail(enderecoMail) {
	var exclude=/[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
	var check=/@[\w\-]+\./;
	var checkend=/\.[a-zA-Z]{2,3}$/;
	if(((enderecoMail.search(exclude) != -1)||	
		(enderecoMail.search(check)) == -1) ||	
		(enderecoMail.search(checkend) == -1))
	{
		return false;
	} else {
		return true;
	}
}
// carregar dados da agenda
function cda(pos, alteracao) {
	if (!alteracao && incluindoContato) {
		alert('Conclua ou cancele a ' + (alterandoContato ? 'altera��o' : 'inclus�o') + ' de contato');
		return;
	}
	var obj = arrAgenda[pos*1];
	var f = document.formSMS;
	var op = obj.operadora;
	var opAtual = f.operadora.value;
	
	if (op != opAtual) {
		for (i = 0; i < f.operadora.options.length; i++) {
			if (f.operadora.options[i].value == op) {
				f.operadora.options[i].selected = true;
				if (!alteracao) trocaOperadora(op);
				break;
			}
		}
	}
	f.dddPara.value = obj.ddd;
	f.telPara.value = obj.telefone;
	f.de.value = obj.de;
	f.mensagem.value = (obj.mensagemPadrao) ? obj.mensagemPadrao : "";
	
	contaCaracteres();
	setTimeout("setarFoco();", 300);
}
function limparFormSMS () {
	var f = document.formSMS;
	//f.operadora.options[0].selected = true;
	f.dddPara.value = '';
	f.telPara.value = '';
	f.de.value = '';
	f.mensagem.value = '';
	contaCaracteres();
	//setarFoco();
	f.operadora.focus();
}
function carregarContatoDefault(nome, execPing) {
	var pos = -1;
	if (nome) {
		for (i = 0; i < arrAgenda.length; i++) {
			var obj = arrAgenda[i];
			if (obj.id == nome) {
				pos = i;
				break;
			}
		}
	}
	if (pos >= 0) {
		cda (pos);
		if (execPing) pingSeNecessario(arrAgenda[pos].operadora);
	} else {
		trocaOperadora (document.formSMS.operadora.value);
		contaCaracteres();
		setarFoco();
	}
}
// excluir contato
function exc(pos) {
	var obj = arrAgenda[pos*1];
	var msg = "Tem certeza que deseja excluir o contato \"" + obj.id + "\"?";
	if (!confirm(msg)) return;
	document.location = currHREF + '?acao=excluirContato&idContato=' + escape(obj.id) + '&r=' + new Date().getTime();
}
// adicionar dados no array de agenda
function addA(id, operadora, ddd, telefone, de, mensagemPadrao, qtdEnviadas) {
	var obj = new Object();
	obj.id = id;
	obj.operadora = operadora;
	obj.ddd = ddd;
	obj.telefone = telefone;
	obj.de = de;
	obj.mensagemPadrao = mensagemPadrao;
	obj.qtdEnviadas = qtdEnviadas;
	arrAgenda[arrAgenda.length] = obj;
}
function incluirContato() {
	cancelarPing();
	
	var f = document.formSMS;
	f.operadora.options[0].selected = true;
	f.nomePara.value = '';
	f.dddPara.value = '';
	f.telPara.value = '';
	f.de.value = usuario;
	f.mensagem.value = '';
	f.mensagem.style.height = 40;
	f.acao.value = 'incluirContato';
	f.contatoAlterado.value = '';
	f.enviar.value = ' OK ';
	f.cancelar.style.display = '';
	f.limpar.style.display = 'none';
	
	document.getElementById("tituloEnviarMSG").style.display = 'none';
	document.getElementById("linkPing").style.display = 'none';
	document.getElementById("tituloIncluirContato").innerHTML = 'Incluir Contato';
	document.getElementById("tituloIncluirContato").style.display = '';
	document.getElementById("linhaNomePara").style.display = '';
	document.getElementById("enterOK").style.display = '';
	document.getElementById("labelTelPara").innerHTML = '<strong>Telefone*</strong>:&nbsp;';
	document.getElementById("labelMensagem").innerHTML = '<strong>Mensagem Padr�o</strong>:&nbsp;';
	
	trocaOperadora('');
	incluindoContato = true;
	alterandoContato = false;
	setTimeout("setarFoco();", 300);
}
function altc(pos) {
	cancelarPing();
	var obj = arrAgenda[pos*1];
	cda(pos, true);
	
	var f = document.formSMS;
	f.nomePara.value = obj.id;
	f.acao.value = 'alterarContato';
	f.contatoAlterado.value = obj.id;
	f.mensagem.style.height = 40;
	f.enviar.value = ' OK ';
	f.cancelar.style.display = '';
	f.limpar.style.display = 'none';
	
	document.getElementById("tituloEnviarMSG").style.display = 'none';
	document.getElementById("linkPing").style.display = 'none';
	document.getElementById("tituloIncluirContato").innerHTML = 'Alterar Contato "' + obj.id + '"';
	document.getElementById("tituloIncluirContato").style.display = '';
	document.getElementById("linhaNomePara").style.display = '';
	document.getElementById("enterOK").style.display = '';
	document.getElementById("labelTelPara").innerHTML = '<strong>Telefone*</strong>:&nbsp;';
	document.getElementById("labelMensagem").innerHTML = '<strong>Mensagem Padr�o</strong>:&nbsp;';
	
	trocaOperadora('');
	incluindoContato = true;
	alterandoContato = true;
	setTimeout("setarFoco();", 300);
}

function sobre() {
	var msg = "fr0g::SMS\n\n";
	msg += "Utilit�rio open-source para envio de mensagens SMS\n";
	msg += "Criado por Luiz Geovani Vier (vier";
	msg += "@";
	msg += "brturbo";
	msg += ".com)\n\n"
	msg += "Deseja visitar o site http://fr0gsms.codigolivre.org.br ?";
	if (confirm(msg)) {
		document.location = "http://fr0gsms.codigolivre.org.br";
	}
	return false;
}
