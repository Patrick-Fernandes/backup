
 <?= $_SERVER['PHP_SELF']; ?> <br>'PHP_SELF'<br>
O nome do arquivo do script que está executando, relativa à raiz do documento. Por exemplo, $_SERVER['PHP_SELF'] em um script no endereço http://example.com/test.php/foo.bar seria /test.php/foo.bar. A constante __FILE__ contém o caminho completo e nome do atual arquivo (i.e. incluído). Se estiver rodando o PHP em linha de comando, esta variável contém o nome do script desde o PHP 4.3.0. Anteriormente ela não estava disponível.
<br><br>




<?= $_SERVER['GATEWAY_INTERFACE']; ?> <br>'GATEWAY_INTERFACE'<br>
O número de revisão da especificação CGI que o servidor está utilizando, por exemplo : 'CGI/1.1'.<br>
<br><br>





<?= $_SERVER['SERVER_ADDR']; ?> <br>'SERVER_ADDR'<br>
O endereço IP do servidor onde está o script em execução.<br>
<br><br>




<?= $_SERVER['SERVER_NAME']; ?> <br>'SERVER_NAME' <br>
O nome host do servidor onde o script atual é executado. Se o script está rodando em um host virtual, este será o valor definido para aquele host virtual.
Nota: No Apache 2, você definir UseCanonicalName = On e ServerName. Caso contrário, este valor refletirá o nome do servidor fornecido pelo cliente, que pode ser forjado. Não é seguro confiar neste valor em contextos que dependam de segurança.
<br><br>





<br><?= $_SERVER['SERVER_SOFTWARE']; ?> <br>'SERVER_SOFTWARE'<br>
A string de identificação do servidor, fornecida nos headers quando respondendo a requests.
<br><br>


<br><?= $_SERVER['SERVER_PROTOCOL']; ?> <br>'SERVER_PROTOCOL' <br>
Nome e número de revisão do protocolo de informação pelo qual a página foi requerida, por exemplo 'HTTP/1.0';
<br><br>


<br><?= $_SERVER['REQUEST_METHOD']; ?> <br>'REQUEST_METHOD' <br>
Contém o método de request utilizando para acessar a página. Geralmente 'GET', 'HEAD', 'POST' ou 'PUT'.
<br>Nota:<br>
O script PHP é terminado depois de enviado cabeçalhos (significa depois de produzir alguma saída sem saída do buffer) se o método da requisição for HEAD.
<br><br>


<br><?= $_SERVER['REQUEST_TIME']; ?><br>'REQUEST_TIME' <br>
O timestamp do início da requisição. Disponível desde o PHP 5.1.0.




<br><br>
<br><?= $_SERVER['REQUEST_TIME_FLOAT']; ?><br> 'REQUEST_TIME_FLOAT' <br>
O timestamp, com precisão em microsegundos, do início da requisição. Disponível desde o PHP 5.4.0.
<br><br>



<br><?= $_SERVER['QUERY_STRING']; ?><br> 'QUERY_STRING' <br>
A query string, se houver, pela qual a página foi acessada.


<br><br>
<br><?= $_SERVER['DOCUMENT_ROOT']; ?><br> 'DOCUMENT_ROOT' <br>
O diretório raiz sob onde o script atual é executado, como definido no arquivos de configuração do servidor.

<br><br>
<br><?= $_SERVER['HTTP_ACCEPT']; ?> <br>'HTTP_ACCEPT'<br>
O conteúdo do header Accept: da requisição atual, se houver.

<br><br>

<br><?= $_SERVER['HTTP_ACCEPT_CHARSET']; ?> <br> 'HTTP_ACCEPT_CHARSET' <br>
O conteúdo do header Accept-Charset: da requisição atual, se houver. Exemplo: 'iso-8859-1,*,utf-8'.
<br><br>

<br><?= $_SERVER['HTTP_ACCEPT_ENCODING']; ?> <br>'HTTP_ACCEPT_ENCODING'
O conteúdo do header Accept-Encoding: da requisição atual, se houver. Exemplo: 'gzip'.
<br><br>

<br><?= $_SERVER['HTTP_ACCEPT_LANGUAGE']; ?> <br>'HTTP_ACCEPT_LANGUAGE'
O conteúdo do header Accept-Language: da requisição atual, se houver. Exemplo 'en'.
<br><br>

<br><?= $_SERVER['HTTP_CONNECTION']; ?> 'HTTP_CONNECTION'<br>
O conteúdo do header Connection: da requisição atual, se houver. Exemplo: 'Keep-Alive'.
<br><br>


<br><?= $_SERVER['HTTP_HOST']; ?>  'HTTP_HOST'<br>
O conteúdo do header Host: da requisição atual, se houver.
<br><br>


<br><?= $_SERVER['HTTP_REFERER']; ?> <br>'HTTP_REFERER'<br>
O endereço da página (se houver) através da qual o agente do usuário acessou a página atual. Essa diretiva é informada pelo agente do usuário. Nem todos os browsers geram esse header, e alguns ainda possuem a habilidade de modificar o conteúdo do HTTP_REFERER como recurso. Em poucas palavras, não é confiável.
<br><br>


<br><?= $_SERVER['HTTP_USER_AGENT']; ?> <br>'HTTP_USER_AGENT'<br>
O conteúdo do header User-Agent: da requisição atual, se houver. É uma string denotando o agente de usuário pelo qual a página é acessada. Um exemplo típico é: Mozilla/4.5 [en] (X11; U; Linux 2.2.9 i586). Além de outras coisas, você pode utilizar este valor com get_browser() para personalizar a geração de suas páginas para as capacidades do agente do usuário.
<br><br>


<br><?= $_SERVER['HTTPS']; ?> <br>'HTTPS'<br>
Define para um valor não vazio se o script foi requisitado através do protocolo HTTPS.
Nota: Note que quando usando ISAPI com IIS, o valor será off se a requisição não for feita por protocolo HTTPS.
<br><br>


<br><?= $_SERVER['REMOTE_ADDR']; ?> <br>'REMOTE_ADDR'<br>
O endereço IP de onde o usuário está visualizado a página atual.
<br><br>


<br><?= $_SERVER['REMOTE_HOST']; ?> <br>'REMOTE_HOST'<br>
O nome do host que o usuário utilizou para chamar a página atual. O DNS reverso (lookup) do REMOTE_ADDR do usuário.
Nota: Seu servidor web precisa estar configurado para criar essa variável. Por exemplo, no Apache você precisa colocar um HostnameLookups On dentro do httpd.conf. Veja também gethostbyaddr().
<br><br>


<br><?= $_SERVER['REMOTE_PORT']; ?> <br>'REMOTE_PORT'<br>
A porta TCP na máquina do usuário utilizada para comunicação com o servidor web.
<br><br>


<br><?= $_SERVER['REMOTE_USER']; ?> <br>'REMOTE_USER'<br>
O usuário autenticado
<br><br>


<br><?= $_SERVER['REDIRECT_REMOTE_USER']; ?> <br>'REDIRECT_REMOTE_USER'<br>
O usuário autenticado utilizado se a requisição foi redirecionada internamente.
<br><br>


<br><?= $_SERVER['SCRIPT_FILENAME']; ?> <br>'SCRIPT_FILENAME'<br>
O caminho absoluto o script atualmente em execução.<br>
Nota:<br>Se o script for executado pela CLI com um caminho relativo, como file.php ou ../file.php, $_SERVER['SCRIPT_FILENAME'] irá conter o caminho relativo especificado pelo usuário.
<br><br>

<br><?= $_SERVER['SERVER_ADMIN']; ?> <br>'SERVER_ADMIN'<br>
O valor fornecido pela diretiva SERVER_ADMIN (do Apache) no arquivo de configuração do servidor. Se o script está sendo executado em um host virtual, este será os valores definidos para aquele host virtual.
<br><br>

<br><?= $_SERVER['SERVER_PORT']; ?> <br>'SERVER_PORT'<br>
A porta na máquina servidora utilizada pelo servidor web para comunicação. Como default, este valor é '80'. Utilizando SSL, entretanto, mudará esse valor para a porta de comunicação segura HTTP.
<br>Nota: Utilizando o Apache 2, você deve configurar UseCanonicalName = On, assim como UseCanonicalPhysicalPort = On para obter a porta física (real), caso contrário, este valor pode ser falsificado e pode, ou não, retornar como valor a porta física. Não é seguro confiar neste valor em contextos que dependem de segurança.
<br><br>

<br><?= $_SERVER['SERVER_SIGNATURE']; ?> <br>'SERVER_SIGNATURE'<br>
String contendo a versão do servidor e nome do host virtual que é adicionado às páginas geradas no servidor, se ativo.
<br><br>

<br><?= $_SERVER['PATH_TRANSLATED']; ?> <br>'PATH_TRANSLATED'<br>
O caminho real do script relativo ao sistema de arquivos (não o document root), depois realizou todos os mapeamentos de caminhos (virtual-to-real).
<br>Nota: A partir do PHP 4.3.2, PATH_TRANSLATED não mais existe implicitamente sob a SAPI do Apache 2, ao contrário da mesma situação no Apache 1, onde ela tinha o mesmo valor da variável de servidor SCRIPT_FILENAME, quando a mesma não era configurada pelo Apache. Essa mudança foi realizada para conformidade com a especificação CGI, onde PATH_TRANSLATED deve existir somente se PATH_INFO estiver definida. Usuários do Apache 2 podem usar AcceptPathInfo = On dentro do httpd.conf para definir o PATH_INFO.
<br><br>

<br><?= $_SERVER['SCRIPT_NAME']; ?> <br>'SCRIPT_NAME'<br>
Contém o caminho completo do script atual. Útil para páginas que precisam apontar para elas mesmas (dinamicamente). A constante __FILE__ contém o caminho completo e nome do arquivo (mesmo incluído) atual.
<br><br>

<br><?= $_SERVER['REQUEST_URI']; ?> <br>'REQUEST_URI'<br>
O URI fornecido para acessar a página atual, por exemplo, '/index.html'.<br>
<br><br>



<?= $_SERVER['PHP_AUTH_DIGEST']; ?> <br>'PHP_AUTH_DIGEST'<br>
Quando executando no Apache como módulo fazendo autenticação HTTP esta variável é definida para o cabeçalho 'Authorization' enviado pelo cliente (que você pode então usar para fazer apropriada validação).
<br><br>

<br><?= $_SERVER['PHP_AUTH_USER']; ?> <br>'PHP_AUTH_USER'<br>
Quando efetuando autenticação HTTP, esta variável estará definida com o username fornecido pelo usuário.
<br><br>

<br><?= $_SERVER['PHP_AUTH_PW']; ?> <br>'PHP_AUTH_PW'<br>
Quando efetuando autenticação HTTP, esta variável estará definida com a senha fornecida pelo usuário.
<br><br>


<br><?= $_SERVER['AUTH_TYPE']; ?> <br>'AUTH_TYPE'<br>
Quando efetuando autenticação HTTP, esta variável estará definida com o tipo de autenticação utilizado.
<br><br>




<br><?= $_SERVER['PATH_INFO']; ?> <br>'PATH_INFO'<br>
Contém qualquer informação fornecida pelo usuário, se disponível, que levam ao nome do script atual, precedendo a query string. Por exemplo, se o script atual fosse acessado via URL http://www.example.com/php/path_info.php/some/stuff?fkoo=bar, então $_SERVER['PATH_INFO']teria como valor /some/stuff
<br><br>



<br><?= $_SERVER['ORIG_PATH_INFO']; ?> <br>'ORIG_PATH_INFO'<br>
Versão original do 'PATH_INFO' antes de ser processada pelo PHP.




<a href="index.php">voltar</a>